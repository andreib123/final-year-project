On this page

[[_TOC_]]

# 1. Dependencies

## Jetpack
Currently using [Jetpack v4.2.1](https://developer.nvidia.com/jetpack-421-archive)<br />
Jetpack 4.2.1 components overview: https://www.jetsonhacks.com/2019/07/22/jetpack-4-2-1-release/

## Redundancy Measures
**Checking L4T version**
```dpkg-query --show nvidia-l4t-core```<br />
**Returned:** ```nvidia-l4t-core	32.2.0-20190716172031```<br />
Check which [Jetpack version](https://developer.nvidia.com/embedded/jetpack-archive) this corresponds to. (In this case it is Jetpack v4.2.1)<br />
To do things like clone Jetson image and restore from backup, we must download the L4T tools corresponding to the L4T version on the system.<br />
The L4T tools can be found in the correct L4T release from the [L4T archive](https://developer.nvidia.com/embedded/linux-tegra-archive), under the **DRIVERS** row , **Jetson Nano and TX1** column, downloadable called **L4T Driver Package (BSP)**.<br />
Follow the [Connect Jetson Nano to a Linux OS using a USB to TTL serial cable](https://desertbot.io/blog/jetson-nano-usb-login) and [Clone/Restore Jetson Nano image](https://elinux.org/Jetson/Clone) tutorials to clone/resotre a Jetson Nano system image.<br />
**Note:** To put Nano into Forced Recovery Mode: while it is powered on, jumper the reset pins on J40, jumper the recovery pins on J40, remove recovery pins jumper, remove reset pins jumper.<br />
**If this doesn't work (didn't work for me):** use the [HDD Raw Copy Tool](https://hddguru.com/software/HDD-Raw-Copy-Tool/) in Windows to create a backup .imgc image of the SD card.

## OpenCV v4.1.1 (Python: not a requirement)
**GUIDE:** [OpenCV 4 + CUDA on Jetson Nano](https://www.jetsonhacks.com/2019/11/22/opencv-4-cuda-on-jetson-nano/)<br />
**NOTE1:** OPENCV python API does not support CUDA GPU acceleration (use C++ API for GPU enabled tasks).<br />
**NOTE2:** Installation becomes twicequicker if increasing the swap size from default 2GB configured on Jetpack 4.2.1 to 4GB<br />
**NOTE3:** The default OpenCV version (both Python and C++) installed with JetPack is 3.3.1.<br />
**INSTALLATION (takes ~2 hrs):**
```bash
git clone https://github.com/JetsonHacksNano/buildOpenCV
cd buildOpenCV
./buildOpenCV.sh |& tee openCV_build.log
```

## Visual Studio Code
**v1.39:**
```bash
curl -s https://packagecloud.io/install/repositories/swift-arm/vscode/script.deb.sh | sudo bash
sudo apt-get install code-oss=1.39.2-1575676457
```
**ALTERNATIVELY v1.32:**
```bash
git clone https://github.com/JetsonHacksNano/installVSCode.git
cd installVSCode
./installVSCode.sh
```

## Extra Python packages
```bash
sudo apt-get install python3-matplotlib -y
```

## STREAMLINK AND YOUTUBE-DL
```bash
sudo apt install streamlink
sudo pip install --upgrade youtube_dl
```

## cuDNN 7.5.0 (Installed by default in Jetpack 4.2.1, currently no way to successfully install and use a version other than that provided by Jetpack)
**GUIDE:** [Installing cuDNN on Linux](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html)<br />
**NOTE:** DO NOT ACTUALLY DO THIS ON THE JETSON NANO AS IT WILL CORRUPT SYMLINKS TO CUDNN LIBRARIES (INSTALLATION STEPS ARE FOR GENERIC LINUX MACHINES).<BR />
IF ACTUALLY DONE THIS AND SYMLINKS BROKEN WITH ERROR MESSAGE ```ImportError: libcudnn.so.7: cannot open shared object file: No such file or directory``` FOLLOW THE FIRST ANSWER IN THIS QUESTION https://stackoverflow.com/questions/49656725/importerror-libcudnn-so-7-cannot-open-shared-object-file-no-such-file-or-dire

**DOWNLOAD LINK:** [NVIDIA cuDNN](https://developer.nvidia.com/cudnn)
**SELECT:** Archived cuDNN Releases -> Download cuDNN v7.6.3 for CUDA 10.0 -> cuDNN Library for Linux

**INSTALLATION:**
```bash
tar -xzvf cudnn-10.2-linux-x64 + TAB
sudo cp cuda/include/cudnn.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*

nano ~/.bashrc
export CUDA_HOME=/usr/local/cuda
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64
export PATH=$PATH:$CUDA_HOME/bin
source ~/.bashrc
```

### CHECK CUDNN VERSION
```bash
cat /usr/local/cuda/include/cudnn.h | grep CUDNN_MAJOR -A 2
```



## Tensorflow GPU 2.0.0
**GUIDE:** [Installing TensorFlow for Jetson Platform](https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html)<br />

**NOTE1:** Tensorflow GPU 2.0.0 is compatible with CUDA 10.0 (on Jetson Nano via JetPack 4.2), and cuDNN 7.5.0 (working combination)<br />

**NOTE2:** In guide, for step[ 3. Installing TensorFlow](https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html#install) use the following command to install the latest version of TensorFlow supported by a particular version of JetPack (Jetpack 4.2.x in this case):
```
sudo pip3 install --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v42 tensorflow-gpu
```
Returns: Successfully installed tensorflow-gpu-2.0.0+nv19.11.tf2

PyPi tensorflow-gpu project: https://pypi.org/project/tensorflow-gpu/



### CUDA 10.0 (Installed by default in Jetpack 4.2.1, currently no way to successfully install and use a version other than that provided by Jetpack)
```bash
cat /usr/local/cuda/version.txt OR nvcc --version
```

### TENSOR_RT:
```bash
dpkg -l | grep nvinfer
```

[TensorRT v5.1.5.0 Developer Guide](https://docs.nvidia.com/deeplearning/tensorrt/archives/tensorrt-515/tensorrt-developer-guide/index.html) found as a [link in TensorRT archives under 5.1.5](https://docs.nvidia.com/deeplearning/tensorrt/archives/index.html)<br />
[TensorRT v5.1.5.0 API Documentation](https://docs.nvidia.com/deeplearning/tensorrt/archives/tensorrt-515/tensorrt-api/c_api/annotated.html)<br />
The corresponding header files containing the API can be found in `/usr/include/aarch64-linux-gnu/` and the useful headers are `NvInfer.h`, `NvOnnxParser.h`, `NvInferPlugin.h`, `NvOnnxConfig.h`, `NvOnnxParserRuntime.h`, `NvUtils.h`<br />
[Sample Inference from ONNX application using TensorRT v7](https://github.com/NVIDIA/TensorRT/tree/master/samples/opensource/sampleOnnxMNIST) This is useful for the README<br />
The corresponding sample application for TensorRT v5.1.5 can be found preinstalled with Jetpack at `/usr/src/tensorrt/samples/sampleOnnxMNIST/`<br />

### CHECK DISTRO EQUIVALENT OF WHAT IS INSTALLED IN JETPACK:
```bash
uname -m && cat /etc/*release
```
### CHECK VERSION OF KERNEL ON SYSTEM:
```bash
uname -r
```
### CHECK TENSORFLOW IS USING GPU:
```python
python3
import tensorflow as tf
print(tf.__version__)
tf.test.is_gpu_available()
tf.test.gpu_device_name()
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
```

### INCREASING SYSTEM SWAP SIZE (TO 4GB)
```
cd ~
git clone https://github.com/JetsonHacksNano/resizeSwapMemory
cd resizeSwapMemory
./setSwapMemorySize.sh -g 4
```

## Converting TF SavedModel to ONNX format
https://github.com/onnx/tensorflow-onnx<br />
**Main command:** `python -m tf2onnx.convert --saved-model tensorflow-model-path --output model.onnx`

## Jetson Nano Additional Useful Links
https://elinux.org/Jetson_Nano<br />
https://www.jetsonhacks.com/category/jetson-nano/<br />
https://www.pyimagesearch.com/2019/05/06/getting-started-with-the-nvidia-jetson-nano/<br />

## Blob Tracking Demo Project
This could be implemented as an extension to this project, used for tracking vehicles across the scene.<br />
https://github.com/xfgryujk/VehicleDetection

## Setting up Cloud Hosted MongoDB Instance
[Create account at MongoDB Atlas](cloud.mongodb.com)<br />
MongoDB version should be 4.2, but can check via Cluster->...->Edit Configuration->Additional Settings->MongoDB <Version><br />

## Setting up MongoDB C/C++ Drivers on Linux
[Check drivers & MongoDB compatibility versions](https://docs.mongodb.com/drivers/cxx#compatibility) Instructions below are for MongoDB v4.2<br />
[Download MongoDB-C driver v1.15.0](https://github.com/mongodb/mongo-c-driver/releases/tag/1.15.0)<br />
[Install MongoDB-C driver via tarball](http://mongoc.org/libmongoc/current/installing.html#building-from-a-release-tarball)<br />
[Download MongoDB-C++ driver v3.5.1](https://github.com/mongodb/mongo-cxx-driver/releases/tag/r3.5.1)<br />
[Install MongoDB-C++ driver via *Step 4*](http://mongocxx.org/mongocxx-v3/installation/) **NOTE1:** must be in build/ dir **NOTE2:** run `sudo make` instead of `make`<br />
Add `/usr/local/lib` to LD_LIBRARY_PATH enivronment variable to find libmongocxx dynamic libraries.<br />

## Using MongoDB C++ Driver in Application
[Connecting to MongoDB Atlas Cloud](https://docs.mongodb.com/drivers/cxx#connect-to-mongodb-atlas)<br />
[Mongocxx Turorial](http://mongocxx.org/mongocxx-v3/tutorial/)<br />
See also: [Working with BSON](http://mongocxx.org/mongocxx-v3/working-with-bson/)<br />
[Mongocxx 3.5.1 API Documentation](http://mongocxx.org/api/mongocxx-3.5.1/)<br />

## Terminal Utility for Monitoring CPU and GPU performance
[Jetson Stats](https://github.com/rbonghi/jetson_stats)<br />
`sudo -H pip install -U jetson-stats`<br />
`sudo jtop`

# 2. Main Code
The current main working code for this project is located in the following places:

## C++ Code (Inference and Dataset Creation)

1. **Main Demo Code:** `final-year-project/cpp_code/inference/currentcode/inferencer`<br />
    **To compile:** `make clean` then `make`<br />
    **To run:** `./../bin/AppInfer`<br />

2. **Create Dataset Images Code:** `final-year-project/cpp_code/inference/currentcode/collector`<br />
    **To compile:** `make clean` then `make`<br />
    **To run:** `./loopAll.sh`<br />

3. **Main Debug Code:** `final-year-project/cpp_code/opencv/currentcode/src/getImgs.cpp`<br />
    **To compile:** `./compileGetImgs.sh`<br />
    **To run:** `./runGetImgs.sh`<br />

## Python Code (Training CNN)

1. **Main Custom CNN Create and Training Code:** `final-year-project/python_code/tf_keras/mycnn.py`

2. **Hyperparameters JSON file for Custom CNN:** `final-year-project/python_code/tf_keras/hparams_mycnn.py`

3. **Main Transfer Learned CNN Code:** `final-year-project/python_code/tf_keras/transferlearn.py`

4. **Hyperparameters JSON file for Transfer Learned CNN:** `final-year-project/python_code/tf_keras/hparams_tlearn.py`

5. **Creating needed structure for dataset in the `final-year-project-results` folder:** `final-year-project/python_code/tf_keras/createtesttrainsplit.py`

6. **Running inference on a test batch using a saved model:** `final-year-project/python_code/tf_keras/mycnnpredict.py`

## Model in ONNX format

The current working model that has been converted to ONNX format is `final-year-project/saved_models/bigger_private_ds_v5.onnx`

## Recording videos from YouTube livestream

The currently used bash script to save videos is: `final-year-project/cpp_code/opencv/currentcode/runGetVideo.sh` with the core command in the form:<br />
`timeout 15m streamlink --hls-live-restart -o video1.mp4 $(youtube-dl -f 96 -g https://www.youtube.com/watch?v=WsYtosQta5Y)`

# 3. Results and Thesis

The results are located in the **final-year-project-results** repository (https://gitlab.com/andreib123/final-year-project-results/-/tree/master).<br />

- A demo video of the main application running as a single threaded application can be found at https://gitlab.com/andreib123/final-year-project-results/-/blob/master/reporting/videos/lagrange2_singlethread_demo.mp4<br />

- A demo video of the main application running as a multithreaded application can be found at https://gitlab.com/andreib123/final-year-project-results/-/blob/master/reporting/videos/lagrange2_multithread_demo.mp4<br />

- A **presentation** of the entire project can be found at https://gitlab.com/andreib123/final-year-project-results/-/blob/master/reporting/nuig/my_reports/FinalPresentation.pptx<br />

- The **thesis** for the project can be found at https://gitlab.com/andreib123/final-year-project-results/-/blob/master/reporting/nuig/my_reports/Thesis_AndreiBarcovschi_16451004.pdf<br />

# 4. Web Application

A web application in Node.js was also written to have a frontend to access the results. The project can be found at https://gitlab.com/andreib123/final-year-project-web<br />
