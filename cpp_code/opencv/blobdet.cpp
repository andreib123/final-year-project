#include "opencv2/opencv.hpp"

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    // Read image
    Mat im = imread( "videos/blurred_dilatedroi.jpg", IMREAD_GRAYSCALE );
    Mat invertedIm;
    bitwise_not(im, invertedIm);

    // Setup SimpleBlobDetector parameters.
    SimpleBlobDetector::Params params;

    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 1500;

    // Set up detector with params
    Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
    
    // Detect blobs.
    std::vector<KeyPoint> keypoints;
    detector->detect( invertedIm, keypoints);
    
    // Draw detected blobs as red circles.
    // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
    Mat im_with_keypoints;
    drawKeypoints( invertedIm, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    
    // Show blobs
    imshow("keypoints", im_with_keypoints );
    waitKey(0);
}