t="15"
c="1"
hrs="3"
mins15cnt=$(( hrs*4 ))
url="https://www.youtube.com/watch?v=WsYtosQta5Y" # LaGrange, KY.

while [ $c -le $mins15cnt ]; do
        echo "creating video" final-year-project-results/videos/livecam/lagrange"$c"_"$t"m_best.mp4
        timeout "$t"m streamlink --hls-live-restart -o ../../../../final-year-project-results/videos/livecam/lagrange"$c"_"$t"m_best.mp4 $(youtube-dl -f 96 -g "$url")
        c=$[$c+1]
done

