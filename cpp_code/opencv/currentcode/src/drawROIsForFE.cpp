#include <opencv2/opencv.hpp>
#include <iostream>

void debugSaveImg(const cv::Mat &frame) {
    static int n = 1;
    char str[64];
    std::sprintf(str, "img_fe%d.jpeg", n);
    cv::imwrite(str, frame);
    n++;
}

int main() {
    cv::Mat image = cv::imread("frame1.jpeg"); // load empty intersection frame.
    if(! image.data ) // Check for invalid input
    {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    // copy pasted code from getImgs.cpp
    int toTop_xtop = 1300;
    int toTop_ytop = 376;
    int toTop_xbot = 1517;
    int toTop_ybot = 539;
    int toBottom_xtop = 1043;
    int toBottom_ytop = 677;
    int toBottom_xbot = 1408;
    int toBottom_ybot = 1060;
    int toLeft_xtop = 470;
    int toLeft_ytop = 492;
    int toLeft_xbot = 920;
    int toLeft_ybot = 691;
    int toRight_xtop = 1419;
    int toRight_ytop = 459;
    int toRight_xbot = 1827;
    int toRight_ybot = 652;
    cv::Rect rect[4];
    rect[0] = cv::Rect(toTop_xtop, toTop_ytop, toTop_xbot - toTop_xtop, toTop_ybot - toTop_ytop);
    rect[1] = cv::Rect(toBottom_xtop, toBottom_ytop, toBottom_xbot - toBottom_xtop, toBottom_ybot - toBottom_ytop);
    rect[2] = cv::Rect(toLeft_xtop, toLeft_ytop, toLeft_xbot - toLeft_xtop, toLeft_ybot - toLeft_ytop);
    rect[3] = cv::Rect(toRight_xtop, toRight_ytop, toRight_xbot - toRight_xtop, toRight_ybot - toRight_ytop);
    
    cv::Mat img_clean = image.clone();
    for(int i = 1; i < 16; i++) {
        for(int b=1, r=0; r < 4; b <<= 1, r++) {
            if(i & b) cv::rectangle(img_clean, rect[r], cv::Scalar(0, 255, 255),    3);
        }
        debugSaveImg(img_clean);
        img_clean = image.clone();
    }

    return 0;
}
