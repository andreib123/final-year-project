#include <opencv2/opencv.hpp>

int main() {
    std::string video = "https://manifest.googlevideo.com/api/manifest/hls_playlist/expire/1595526418/ei/sngZX7PAIa20xN8Pn6W0oA0/ip/79.97.160.11/id/WsYtosQta5Y.1/itag/96/source/yt_live_broadcast/requiressl/yes/ratebypass/yes/live/1/goi/160/sgoap/gir%3Dyes%3Bitag%3D140/sgovp/gir%3Dyes%3Bitag%3D137/hls_chunk_host/r1---sn-q0c7rn76.googlevideo.com/playlist_duration/30/manifest_duration/30/vprv/1/playlist_type/DVR/initcwndbps/16070/mh/Ri/mm/44/mn/sn-q0c7rn76/ms/lva/mv/m/mvi/1/pl/17/dover/11/keepalive/yes/fexp/23883097/mt/1595504715/disable_polymer/true/sparams/expire,ei,ip,id,itag,source,requiressl,ratebypass,live,goi,sgoap,sgovp,playlist_duration,manifest_duration,vprv,playlist_type/sig/AOq0QJ8wRQIhALsf081_TRYtCmXaOAlcMO02Gx51YqB-rFfhgzekmAw2AiBKOeh9ZoyneCWg62nWCpjvmxgWiAOzoM2jYNWdBaB_8w%3D%3D/lsparams/hls_chunk_host,initcwndbps,mh,mm,mn,ms,mv,mvi,pl/lsig/AG3C_xAwRQIgEJmMIKSbWjJLo3gG4afSL2HZpNMxMbms7RTe4HEHueMCIQC0HmS00esejdrX_0yA4nAWzJt80sDN2PZtkuTaSzayoA%3D%3D/playlist/index.m3u8";

    cv::Mat frame;
    cv::VideoCapture capture(video);
    if (!capture.isOpened()){
        //error in opening the video input
        std::cerr << "Unable to open: " << video << std::endl;
        return -1;
    }
    while (true) {
        capture >> frame;

        if(frame.empty())
            break;
        
        cv::imshow("orig", frame);
        cv::waitKey(1);
    }

    return 0;
}