
#include <iostream>
#include <sstream>
#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
using namespace cv;
using namespace std;

bool printCoords = false;

typedef struct {
    cv::Point pt1;
    cv::Point pt2;
} rect_t;

rect_t grc = {}; // grc = global rectangle.

void mouseCallBack(int event, int x, int y, int flags, void* userdata) {
    if(event == EVENT_LBUTTONDOWN) {//cout << "Left button of the mouse is pressed down - position (" << x << "," << y << ")" << endl;
        grc.pt1.x = x;
        grc.pt1.y = y;
        grc.pt2.x = grc.pt2.y = 0;
    }
    else if(event == EVENT_LBUTTONUP) { // cout << "Left button of the mouse is released - position (" << x << "," << y << ")" << endl;
        grc.pt2.x = x;
        grc.pt2.y = y;
        printCoords = true;
    }
}

int main(int argc, char* argv[])
{
    string video = string(argv[1]);
    bool playVid = true;
    
    Mat frame;

    VideoCapture capture(video);
    if (!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open: " << video << endl;
        return 0;
    }

    namedWindow("Original frame", 1);
    setMouseCallback("Original frame", mouseCallBack, NULL);

    while (true) {
        if(playVid)
            capture >> frame;

        if (frame.empty())
            break;

        // get the frame number and write it on the current frame.
        rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                  cv::Scalar(255,255,255), -1);
        stringstream ss;
        ss << capture.get(CAP_PROP_POS_FRAMES);
        string frameNumberString = ss.str();
        putText(frame, frameNumberString.c_str(), cv::Point(15, 15),
                FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));

        if(grc.pt2.x && grc.pt2.y) {
            rectangle(frame, grc.pt1, grc.pt2, cv::Scalar(0, 255, 0), 2);
            if(printCoords) {
                cout << "==========================================================================================" << endl;
                cout << "top left corner of rec: " << "(" << grc.pt1.x << "," << grc.pt1.y << ")" << endl;
                cout << "bottom right corner of rec: " << "(" << grc.pt2.x << "," << grc.pt2.y << ")" << endl;
                printCoords=false;
            }
        }
        

        // show needed frames.
        imshow("Original frame", frame);

        //get the input from the keyboard
        int keyboard = waitKey(30);

        // escape for exit.
        if (keyboard == 27)
            break;
        // space bar for pause.
        if (keyboard == 32) {
            playVid = !playVid; 
        }
    }

    return 0;
}