/*
    This is a test program for creating a database of car images from any folder in the ../../videos folder.

    The main steps involved are:
        - open video file.
        - create MOG2 background subtractor (BGS).
        - perform morphological operations on frames produced by BGS
          to reduce noise and dilate blobs.
        - remove shadow detection (done when shadows are very strong and classified as foreground by BGS.
        - create cropped regions of interest (ROI) where needed to detect cars.
        - save crop of original frame if the number of foreground pixels exceeds a threshold in the ROIs.
    
    This code is independent of the lane driving conventions of the country that the video recording is of.

    Images from the Top and Left ROIs should be delayed a few frames before being saved as originally they are saved too early for some reason.

    NOTE1: THIS IS THE TEST VERSION WITH PLENTY OF DEBUGGING FUNCTIONALITY. THERE IS A BETTER STRUCTURED AND MODULATED VERSION
           CALLED AppDsCollect.cpp for production in final-year-project/cpp_code/inference/currentcode/collector/.

    NOTE2: the intersection is taken as the focal point of the ROIs, so the word "to" implies "to the intersection", i.e. vehicle entering intersection.
           Similarly, "from" implies from the intersection, i.e. vehicle is leaving the intersection. 
*/

#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>
// #include <opencv2/imgcodecs.hpp>
// #include <opencv2/imgproc.hpp>
// #include <opencv2/videoio.hpp>
// #include <opencv2/highgui.hpp>
// #include <opencv2/video.hpp>

// using namespace cv;
// using namespace std;

class FrontalLane {

    bool top; // left = ROI is @ top.
    bool to; // to = driving into intersection.
    bool vehicleSaved;
    bool saveTopROI; // used for saving top ROI frame N frames later.
    int waitFramesAmt; // amount of frames to wait before saving frame if top ROI.
    int waitNFramesCounter;
    int vehiclesExited; // number of vehicles that have exited the intersection from this ROI.
    int detectedCounter;
    bool vehicleDetected;

    // ints that are params to constr
    int topH, botH, thTop, thCen, thBot;

    // derived ints
    int roiW, roiH, topArea, cenArea, botArea, whPixThTop, whPixThCen, whPixThBot;
    int imCounter; 
    std::string roiFolder, imNameSeries, fullFrameFolder, fullFramePath;
    
    cv::Range roiRangeX, roiRangeY;  // OpenCV range object for the ROI.

    cv::Rect a;

    public:

        FrontalLane(const cv::Rect a, bool t, bool to, const char *folder, const char *series,
            const cv::Range &roix, const cv::Range &roiy,
            int toph, int both, int thtop, int thcen, int thbot)
            :
            a(a), top(t), to(to), roiFolder(folder), imNameSeries(series),
            roiRangeX(roix), roiRangeY(roiy),
            topH(toph), botH(both), thTop(thtop), thCen(thcen), thBot(thbot)
        {
            roiW = roiRangeX.end - roiRangeX.start;
            roiH = roiRangeY.end - roiRangeY.start;
            topArea = roiW*topH;
            cenArea = roiW*(roiH-topH-botH);
            botArea = roiW*botH;
            whPixThTop = topArea * thTop / 100;
            whPixThCen = cenArea * thCen / 100;
            whPixThBot = botArea * thBot / 100;
            vehicleSaved = false;
            saveTopROI = false;
            fullFrameFolder = top ? "fromtop/" : "frombottom/";
            fullFramePath = roiFolder + "../full_frames/frontal/" + fullFrameFolder;
            imCounter = 1;
            waitNFramesCounter = 0;
            waitFramesAmt = 0;
            vehiclesExited = 0;
            detectedCounter = 0;
            vehicleDetected = false;
        }

        // called on each frame.
        void detectVehicle(cv::Mat &frame, cv::Mat &roiDilated) {
            if(!to) {
                cv::rectangle(frame, cv::Point(a.x + 5, a.y + a.height - 35), cv::Point(a.x + 200, a.y + a.height - 5), cv::Scalar(255,255,255), -1);
                cv::putText(frame,  "exited: " +  std::to_string(vehiclesExited), cv::Point(a.x + 10, a.y + a.height - 10),   cv::FONT_HERSHEY_DUPLEX, 1,  cv::Scalar::all(0), 1, 8, false);
                //          drawOn, toDraw,                                 bottomLeftStart,        style,                           size, colour,               thickness, lineType. 
            }
            if(saveTopROI) waitNframesThenSave(frame); // calls waitNframesThenSave repeatedly until saveTopROI flag is false.
            // local variables
            cv::Mat segTop = roiDilated(cv::Range(0, topH), cv::Range(0, roiW));
            cv::Mat segCenter = roiDilated(cv::Range(topH, roiH-botH), cv::Range(0, roiW));
            cv::Mat segBottom = roiDilated(cv::Range(roiH-botH, roiH), cv::Range(0, roiW));

            int whitePixelsTop = countNonZero(segTop);
            double percWhiteTop = whitePixelsTop * 100.0 / topArea;
            
            int whitePixelsCenter = countNonZero(segCenter);
            double percWhiteCenter = whitePixelsCenter * 100.0 / cenArea;
            int whitePixelsBottom = countNonZero(segBottom);
            double percWhiteBottom = whitePixelsBottom * 100.0 / botArea;

            // std::cout << "Percentage white pixels in TOP: " << percWhiteTop << std::endl;
            // std::cout << "Percentage white pixels in CENTER: " << percWhiteCenter << std::endl;
            // std::cout << "Percentage white pixels in BOTTOM: " << percWhiteBottom << std::endl;
            // std::cout << "-----------------------------------------------------------------" << std::endl;

            if(to) {
                trySaveImage(frame, whitePixelsTop, whitePixelsCenter, whitePixelsBottom);
            }
            else {
                tryIncreaseExitCnt(whitePixelsTop, whitePixelsCenter, whitePixelsBottom);
            }
            if(vehicleDetected) detectedCounter++;
        }

        void trySaveImage(cv::Mat &frame, int whitePixelsTop, int whitePixelsCenter, int whitePixelsBottom) {
            // segTop and segBottom both empty (below their thresholds of vehicles present).
            if((whitePixelsTop < whPixThTop) && (whitePixelsBottom < whPixThBot)) {
                // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleSaved = false).
                if((whitePixelsCenter > whPixThCen) && (vehicleSaved == false)) {
                    // for a top ROI object, save a frame N frames later.
                    if(top) { 
                        saveTopROI = true; // will call waitNframesThenSave on next frame.
                    }
                    // save ROI with vehicle in the center as image immediately if bottomROI.
                    else {

                        std::string imName = "vehicle" + imNameSeries + std::to_string(imCounter) + ".jpeg";
                        cv::imwrite(roiFolder + imName, frame(roiRangeY, roiRangeX));
                        cv::imwrite(fullFramePath + imName, frame); // saves the corresponding entire from for this ROI capture. Comment out if not needed.
                        imCounter++;
                        vehicleSaved = true;
                        vehicleDetected = true;
                    }
                }
            } // vehicle is leaving the ROI.
            // top frontal ROI.
            else if(top && detectedCounter) {
                // entering intersection.
                if(whitePixelsBottom > whPixThBot) {
                    vehicleSaved = false;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
            // bottom frontal ROI.
            else if(detectedCounter){
                // entering intersection.
                if(whitePixelsTop > whPixThTop) {
                    vehicleSaved = false;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
        }

        void tryIncreaseExitCnt(int whitePixelsTop, int whitePixelsCenter, int whitePixelsBottom) {
            // segTop and segBottom both empty (below their thresholds of vehicles present)
            if((whitePixelsTop < whPixThTop) && (whitePixelsBottom < whPixThBot)) {
                // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleSaved = false)
                if((whitePixelsCenter > whPixThCen) && (vehicleDetected == false)) {
                    vehicleDetected = true;
                }
            } // vehicle is leaving the ROI
            // if ROI is for vehicles leaving the intersection from this ROI, then increase total count of number of vehicles that left from this ROI.
            // top frontal ROI.
            else if(top && detectedCounter) {
                // leaving intersection.
                if((whitePixelsTop > whPixThTop)) {
                    vehiclesExited++;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
            // bottom frontal ROI.
            else if(detectedCounter) {
                // leaving intersection.
                if((whitePixelsBottom > whPixThBot)) {
                    vehiclesExited++;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
        }

        void waitNframesThenSave(cv::Mat &frame) {
            if(waitNFramesCounter < waitFramesAmt) {
                waitNFramesCounter++;
                return;
            }

            saveTopROI = false;
            // save ROI with vehicle in the center as image
            std::string imName = "vehicle" + imNameSeries + std::to_string(imCounter) + ".jpeg";
            cv::imwrite(roiFolder + imName, frame(roiRangeY, roiRangeX));
            cv::imwrite(fullFramePath + imName, frame);  // saves the corresponding entire from for this ROI capture. Comment out if not needed.
            imCounter++;
            vehicleSaved = true;
            vehicleDetected = true;
            detectedCounter++;
            waitNFramesCounter = 0;
            return;
        }

        int getRoiW() {
            return roiW;
        }

        int getRoiH() {
            return roiH;
        }

        int getTopH() {
            return topH;
        }

        int getBotH() {
            return botH;
        }
};

class LateralLane {

    bool left; // left = ROI is for left side.
    bool to; // to = driving into intersection.
    bool vehicleSaved;
    bool saveLeftROI; // used for saving left ROI frame N frames later.
    int waitFramesAmt; // amount of frames to wait before saving frame if top ROI.
    int waitNFramesCounter;
    int vehiclesExited; // number of vehicles that have exited the intersection from this ROI.
    int detectedCounter;
    bool vehicleDetected;

    // ints that are params to constr
    int leftW, rightW, thLeft, thCen, thRight;

    // derived ints
    int roiW, roiH, leftArea, cenArea, rightArea, whPixThLeft, whPixThCen, whPixThRight;
    int imCounter; 
    std::string roiFolder, imNameSeries, fullFrameFolder, fullFramePath;
    cv::Range roiRangeX, roiRangeY;  // OpenCV range object for the ROI.

    cv::Rect a;

    public:

        LateralLane(const cv::Rect a, bool l, bool to, const char *folder, const char *series,
            const cv::Range &roix, const cv::Range &roiy,
            int leftw, int rightw, int thleft, int thcen, int thright)
            :
            a(a), left(l), to(to), roiFolder(folder), imNameSeries(series),
            roiRangeX(roix), roiRangeY(roiy),
            leftW(leftw), rightW(rightw), thLeft(thleft), thCen(thcen), thRight(thright)
        {
            roiW = roiRangeX.end - roiRangeX.start;
            roiH = roiRangeY.end - roiRangeY.start;
            leftArea = roiH*leftW;
            cenArea = roiH*(roiW-leftW-rightW);
            rightArea = roiH*rightW;
            whPixThLeft = leftArea * thLeft / 100;
            whPixThCen = cenArea * thCen / 100;
            whPixThRight = rightArea * thRight / 100;
            vehicleSaved = false;
            saveLeftROI = false;
            fullFrameFolder = left ? "fromleft/" : "fromright/";
            fullFramePath = roiFolder + "../full_frames/lateral/" + fullFrameFolder;
            imCounter = 1;
            waitNFramesCounter = 0;
            waitFramesAmt = 0;
            vehiclesExited = 0;
            detectedCounter = 0;
            vehicleDetected = false;
        }

        // called on each frame.
        void detectVehicle(cv::Mat &frame, cv::Mat &roiDilated) {
            if(!to) {
                cv::rectangle(frame, cv::Point(a.x + 5, a.y + a.height - 35), cv::Point(a.x + 200, a.y + a.height - 5), cv::Scalar(255,255,255), -1);
                cv::putText(frame,  "exited: " +  std::to_string(vehiclesExited), cv::Point(a.x + 10, a.y + a.height - 10),   cv::FONT_HERSHEY_DUPLEX, 1,  cv::Scalar::all(0), 1, 8, false);
                //          drawOn, toDraw,                                 bottomLeftStart,        style,                           size, colour,               thickness, lineType. 
            }
            if(saveLeftROI) waitNframesThenSave(frame); // calls waitNframesThenSave repeatedly until saveLeftROI flag is false.
            // local variables
            cv::Mat segLeft = roiDilated(cv::Range(0, roiH), cv::Range(0, leftW));
            cv::Mat segCenter = roiDilated(cv::Range(0, roiH), cv::Range(leftW, roiW-leftW));
            cv::Mat segRight = roiDilated(cv::Range(0, roiH), cv::Range(roiW-rightW, roiW));

            int whitePixelsLeft = cv::countNonZero(segLeft);
            double percWhiteLeft = whitePixelsLeft * 100.0 / leftArea;
            int whitePixelsCenter = cv::countNonZero(segCenter);
            double percWhiteCenter = whitePixelsCenter * 100.0 / cenArea;
            int whitePixelsRight = cv::countNonZero(segRight);
            double percWhiteRight = whitePixelsRight * 100.0 / rightArea;

            // std::cout << "Percentage white pixels in LEFT: " << percWhiteLeft << std::endl;
            // std::cout << "Percentage white pixels in CENTER: " << percWhiteCenter << std::endl;
            // std::cout << "Percentage white pixels in RIGHT: " << percWhiteRight << std::endl;
            // std::cout << "-----------------------------------------------------------------" << std::endl;
            if(to) {
                trySaveImage(frame, whitePixelsLeft, whitePixelsCenter, whitePixelsRight);
            }
            else {
                tryIncreaseExitCnt(frame, whitePixelsLeft, whitePixelsCenter, whitePixelsRight);
            }
            if(vehicleDetected) detectedCounter++;
        }

        void trySaveImage(const cv::Mat &frame, int whitePixelsLeft, int whitePixelsCenter, int whitePixelsRight) {
            // segLeft and segRight both empty (below their thresholds of vehicles present)
            if((whitePixelsLeft < whPixThLeft) && (whitePixelsRight < whPixThRight)) {
                // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleSaved = false)
                if((whitePixelsCenter > whPixThCen) && (vehicleSaved == false)) {
                    // for a left ROI object, save a frame N frames later.
                    if(left) { 
                        saveLeftROI = true; // will call waitNframesThenSave on next frame.
                    }
                    // save ROI with vehicle in the center as image immediately if rightROI.
                    else {
                        std::string imName = "vehicle" + imNameSeries + std::to_string(imCounter) + ".jpeg";
                        cv::imwrite(roiFolder + imName, frame(roiRangeY, roiRangeX));
                        cv::imwrite(fullFramePath + imName, frame); // saves the corresponding entire from for this ROI capture. Comment out if not needed.
                        imCounter++;
                        
                        vehicleSaved = true;
                        vehicleDetected = true;
                    }
                }
            } // vehicle is leaving the ROI
            // if ROI is for vehicles leaving the intersection from this ROI, then increase total count of number of vehicles that left from this ROI.
            // left lateral ROI.
            else if(left && detectedCounter) {
                // entering intersection.
                if(whitePixelsRight > whPixThRight) {
                    vehicleSaved = false;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
            // right lateral ROI.
            else if(detectedCounter){
                // entering intersection.
                if(whitePixelsLeft > whPixThLeft) {
                    vehicleSaved = false;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
        }

        void tryIncreaseExitCnt(const cv::Mat &frame, int whitePixelsLeft, int whitePixelsCenter, int whitePixelsRight) {
            // segLeft and segRight both empty (below their thresholds of vehicles present)
            if((whitePixelsLeft < whPixThLeft) && (whitePixelsRight < whPixThRight)) {
                // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleSaved = false)
                if((whitePixelsCenter > whPixThCen) && (vehicleDetected == false)) {
                    vehicleDetected = true;
                }
            } // vehicle is leaving the ROI
            // if ROI is for vehicles leaving the intersection from this ROI, then increase total count of number of vehicles that left from this ROI.
            // left lateral ROI.
            else if(left && detectedCounter) {
                // leaving intersection.
                if((whitePixelsLeft > whPixThLeft)) {
                    vehiclesExited++;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
            // right lateral ROI.
            else if(detectedCounter) {
                // leaving intersection.
                if((whitePixelsRight > whPixThRight)) {
                    vehiclesExited++;
                    detectedCounter = 0;
                    vehicleDetected = false;
                }
            }
        }


        void waitNframesThenSave(cv::Mat &frame) {
            if(waitNFramesCounter < waitFramesAmt) {
                waitNFramesCounter++;
                return;
            }
            saveLeftROI = false;
            // save ROI with vehicle in the center as image
            std::string imName = "vehicle" + imNameSeries + std::to_string(imCounter) + ".jpeg";
            cv::imwrite(roiFolder + imName, frame(roiRangeY, roiRangeX));
            cv::imwrite(fullFramePath + imName, frame); // saves the corresponding entire from for this ROI capture. Comment out if not needed.
            imCounter++;
            vehicleSaved = true;
            vehicleDetected = true;
            detectedCounter++;
            waitNFramesCounter = 0;
            return;
        }

        int getRoiW() {
            return roiW;
        }

        int getRoiH() {
            return roiH;
        }

        int getLeftW() {
            return leftW;
        }

        int getRightW() {
            return rightW;
        }
};

void drawSegmentsInROI(bool original, cv::Mat &roiMatObj, const cv::Point &line1startCoord, const cv::Point &line1endCoord,
                        const cv::Point &line2startCoord, const cv::Point &line2endCoord) {
    if(original) {
        cv::line(roiMatObj, line1startCoord, line1endCoord, cv::Scalar(0,255,0), 1);
        cv::line(roiMatObj, line2startCoord, line2endCoord, cv::Scalar(0,255,0), 1);
    }
    else {
        cv::line(roiMatObj, line1startCoord, line1endCoord, cv::Scalar(255,255,255), 1);
        cv::line(roiMatObj, line2startCoord, line2endCoord, cv::Scalar(255,255,255), 1);
    }
}

typedef struct {
    cv::Point pt1;
    cv::Point pt2;
} line_t;

line_t gln = {};  // gln = global line

bool printCoords = false;

void mouseCallBack(int event, int x, int y, int flags, void* userdata) {
    if(event == cv::EVENT_LBUTTONDOWN) {
        std::cout << "Left button of the mouse is pressed down - position (" << x << "," << y << ")" << std::endl;
        gln.pt1.x = x;
        gln.pt1.y = y;
        gln.pt2.x = gln.pt2.y = 0;
    }
    else if(event == cv::EVENT_LBUTTONUP) { 
        std::cout << "Left button of the mouse is released - position (" << x << "," << y << ")" << std::endl;
        gln.pt2.x = x;
        gln.pt2.y = y;
        printCoords = true;
    }
}

void debugSaveImg(const cv::Mat &frame) {
    static int n = 1;
    char str[64];
    std::sprintf(str, "frame%d.jpeg", n);
    cv::imwrite(str, frame);
    n++;
}

int main(int argc, char* argv[]) {

    const char* imgSuffix = argv[1]; // p[0].
    std::string video = std::string(argv[2]);
    bool playVid = true;
    int key;
    std::string str;

    // FRONTAL TO TOP SETUP (DESIGNED TO CAPTURE VEHICLES DRIVING *TO* THE INTERSECTION, I.E. ENTERING THE INTERSECTION).
    cv::Mat roiDilatedToTop;
    // define ROI. top left corner of roi = 1451,314, bottom right = 1585,475.
    // Range #1 = (x)TopLeftROI, (x)BottomRightROI.
    // Range #2 = (y)TopLeftROI, (y)BottomRightROI.
    int toTop_xtop = 1300; // (x)TopLeftROI.
    int toTop_ytop = 376; // (y)TopLeftROI.
    int toTop_xbot = 1517; // (x)BottomRightROI.
    int toTop_ybot = 539; // (y)BottomRightROI.
    cv::Rect toTopRect(toTop_xtop, toTop_ytop, toTop_xbot - toTop_xtop, toTop_ybot - toTop_ytop);
    cv::Range toTopX = cv::Range(toTop_xtop, toTop_xbot);
    cv::Range toTopY = cv::Range(toTop_ytop, toTop_ybot);
    str = "tt"; str += imgSuffix;
    FrontalLane *toTopPtr = new FrontalLane(toTopRect, true, true, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), toTopX, toTopY, 25,   15,   7,    25,    10);
    //                                                                                                                                                 topH, botH, thTop, thCen, thBot

    // FRONTAL TO BOTTOM SETUP.
    cv::Mat roiDilatedToBottom;
    // define ROI. top left corner of roi = 1043,677, bottom right = 1408,1060.
    int toBottom_xtop = 1043;
    int toBottom_ytop = 677;
    int toBottom_xbot = 1408;
    int toBottom_ybot = 1060;
    cv::Rect toBottomRect(toBottom_xtop, toBottom_ytop, toBottom_xbot - toBottom_xtop, toBottom_ybot - toBottom_ytop);
    cv::Range toBottomX = cv::Range(toBottom_xtop, toBottom_xbot);
    cv::Range toBottomY = cv::Range(toBottom_ytop, toBottom_ybot);
    str = "tb"; str += imgSuffix;
    FrontalLane *toBottomPtr = new FrontalLane(toBottomRect, false, true, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), toBottomX, toBottomY, 50, 32, 15, 20, 8);

    // LATERAL TO LEFT SETUP
    cv::Mat roiDilatedToLeft;
    // define ROI. top left corner of roi = 2,424, bottom right = 314,608
    int toLeft_xtop = 470;
    int toLeft_ytop = 492;
    int toLeft_xbot = 920;
    int toLeft_ybot = 691;
    cv::Rect toLeftRect(toLeft_xtop, toLeft_ytop, toLeft_xbot - toLeft_xtop, toLeft_ybot - toLeft_ytop);
    cv::Range toLeftX = cv::Range(toLeft_xtop, toLeft_xbot);
    cv::Range toLeftY = cv::Range(toLeft_ytop, toLeft_ybot);
    str = "tl"; str += imgSuffix;
    LateralLane *toLeftPtr = new LateralLane(toLeftRect, true, true, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), toLeftX, toLeftY, 50,    47,     5,     25,    10);
    //                                                                                                                                                    leftW, rightW, thLeft, thCen, thRight

    // LATERAL TO RIGHT SETUP
    cv::Mat roiDilatedToRight;
    // define ROI. top left corner of roi = 1419,459, bottom right = 1827,652
    int toRight_xtop = 1419;
    int toRight_ytop = 459;
    int toRight_xbot = 1827;
    int toRight_ybot = 652;
    cv::Rect toRightRect(toRight_xtop, toRight_ytop, toRight_xbot - toRight_xtop, toRight_ybot - toRight_ytop);
    cv::Range toRightX = cv::Range(toRight_xtop, toRight_xbot);
    cv::Range toRightY = cv::Range(toRight_ytop, toRight_ybot);
    str = "tr"; str += imgSuffix;
    LateralLane *toRightPtr = new LateralLane(toRightRect, false, true, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), toRightX, toRightY, 40, 68, 10, 30, 30);

    // FRONTAL FROM TOP SETUP.
    cv::Mat roiDilatedFromTop;
    int fromTop_xtop = 1585;
    int fromTop_ytop = 314;
    int fromTop_xbot = 1719;
    int fromTop_ybot = 475;
    cv::Rect fromTopRect(fromTop_xtop, fromTop_ytop, fromTop_xbot - fromTop_xtop, fromTop_ybot - fromTop_ytop);
    cv::Range fromTopX = cv::Range(fromTop_xtop, fromTop_xbot);
    cv::Range fromTopY = cv::Range(fromTop_ytop, fromTop_ybot);
    str = "ft"; str += imgSuffix;
    FrontalLane *fromTopPtr = new FrontalLane(fromTopRect, true, false, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), fromTopX, fromTopY, 25,   27,   26,    18,    10);

    // FRONTAL FROM BOTTOM SETUP.
    cv::Mat roiDilatedFromBottom;
    int fromBottom_xtop = 558;
    int fromBottom_ytop = 677;
    int fromBottom_xbot = 923;
    int fromBottom_ybot = 1060;
    cv::Rect fromBottomRect(fromBottom_xtop, fromBottom_ytop, fromBottom_xbot - fromBottom_xtop, fromBottom_ybot - fromBottom_ytop);
    cv::Range fromBottomX = cv::Range(fromBottom_xtop, fromBottom_xbot);
    cv::Range fromBottomY = cv::Range(fromBottom_ytop, fromBottom_ybot);
    str = "tb"; str += imgSuffix;
    FrontalLane *fromBottomPtr = new FrontalLane(fromBottomRect, false, false, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), fromBottomX, fromBottomY, 50, 32, 20, 18, 10);

    // LATERAL FROM LEFT SETUP
    cv::Mat roiDilatedFromLeft;
    // define ROI. top left corner of roi = 2,424, bottom right = 314,608
    int fromLeft_xtop = 63;
    int fromLeft_ytop = 332;
    int fromLeft_xbot = 375;
    int fromLeft_ybot = 516;
    cv::Rect fromLeftRect(fromLeft_xtop, fromLeft_ytop, fromLeft_xbot - fromLeft_xtop, fromLeft_ybot - fromLeft_ytop);
    cv::Range fromLeftX = cv::Range(fromLeft_xtop, fromLeft_xbot);
    cv::Range fromLeftY = cv::Range(fromLeft_ytop, fromLeft_ybot);
    str = "fl"; str += imgSuffix;
    LateralLane *fromLeftPtr = new LateralLane(fromLeftRect, true, false, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), fromLeftX, fromLeftY, 50,    47,     15,     18,    10);
    //                                                                                                                                                                   leftW, rightW, thLeft, thCen, thRight

    // LATERAL FROM RIGHT SETUP
    cv::Mat roiDilatedFromRight;
    // define ROI. top left corner of roi = 1419,459, bottom right = 1827,652
    int fromRight_xtop = 1320;
    int fromRight_ytop = 550;
    int fromRight_xbot = 1827;
    int fromRight_ybot = 781;
    cv::Rect fromRightRect(fromRight_xtop, fromRight_ytop, fromRight_xbot - fromRight_xtop, fromRight_ybot - fromRight_ytop);
    cv::Range fromRightX = cv::Range(fromRight_xtop, fromRight_xbot);
    cv::Range fromRightY = cv::Range(fromRight_ytop, fromRight_ybot);
    str = "tr"; str += imgSuffix;
    LateralLane *fromRightPtr = new LateralLane(fromRightRect, false, false, "../../../../final-year-project-results/images/livecam/t/", str.c_str(), fromRightX, fromRightY, 40, 68, 15, 12, 30);
    
    cv::Mat frame, fgMask, fgMaskBlur, fgDilated; // different frames needed in main while loop.
    cv::Mat roiOriginalFromTop, roiOriginalFromBottom, roiOriginalFromLeft, roiOriginalFromRight; // different ROIs needed in main while loop.
    cv::Mat roiOriginalToTop, roiOriginalToBottom, roiOriginalToLeft, roiOriginalToRight; // different ROIs needed in main while loop.
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size( 6,6 )); // dilation element.

    time_t start, end; // start and end times for calculating FPS.
    int framecount = 0;
    
    //create Background Subtractor object
    cv::Ptr<cv::BackgroundSubtractor> pBackSub = cv::createBackgroundSubtractorMOG2();

    //VideoCapture capture(parser.get<String>("input"));
    cv::VideoCapture capture(video);
    if (!capture.isOpened()){
        //error in opening the video input
        std::cerr << "Unable to open: " << video << std::endl;
        return 0;
    }

    std::cout << "Original video frames per second : " << capture.get(cv::CAP_PROP_FPS) << std::endl;

    // used to draw lines in the ROI masks. Comment out all unneeded ones and only leave the original ROI and ROI mask for the ROI
    // you're trying to set the values for the segments for.
    // cv::namedWindow("Original Frame", 1);
    // cv::setMouseCallback("Original Frame", mouseCallBack, NULL);
    // namedWindow("FromTop ROI Blurred and Dilated", 1);
    // setMouseCallback("FromTop ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("FromBottom ROI Blurred and Dilated", 1);
    // setMouseCallback("FromBottom ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("FromLeft ROI Blurred and Dilated", 1);
    // setMouseCallback("FromLeft ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("FromRight ROI Blurred and Dilated", 1);
    // setMouseCallback("FromRight ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("ToTop ROI Blurred and Dilated", 1);
    // setMouseCallback("ToTop ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("ToBottom ROI Blurred and Dilated", 1);
    // setMouseCallback("ToBottom ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("ToLeft ROI Blurred and Dilated", 1);
    // setMouseCallback("ToLeft ROI Blurred and Dilated", mouseCallBack, NULL);
    // namedWindow("ToRight ROI Blurred and Dilated", 1);
    // setMouseCallback("ToRight ROI Blurred and Dilated", mouseCallBack, NULL);

    bool fsave = false;
    cv::Mat prevFrame;

    // ofstream outfile;
    // outfile.open("coords.txt", ios::trunc | ios::out); // open a file for writing, rewrite contents if present.

    // main while loop
    time(&start);
    while (true) {
        if(playVid)
            capture >> frame;

        if(fsave) {
            debugSaveImg(frame);
            cv::Mat frameDiff; 
            subtract(frame, prevFrame, frameDiff);
            debugSaveImg(frameDiff);
            fsave = false;
        }

        if(frame.empty())
            break;

        if(playVid) {
            //update the background model
            pBackSub->apply(frame, fgMask, 0.008); // learning rate = 0.008 [0,1]
        

            cv::blur(fgMask, fgMaskBlur, cv::Size( 9, 9 ), cv::Point(-1,-1) );
            cv::dilate(fgMaskBlur, fgDilated, element );
            cv::threshold(fgDilated, fgDilated, 128, 255, cv::THRESH_BINARY); // Remove the shadow parts and the noise
        

            roiDilatedFromTop = fgDilated(fromTopY, fromTopX);
            roiDilatedFromBottom = fgDilated(fromBottomY, fromBottomX);
            roiDilatedFromLeft = fgDilated(fromLeftY, fromLeftX);
            roiDilatedFromRight = fgDilated(fromRightY, fromRightX);
            roiDilatedToTop = fgDilated(toTopY, toTopX);
            roiDilatedToBottom = fgDilated(toBottomY, toBottomX);
            roiDilatedToLeft = fgDilated(toLeftY, toLeftX);
            roiDilatedToRight = fgDilated(toRightY, toRightX);
        }

        fromTopPtr->detectVehicle(frame, roiDilatedFromTop);
        fromBottomPtr->detectVehicle(frame, roiDilatedFromBottom);
        fromLeftPtr->detectVehicle(frame, roiDilatedFromLeft);
        fromRightPtr->detectVehicle(frame, roiDilatedFromRight);
        toTopPtr->detectVehicle(frame, roiDilatedToTop);
        toBottomPtr->detectVehicle(frame, roiDilatedToBottom);
        toLeftPtr->detectVehicle(frame, roiDilatedToLeft);
        toRightPtr->detectVehicle(frame, roiDilatedToRight);

        // get the frame number and write it on the current frame.
        cv::rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                 cv::Scalar(255,255,255), -1);
        std::stringstream ss;
        ss << capture.get(cv::CAP_PROP_POS_FRAMES);
        std::string frameNumberString = ss.str();
        cv::putText(frame, frameNumberString.c_str(), cv::Point(15, 15),
                cv::FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(255,255,255));

        roiOriginalFromTop = frame(fromTopY, fromTopX);
        roiOriginalFromBottom = frame(fromBottomY, fromBottomX);
        roiOriginalFromLeft = frame(fromLeftY, fromLeftX);
        roiOriginalFromRight = frame(fromRightY, fromRightX);
        roiOriginalToTop = frame(toTopY, toTopX);
        roiOriginalToBottom = frame(toBottomY, toBottomX);
        roiOriginalToLeft = frame(toLeftY, toLeftX);
        roiOriginalToRight = frame(toRightY, toRightX);

        // comment out all drawSegmentsInROI() calls when trying to select the values for segments.
        // draw lines to create segments for original and dilated ROI FromTop.
        // drawSegmentsInROI(true, roiOriginalFromTop, cv::Point(0, fromTopPtr->getTopH()), cv::Point(fromTopPtr->getRoiW(), fromTopPtr->getTopH()),
        //                cv::Point(0, fromTopPtr->getRoiH() - fromTopPtr->getBotH()), cv::Point(fromTopPtr->getRoiW(), fromTopPtr->getRoiH() - fromTopPtr->getBotH()));
        // drawSegmentsInROI(false, roiDilatedFromTop, cv::Point(0, fromTopPtr->getTopH()), cv::Point(fromTopPtr->getRoiW(), fromTopPtr->getTopH()),
        //                cv::Point(0, fromTopPtr->getRoiH() - fromTopPtr->getBotH()), cv::Point(fromTopPtr->getRoiW(), fromTopPtr->getRoiH() - fromTopPtr->getBotH()));

        // drawSegmentsInROI(true, roiOriginalFromBottom, cv::Point(0, fromBottomPtr->getTopH()), cv::Point(fromBottomPtr->getRoiW(), fromBottomPtr->getTopH()),
        //                 cv::Point(0, fromBottomPtr->getRoiH() - fromBottomPtr->getBotH()), cv::Point(fromBottomPtr->getRoiW(), fromBottomPtr->getRoiH() - fromBottomPtr->getBotH()));
        // drawSegmentsInROI(false, roiDilatedFromBottom, cv::Point(0, fromBottomPtr->getTopH()), cv::Point(fromBottomPtr->getRoiW(), fromBottomPtr->getTopH()),
        //                 cv::Point(0, fromBottomPtr->getRoiH() - fromBottomPtr->getBotH()), cv::Point(fromBottomPtr->getRoiW(), fromBottomPtr->getRoiH() - fromBottomPtr->getBotH()));
        
        // drawSegmentsInROI(true, roiOriginalFromLeft, cv::Point(fromLeftPtr->getLeftW(), 0), cv::Point(fromLeftPtr->getLeftW(), fromLeftPtr->getRoiH()),
        //                 cv::Point(fromLeftPtr->getRoiW() - fromLeftPtr->getRightW(), 0), cv::Point(fromLeftPtr->getRoiW() - fromLeftPtr->getRightW(), fromLeftPtr->getRoiH()));
        // drawSegmentsInROI(false, roiDilatedFromLeft, cv::Point(fromLeftPtr->getLeftW(), 0), cv::Point(fromLeftPtr->getLeftW(), fromLeftPtr->getRoiH()),
        //                 cv::Point(fromLeftPtr->getRoiW() - fromLeftPtr->getRightW(), 0), cv::Point(fromLeftPtr->getRoiW() - fromLeftPtr->getRightW(), fromLeftPtr->getRoiH()));
        
        // drawSegmentsInROI(true, roiOriginalFromRight, cv::Point(fromRightPtr->getLeftW(), 0), cv::Point(fromRightPtr->getLeftW(), fromRightPtr->getRoiH()),
        //                 cv::Point(fromRightPtr->getRoiW() - fromRightPtr->getRightW(), 0), cv::Point(fromRightPtr->getRoiW() - fromRightPtr->getRightW(), fromRightPtr->getRoiH()));
        // drawSegmentsInROI(false, roiDilatedFromRight, cv::Point(fromRightPtr->getLeftW(), 0), cv::Point(fromRightPtr->getLeftW(), fromRightPtr->getRoiH()),
        //                 cv::Point(fromRightPtr->getRoiW() - fromRightPtr->getRightW(), 0), cv::Point(fromRightPtr->getRoiW() - fromRightPtr->getRightW(), fromRightPtr->getRoiH()));
        
        // drawSegmentsInROI(true, roiOriginalToTop, cv::Point(0, toTopPtr->getTopH()), cv::Point(toTopPtr->getRoiW(), toTopPtr->getTopH()),
        //                cv::Point(0, toTopPtr->getRoiH() - toTopPtr->getBotH()), cv::Point(toTopPtr->getRoiW(), toTopPtr->getRoiH() - toTopPtr->getBotH()));
        // drawSegmentsInROI(false, roiDilatedToTop, cv::Point(0, toTopPtr->getTopH()), cv::Point(toTopPtr->getRoiW(), toTopPtr->getTopH()),
        //                cv::Point(0, toTopPtr->getRoiH() - toTopPtr->getBotH()), cv::Point(toTopPtr->getRoiW(), toTopPtr->getRoiH() - toTopPtr->getBotH()));

        // drawSegmentsInROI(true, roiOriginalToBottom, cv::Point(0, toBottomPtr->getTopH()), cv::Point(toBottomPtr->getRoiW(), toBottomPtr->getTopH()),
        //                 cv::Point(0, toBottomPtr->getRoiH() -     toBottomPtr->getBotH()), cv::Point(toBottomPtr->getRoiW(), toBottomPtr->getRoiH() - toBottomPtr->getBotH()));
        // drawSegmentsInROI(false, roiDilatedToBottom, cv::Point(0, toBottomPtr->getTopH()), cv::Point(toBottomPtr->getRoiW(), toBottomPtr->getTopH()),
        //                 cv::Point(0, toBottomPtr->getRoiH() -     toBottomPtr->getBotH()), cv::Point(toBottomPtr->getRoiW(), toBottomPtr->getRoiH() - toBottomPtr->getBotH()));
        
        // drawSegmentsInROI(true, roiOriginalToLeft, cv::Point(toLeftPtr->getLeftW(), 0), cv::Point( toLeftPtr->getLeftW(), toLeftPtr->getRoiH()),
        //                 cv::Point(toLeftPtr->getRoiW() -     toLeftPtr->getRightW(), 0), cv::Point(toLeftPtr->getRoiW() - toLeftPtr->getRightW(), toLeftPtr->getRoiH()));
        // drawSegmentsInROI(false, roiDilatedToLeft, cv::Point(toLeftPtr->getLeftW(), 0), cv::Point( toLeftPtr->getLeftW(), toLeftPtr->getRoiH()),
        //                 cv::Point(toLeftPtr->getRoiW() -     toLeftPtr->getRightW(), 0), cv::Point(toLeftPtr->getRoiW() - toLeftPtr->getRightW(), toLeftPtr->getRoiH()));
        
        // drawSegmentsInROI(true, roiOriginalToRight, cv::Point(toRightPtr->getLeftW(), 0), cv::Point( toRightPtr->getLeftW(), toRightPtr->getRoiH()),
        //                 cv::Point(toRightPtr->getRoiW() -     toRightPtr->getRightW(), 0), cv::Point(toRightPtr->getRoiW() - toRightPtr->getRightW(), toRightPtr->getRoiH()));
        // drawSegmentsInROI(false, roiDilatedToRight, cv::Point(toRightPtr->getLeftW(), 0), cv::Point( toRightPtr->getLeftW(), toRightPtr->getRoiH()),
        //                 cv::Point(toRightPtr->getRoiW() -     toRightPtr->getRightW(), 0), cv::Point(toRightPtr->getRoiW() - toRightPtr->getRightW(), toRightPtr->getRoiH()));

        cv::rectangle(frame, fromRightRect, cv::Scalar(0, 0, 255));
        cv::rectangle(frame, fromLeftRect, cv::Scalar(0, 0, 255));
        cv::rectangle(frame, fromTopRect, cv::Scalar(0, 0, 255));
        cv::rectangle(frame, fromBottomRect, cv::Scalar(0, 0, 255));
        cv::rectangle(frame, toRightRect, cv::Scalar(    0, 255, 0));
        cv::rectangle(frame, toLeftRect, cv::Scalar(     0, 255, 0));
        cv::rectangle(frame, toTopRect, cv::Scalar(      0, 255, 0));
        cv::rectangle(frame, toBottomRect, cv::Scalar(   0, 255, 0));

        // used when trying to select the values for segments. 
        // change 1st arg of line() to whatever ROI you need to draw a line in, determine values for segments for one ROI at a time.
        // if(gln.pt2.x && gln.pt2.y) {
        //     line(frame, gln.pt1, gln.pt2, cv::Scalar(255, 255, 255), 1);
        //     if(printCoords) {
        //         std::cout << "==========================================================================================" << std::endl;
        //         std::cout << "start point of line: " << "(" << gln.pt1.x << "," << gln.pt1.y << ")" << std::endl;
        //         std::cout << "end point of line: " << "(" << gln.pt2.x << "," << gln.pt2.y << ")" << std::endl;
        //         printCoords=false;
        //     }
        // }

        // show needed frames
        cv::imshow("Original Frame", frame);
        // imshow("Original frame Blurred and Dilated", fgDilated);
        // imshow("FromTop ROI Original", roiOriginalFromTop);
        // imshow("FromTop ROI Blurred and Dilated", roiDilatedFromTop);
        // imshow("FromBottom ROI Original", roiOriginalFromBottom);
        // imshow("FromBottom ROI Blurred and Dilated", roiDilatedFromBottom);
        // imshow("FromLeft ROI Original", roiOriginalFromLeft);
        // imshow("FromLeft ROI Blurred and Dilated", roiDilatedFromLeft);
        // imshow("FromRight ROI Original", roiOriginalFromRight);
        // imshow("FromRight ROI Blurred and Dilated", roiDilatedFromRight);

        // imshow("ToTop ROI Original", roiOriginalToTop);
        // imshow("ToTop ROI Blurred and Dilated", roiDilatedToTop);
        // imshow("ToBottom ROI Original", roiOriginalToBottom);
        // imshow("ToBottom ROI Blurred and Dilated", roiDilatedToBottom);
        // imshow("ToLeft ROI Original", roiOriginalToLeft);
        // imshow("ToLeft ROI Blurred and Dilated", roiDilatedToLeft);
        // imshow("ToRight ROI Original", roiOriginalToRight);
        // imshow("ToRight ROI Blurred and Dilated", roiDilatedToRight);

        // get the input from the keyboard
        key = cv::waitKey(30);

        // escape for exit.
        if (key == 27)
            break;
        // space bar for pause.
        if (key == 32) {
            playVid = !playVid;
        }
        // click s to get the difference between two consecutive frames and save it as an image "frameN.txt"
        if (key == 's') {
            debugSaveImg(frame);
            prevFrame = frame.clone();
            fsave = true;
        }
        if (framecount == 30) {
            time(&end);
        }
        framecount++;
    }

    double seconds = difftime(end, start);
    double fps  = 30 / seconds;
    std::cout << "Processing frames per second: " << fps << std::endl;
    std::cout << "num frames: " << framecount << std::endl;
    //outfile.close();

    return 0;
}