// using namespace example
#include <iostream>
using namespace std;

class Square;

class Rectangle {
  int width, height;
    public:
      void convert(Square a);
};

class Square {
  friend class Rectangle;
  int side;
    public:
      Square (int a) : side(a) {}
      Square (const Square& a) : side(10) {}
      int getSide() { return side; }
};

void Rectangle::convert(Square a) {
  cout << a.side << endl;
  width = a.side;
  height = a.side;
}

int main() {
  Rectangle rec;
  Square sqr(4);
  cout << sqr.getSide() << endl;
  rec.convert(sqr);

  return 0;
}