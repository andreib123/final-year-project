

#include <iostream>
#include <sstream>
#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
using namespace cv;
using namespace std;

const char* params = "{ input i        | ../data/vtest.avi | Path to a video or a sequence of image }";



int main(int argc, char* argv[])
{
    CommandLineParser parser(argc, argv, params);

    bool playVid = true;
    string imNameFolder = "nuigimages/";
    string imNameStart = "vehiclesA";
    string imNameExt = ".jpg";
    int imNum = 0;
    
    Mat frame;

    VideoCapture capture(parser.get<String>("input"));
    if (!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open: " << parser.get<String>("input") << endl;
        return 0;
    } 

    while (true) {
        if(playVid)
            capture >> frame;

        if (frame.empty())
            break;

        // get the frame number and write it on the current frame.
        rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                  cv::Scalar(255,255,255), -1);
        stringstream ss;
        ss << capture.get(CAP_PROP_POS_FRAMES);
        string frameNumberString = ss.str();
        putText(frame, frameNumberString.c_str(), cv::Point(15, 15),
                FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));

        // show needed frames.
        imshow("ROI Original", frame);

        //get the input from the keyboard
        int keyboard = waitKey(30);

        // escape for exit.
        if (keyboard == 27)
            break;
        // space bar for pause.
        if (keyboard == 32) {
            playVid = !playVid; 
        }
        // 'w' key means save frame
        if (keyboard == 119) {
            string imName = imNameFolder + imNameStart + to_string(imNum) + imNameExt;
            imwrite(imName, frame);
            imNum++;
        }
        
    }

    return 0;
}