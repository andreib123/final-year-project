#include "opencv2/opencv.hpp"
#include <iostream>
 
using namespace std;
using namespace cv;

int main() {

    VideoCapture cap(0); // access to camera 0, will open /dev/video0

    // Check if camera opened successfully.
    if(!cap.isOpened()) {
        cout << "Error opening video stream or file" << endl;
        return -1;
    }

    // Default resolution of the frame is obtained.The default resolution is system dependent. 
    int frame_width = 1920;
    int frame_height = 1080; 
   
    // Define the codec and create VideoWriter object.The output is stored in 'outcpp.avi' file. 
    VideoWriter video("outcpp.avi",CV_FOURCC('M','J','P','G'),10, Size(frame_height,frame_width));

    while(true) {
 
        Mat frame;

        // Capture frame-by-frame
        cap >> frame;

        cv::resize(frame,frame, cv::Size(frame_width, frame_height*3));
  
        // If the frame is empty, break immediately
        if (frame.empty()) break;

        // Write the frame into the file
        video.write(frame);
 
        // Display the resulting frame
        imshow( "Frame", frame );
 
        // Press  ESC on keyboard to exit
        char c = (char)waitKey(25);
        if(c == 27) break;
    }
        
    // When everything done, release the video capture object
    cap.release();
    video.release();
 
    // Closes all the frames
    destroyAllWindows();
     
    return 0;
 }