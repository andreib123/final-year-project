/*
    This is a test program for creating a database of car images from the 
    nuigvids/ folder.

    The main steps involved are:
        - open video file.
        - create MOG2 background subtractor (BGS).
        - perform morphological operations on frames produced by BGS
          to reduce noise and dilate blobs.
        - remove shadow detection (done when shadows are very strong and classified as foreground by BGS.
        - create cropped regions of interest (ROI) where needed to detect cars.
        - save crop of original frame if the number of foreground pixels exceeds a threshold in the ROIs.
*/

#include <iostream>
#include <sstream>
#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
using namespace cv;
using namespace std;

const char* params = "{ input i        | ../data/vtest.avi | Path to a video or a sequence of image }";

int main(int argc, char* argv[])
{
    CommandLineParser parser(argc, argv, params);

    string imNameFolder = "nuigimages/frontalrightlane/";
    string imNameStart = "vehicle";
    string imNameExt = ".jpg";
    int imNum = 1; // change to latest image + 1 for a new video to continue adding more images and not overwiting existing ones.
    bool vehicleSaved = false;

    Mat frame, fgMask, fgMaskBlur, fgDilate, roiDilated, segTop, segCenter, segBottom; // different frames needed.
    Mat element = getStructuringElement(MORPH_RECT, Size( 6,6 )); // dilation element.

    // define ROI. top left corner of roi = 1530,600, bottom right = 1630,780
    Range roiRangeX = Range(1530, 1630); // OpenCV range object for the ROI.
    Range roiRangeY = Range(600, 780); // for Y: (,y)TopLeftROI, (,y)BottomRightROI.

    time_t start, end; // start and end times for calculating FPS.
    int framecount = 0;

    constexpr int ROI_W = 100;
    constexpr int ROI_H = 180;
    int ROI_SIZE = ROI_W*ROI_H;
    int TOP_SIZE = ROI_W*30;  // in pixels.
    int CENTER_SIZE = ROI_W*125;
    constexpr int BOTTOM_SIZE = ROI_W*25;

    int thTop    = 50; // threshold in %
    int thCenter = 43;
    int thBottom = 72;

    int whitePixelsThTop    = TOP_SIZE    * thTop    / 100; // threshold in pixels
    int whitePixelsThCenter = CENTER_SIZE * thCenter / 100;
    int whitePixelsThBottom = BOTTOM_SIZE * thBottom / 100;
    
    
    int whitePixelsTop, whitePixelsCenter, whitePixelsBottom;
    double percWhiteTop, percWhiteCenter, percWhiteBottom;
    
    //create Background Subtractor object
    Ptr<BackgroundSubtractor> pBackSub = createBackgroundSubtractorMOG2();

    VideoCapture capture(parser.get<String>("input"));
    if (!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open: " << parser.get<String>("input") << endl;
        return 0;
    }

    /*
    Ptr<SimpleBlobDetector> detector;
    SimpleBlobDetector::Params params;
    vector<KeyPoint> keypoints;

    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 100*100;

    detector = SimpleBlobDetector::create(params);
    */
    

    time(&start);
    while (true) {
        capture >> frame;

        if (frame.empty())
            break;

        //update the background model
        pBackSub->apply(frame, fgMask);

        blur( fgMask, fgMaskBlur, Size( 2, 2 ), Point(-1,-1) );
        dilate(fgMaskBlur, fgDilate, element );
        threshold(fgDilate, fgDilate, 128, 255, THRESH_BINARY); // Remove the shadow parts and the noise

        roiDilated = fgDilate(roiRangeY, roiRangeX);
        segTop = roiDilated(Range(0, 30), Range(0, 100));
        segCenter = roiDilated(Range(30, 155), Range(0, 100));
        segBottom = roiDilated(Range(155, 180), Range(0, 100));

        whitePixelsTop = countNonZero(segTop);
        percWhiteTop = whitePixelsTop * 100.0 / TOP_SIZE;
        cout << "Percentage white pixels in TOP: " << percWhiteTop << endl;
        whitePixelsCenter = countNonZero(segCenter);
        percWhiteCenter = whitePixelsCenter * 100.0 / CENTER_SIZE;
        cout << "Percentage white pixels in CENTER: " << percWhiteCenter << endl;
        whitePixelsBottom = countNonZero(segBottom);
        percWhiteBottom = whitePixelsBottom * 100.0 / BOTTOM_SIZE;
        cout << "Percentage white pixels in BOTTOM: " << percWhiteBottom << endl;
        cout << "-----------------------------------------------------------------" << endl;

        // segTop and segBottom both empty (below their thresholds of vehicles present)
        if((whitePixelsTop < whitePixelsThTop) && (whitePixelsBottom < whitePixelsThBottom)) {
            // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleSaved = false)
            if((whitePixelsCenter > whitePixelsThCenter) && (vehicleSaved == false)) {
                // save ROI with vehicle in the center as image.
                string imName = imNameFolder + imNameStart + to_string(imNum) + imNameExt;
                //imwrite(imName, frame(Range(630,780), roiRangeX));
                imNum++;
                vehicleSaved = true;
            }
        } // vehicle is leaving the ROI
        else if(whitePixelsBottom > whitePixelsThBottom) {
            vehicleSaved = false; // allows the next new vehicle entering the ROI to be saved as image
        }

        /*
        detector->detect( roi, keypoints);
        Mat im_with_keypoints;
        drawKeypoints( roi, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
        imshow("keypoints", im_with_keypoints ); // Show blobs
        */

        // get the frame number and write it on the current frame.
        rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                  cv::Scalar(255,255,255), -1);
        stringstream ss;
        ss << capture.get(CAP_PROP_POS_FRAMES);
        string frameNumberString = ss.str();
        putText(frame, frameNumberString.c_str(), cv::Point(15, 15),
                FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));

        // draw lines for segments of ROI
        Mat originalRoi = frame(roiRangeY, roiRangeX);
        line(originalRoi, Point(0,30), Point(130,30), Scalar(0,255,0), 1);
        line(originalRoi, Point(0,155), Point(130,155), Scalar(0,255,0), 1);
        line(roiDilated, Point(0,30), Point(130,30), Scalar(255,255,255), 1);
        line(roiDilated, Point(0,155), Point(130,155), Scalar(255,255,255), 1);


        // show needed frames.
        imshow("ROI Original", originalRoi);
        imshow("ROI Blurred and Dilated", roiDilated);

        //get the input from the keyboard
        int keyboard = waitKey(30);

        // escape for exit.
        if (keyboard == 27)
            break;
        // space bar for pause.
        if (keyboard == 32) {
            waitKey(0);
        }
        if (framecount == 30) {
            time(&end);
        }
        framecount++;
    }

    double seconds = difftime (end, start);
    double fps  = 30 / seconds;
    cout << "Frames per second: " << fps << endl;

    return 0;
}

