/*
    This is a test program for creating a database of car images from the 
    nuigvids/ folder.

    The main steps involved are:
        - open video file.
        - create MOG2 background subtractor (BGS).
        - perform morphological operations on frames produced by BGS
          to reduce noise and dilate blobs.
        - remove shadow detection (done when shadows are very strong and classified as foreground by BGS.
        - create cropped regions of interest (ROI) where needed to detect cars.
        - save crop of original frame if the number of foreground pixels exceeds a threshold in the ROIs.
*/

#include <iostream>
#include <sstream>
#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include "opencv2/tracking.hpp"

using namespace cv;
using namespace std;

const char* params = "{ input i        | ../data/vtest.avi | Path to a video or a sequence of image }";

int main(int argc, char* argv[])
{
    CommandLineParser parser(argc, argv, params);

    // List of tracker types in OpenCV 3.4.1
    string trackerTypes[8] = {"BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW", "GOTURN", "MOSSE", "CSRT"};

    // Create a tracker
    string trackerType = trackerTypes[2];

    if (trackerType == "BOOSTING")
        tracker = TrackerBoosting::create();
    if (trackerType == "MIL")
        tracker = TrackerMIL::create();
    if (trackerType == "KCF")
        tracker = TrackerKCF::create();
    if (trackerType == "TLD")
        tracker = TrackerTLD::create();
    if (trackerType == "MEDIANFLOW")
        tracker = TrackerMedianFlow::create();
    if (trackerType == "GOTURN")
        tracker = TrackerGOTURN::create();
    if (trackerType == "MOSSE")
        tracker = TrackerMOSSE::create();
    if (trackerType == "CSRT")
        tracker = TrackerCSRT::create();



    string imNameFolder = "nuigimages/frontalleftlane/";
    string imNameStart = "vehicle";
    string imNameExt = ".jpg";
    int imNum = 1;

    Mat frame, fgMask, fgMaskBlur, fgDilate, crop, cropDilated; // different frames needed.
    Mat element = getStructuringElement(MORPH_RECT, Size( 6,6 )); // dilation element.

    // define ROI. top left corner of roi = 1400,600, bottom right = 1550,766
    Range rangeX = Range(1400, 1550); // OpenCV range object for the ROI.
    Range rangeY = Range(600, 766); // for Y: (,y)TopLeftROI, (,y)BottomRightROI.

    time_t start, end; // start and end times for calculating FPS.
    int framecount = 0;

    int ROI_SIZE = 150*155; // in pixels.
    int blob_threshold = (ROI_SIZE / 100) * 20; // % of ROI_SIZE
    
    int whitepixels;
    double percwhite;
    int waitCount = 0;
    
    //create Background Subtractor object
    Ptr<BackgroundSubtractor> pBackSub = createBackgroundSubtractorMOG2();

    VideoCapture capture(parser.get<String>("input"));
    if (!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open: " << parser.get<String>("input") << endl;
        return 0;
    }

    /*
    Ptr<SimpleBlobDetector> detector;
    SimpleBlobDetector::Params params;
    vector<KeyPoint> keypoints;

    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 100*100;

    detector = SimpleBlobDetector::create(params);
    */
    

    time(&start);
    while (true) {
        capture >> frame;

        if (frame.empty())
            break;

        //update the background model
        pBackSub->apply(frame, fgMask);

        blur( fgMask, fgMaskBlur, Size( 2, 2 ), Point(-1,-1) );
        dilate(fgMaskBlur, fgDilate, element );
        threshold(fgDilate, fgDilate, 128, 255, THRESH_BINARY); // Remove the shadow parts and the noise

        Mat crop = fgMask(rangeY, rangeX);
        Mat cropDilated = fgDilate(rangeY, rangeX);

        whitepixels = countNonZero(cropDilated);
        percwhite = ((double)whitepixels / ROI_SIZE) * 100.0;
        cout << "Percentage white pixels in roi: " << percwhite << endl;

        if(whitepixels > blob_threshold) {
            // wait 3 frames before checking threshold again
            if(waitCount == 3) {
                // Define initial bounding box (xmin,ymin,boxwidth,boxheight)
                Rect2d bbox(1434, 641, 85, 98);
                // Display bounding box.
                rectangle(frame, bbox, Scalar( 255, 0, 0 ), 2, 1 );
                tracker->init(frame, bbox);
                //string imName = imNameFolder + imNameStart + to_string(imNum) + imNameExt;
                //imwrite(imName, frame(rangeY, rangeX));
                //imNum++;
                waitCount = 0;
            }
            else waitCount++;
        }
        else {
            waitCount = 0;
        }

        // Update the tracking result
        bool ok = tracker->update(frame, bbox);

        if(ok) {
            // Tracking success : Draw the tracked object
            rectangle(frame, bbox, Scalar( 255, 0, 0 ), 2, 1 );
        }
        else {
            // Tracking failure detected.
            putText(frame, "Tracking failure detected", Point(100,80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
        }

        /*
        detector->detect( roi, keypoints);
        Mat im_with_keypoints;
        drawKeypoints( roi, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
        imshow("keypoints", im_with_keypoints ); // Show blobs
        */

        // get the frame number and write it on the current frame.
        rectangle(frame, cv::Point(10, 2), cv::Point(100,20),
                  cv::Scalar(255,255,255), -1);
        stringstream ss;
        ss << capture.get(CAP_PROP_POS_FRAMES);
        string frameNumberString = ss.str();
        putText(frame, frameNumberString.c_str(), cv::Point(15, 15),
                FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));

        // show needed frames.
        imshow("Original", frame);
        imshow("ROI Original", frame(rangeY, rangeX));
        imshow("ROI Blurred and Dilated", cropDilated);

        //get the input from the keyboard
        int keyboard = waitKey(30);

        // escape for exit.
        if (keyboard == 27)
            break;
        // space bar for pause.
        if (keyboard == 32) {
            waitKey(0);
        }
        if (framecount == 30) {
            time(&end);
        }
        framecount++;
    }

    double seconds = difftime (end, start);
    double fps  = 30 / seconds;
    cout << "Frames per second: " << fps << endl;

    return 0;
}