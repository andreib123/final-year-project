#include "postprocessor.h"
#include "inferencer.h"

#include <time.h>

using bsoncxx::builder::basic::kvp;

bool Postprocessor::run(const float* res, int roi) {
    // loop through predictions and set predicted class to the class with the highest probability.
    max_prob = res[0];
    pred_class = 0;
    for(int i = 0; i < CLASS_COUNT; i++) {
        if(res[i] > max_prob) {
            max_prob = res[i];
            pred_class = i;
        }
    }
    if(max_prob > CONF_THR) {
        post(pred_class, roi);
        return true; // successfully posted an actual predicted class to the database if max prob is above threshold.
    }

    return false; // prediction too unconfident to post to database/ could be due to false positive detection by vehicle detection algorithm.
}

void Postprocessor::post(int pred_class, int roi) {
    // get time of classification.
    std::time_t t
        = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&t), "%FT%H:%M"); // ISO 8601 time format string.

    db = clnt->database("FinalYearProject"); 
    coll = db[col_name]; // the collection.

    // create database entry document.
    basic_builder.append(
        kvp("class", pred_class),
        kvp("roi", roi),
        kvp("time", ss.str())
    );

    bsoncxx::document::value doc_val = basic_builder.extract(); // data buffer owning BSON object.
    bsoncxx::document::view doc_view = doc_val.view(); // non-owning copy of a BSON object (pointer).
    coll.insert_one(doc_view); // post document to collection in database.
    // TO DO: should probably have a check to ensure the post was successful. Otherwise throw exception.

    basic_builder.clear();
}
    