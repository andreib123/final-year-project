#pragma once

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

class Postprocessor {

public:

    Postprocessor(const char* col) {
        col_name = col;

        clnt = new mongocxx::client{
            mongocxx::uri{
                "mongodb+srv://andrei:admin@cluster0-cnuh2.mongodb.net/FinalYearProject?retryWrites=true&w=majority"
            }
        };
    }

    ~Postprocessor() {
        delete clnt;
    }

    bool run(const float* res, int roi);

    inline int getClassID() { return pred_class; }

    inline float getProb() { return max_prob; }

private:

    void post(int pred_class, int roi);

private:

    static constexpr float CONF_THR = .5f; // threshold for prediction class confidence.
    int pred_class; // the predicted class returned by the inferencer.
    float max_prob; // the maximum probability when a detection happens.

    const char* col_name; // name of collection.
    mongocxx::instance inst; // must have one instance created.
    mongocxx::client* clnt; // pointer as if using default constructor cannot initialise further.
    mongocxx::database db; // the database.
    mongocxx::collection coll; // the collection.
    bsoncxx::builder::basic::document basic_builder;
};