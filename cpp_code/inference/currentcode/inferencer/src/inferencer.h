#pragma once

#include <cuda_runtime_api.h>
#include <stddef.h>
#include <opencv2/opencv.hpp>

#include "logger.h"

namespace nvinfer1 {
    class ICudaEngine;
    class IHostMemory;
    class IExecutionContext;
}

typedef enum {
    CLASS_PICKUP = 0,
    CLASS_SEDAN,
    CLASS_SUV,
    CLASS_VAN,
    CLASS_COUNT,
    CLASS_UNK = -1
} nwk_class_t;

class Inferencer {

public:

    ~Inferencer() { destroy(); }

    /**
     * Initialise Inferencer object so it is ready for running inference.
     * 
     * @param char[in] Path to the model ONNX file.
     */
    bool init(const char* model_path);

    /**
     * Run inference on one preprocessed input image.
     * 
     * @param void[in] Input image (float32 format, gray padded 3,128,128 planar RGB rescaled to -1,1).
     */
    void run(const void* inputBuffer);

    /**
     * Pre-processes input image and creates image ready for inferencer.
     *
     * @param input[in] Input image (uint8_t format, interleaved RGB).
     * @param output[out] Output image (float32 format, resized gray padded 3,128,128 planar RGB rescaled to -1,1 range).
     */
    void preprocessInput(const cv::Mat* input, cv::Mat* output) const;

    /**
     * Return result of one inference run.
     * 
     * @return float* array of length NUM_CLASSES.
     */
    inline const float* getOut() const { return outputBuffer; }

    static const char* getClassLabel(int classID) {
        const char* labels[CLASS_COUNT] = {
            "pickup",
            "sedan",
            "suv",
            "van"
        };
        if((classID < CLASS_COUNT) && (classID >= 0)) {
            return labels[classID];
        }
        return "UNK";
    }

private:

    bool loadOnnx(const char* model);

    bool setCudaEngine();

    void allocateCudaMemStream();

    void destroy();

private:

    static const int NET_IN_SIDE = 128; // image for CNN input will have shape (3,NET_IN_SIDE,NET_IN_SIDE).
    static const int OUTPUT_SIZE = CLASS_COUNT; // number of classes.
    static const int BATCH_SIZE = 1; // for 1 inference.
    const size_t INBUFBYTES = NET_IN_SIDE * NET_IN_SIDE * 3 * sizeof(float);

    int inputBIndex;
    int outputBIndex;
    void* buffers[2]; // stores inputBuffer pointer and outputBuffer pointer.
    float* outputBuffer;  // where a result of one inference is stored.

    nvinfer1::IHostMemory* trtModelStream;
    nvinfer1::ICudaEngine* engine;
    nvinfer1::IExecutionContext* context;
    cudaStream_t stream;

    Logger logger;
};
