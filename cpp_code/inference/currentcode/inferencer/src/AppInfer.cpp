// Note: the intersection is taken as the focal point of the ROIs, so the word "to" implies "to the intersection", i.e. vehicle entering intersection.
//           Similarly, "from" implies from the intersection, i.e. vehicle is leaving the intersection.

#include "lanes.h"
#include "inferencer.h"
#include "postprocessor.h"
#include "profiler.h"

#include <semaphore.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <unistd.h>

// ------------------------- code for create a non-blocking listener for the keyboard input in terminal. --------------_-- //
struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}

int kbhit()
{
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

int getch()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}

// ----------------------------------------------------------------------------------------------------------------------- //


class LaneLabel {
// for ROIs monitoring entering vehicles.
// the class of vehicles detected will be displayed in the bottom left corner of the ROI.

public:

    LaneLabel(const cv::Rect &roi) {
        counter = 0;
        bgRect.width = 250;
        bgRect.height = 30;
        bgRect.x = roi.x+5;
        bgRect.y = roi.y+roi.height-5-bgRect.height;
        blText.x = bgRect.x+5;
        blText.y = bgRect.y+bgRect.height-5;
    }

    void setLabel(int c_id, float mx_prb) {
        classID = c_id;
        maxProb = mx_prb;
        counter = 40;
    }

    void drawLabel(cv::Mat &frame) {
        if(counter) {
            char str[256];

            snprintf(str, sizeof(str), "%s: %.2f",
                Inferencer::getClassLabel(classID),
                maxProb);

            cv::rectangle(frame,
                bgRect,
                cv::Scalar(255,255,255),
                -1);
            cv::putText(frame,
                str,
                blText,
                cv::FONT_HERSHEY_DUPLEX,
                1, cv::Scalar::all(0), 1, 8, false);

            counter--;
        }
    }

private:
    cv::Rect bgRect; // background rectangle.
    cv::Point blText; // bottom left corner of text.
    int classID;
    float maxProb;
    int counter; // amount of frames the label will be shown for.


};

class LaneExitCount {
// for ROIs monitoring exiting vehicles.
// the number of vehicles exited through an ROI will be displayed in the top left corner of that ROI.

public:

    LaneExitCount(const cv::Rect &roi) {
        bgRect.width = 195;
        bgRect.height = 30;
        bgRect.x = roi.x+5;
        bgRect.y = roi.y+5;
        blText.x = bgRect.x+5;
        blText.y = bgRect.y+bgRect.height-5;
    }

    void drawExitCount(int cnt, cv::Mat &frame) {
        cv::rectangle(frame,
                bgRect,
                cv::Scalar(255,255,255),
                -1);
        cv::putText(frame,
            "exited: " + std::to_string(cnt),
            blText,
            cv::FONT_HERSHEY_DUPLEX, 1,
            cv::Scalar::all(0),
            1, 8, false);
    }

private:

    cv::Rect bgRect; // background rectangle.
    cv::Point blText; // bottom left corner of text.

};

class CrossroadsApp {
// the main application class.

public:
    CrossroadsApp(const char* _video_fname_, const char* model_path, const char* collection_name) {

        memset(enteredVehicles, 0, sizeof(enteredVehicles));

        pthread_mutex_init(&mu_f2, NULL);
        pthread_mutex_init(&mu_f3, NULL);

        video_fname = _video_fname_;
        quit = false;
        showImg = 0;

        live = (video_fname.find("://") != std::string::npos);
        // live = true; // to show what live would look like.

        recs[0] = cv::Rect(1451, 314, 134, 161); // toTop.
        recs[1] = cv::Rect(1043, 677, 365, 383); // toBottom.
        recs[2] = cv::Rect(470, 492, 450, 199); // toLeft.
        recs[3] = cv::Rect(1419, 459, 408, 193); // toRight.
        recs[4] = cv::Rect(1585, 314, 134, 161); // fromTop.
        recs[5] = cv::Rect(558, 677, 365, 383); // fromBottom.
        recs[6] = cv::Rect(63, 332, 312, 184); // fromLeft.
        recs[7] = cv::Rect(1320, 550, 507, 231); // fromRight.
        
        
        entryLanes[0] = new FrontalLane(true,  true, recs[0], 25,   27,   7,     25,    10,    7); // FRONTAL TO TOP SETUP.
        //                                                    topH, botH, thTop, thCen, thBot, waitFrames
        
        entryLanes[1] = new FrontalLane(false, true, recs[1], 50, 32, 15, 12, 8, 0); // FRONTAL TO BOTTOM SETUP.
        
        entryLanes[2] = new LateralLane(true,  true, recs[2], 50,    47,     7,     14,    15,      0); // LATERAL TO LEFT SETUP.
        //                                                    leftW, rightW, thLeft, thCen, thRight, waitFrames

        entryLanes[3] = new LateralLane(false, true, recs[3], 40, 68, 10, 25, 30, 0); // LATERAL TO RIGHT SETUP.

        exitLanes[0]  = new FrontalLane(true,  false, recs[4], 25, 27, 20, 18, 10, 0); // FRONTAL FROM TOP SETUP.
        exitLanes[1]  = new FrontalLane(false, false, recs[5], 50, 32, 20, 18, 10, 0); // FRONTAL FROM BOTTOM SETUP.
        exitLanes[2]  = new LateralLane(true,  false, recs[6], 50, 47, 15, 10, 10, 0); // LATERAL FROM LEFT SETUP.
        exitLanes[3]  = new LateralLane(false, false, recs[7], 40, 68, 15, 12, 30, 0); // LATERAL FROM RIGHT SETUP.

        laneLabels[0] = new LaneLabel(recs[0]);
        laneLabels[1] = new LaneLabel(recs[1]);
        laneLabels[2] = new LaneLabel(recs[2]);
        laneLabels[3] = new LaneLabel(recs[3]);

        laneExitCounts[0] = new LaneExitCount(recs[4]);
        laneExitCounts[1] = new LaneExitCount(recs[5]);
        laneExitCounts[2] = new LaneExitCount(recs[6]);
        laneExitCounts[3] = new LaneExitCount(recs[7]);

        inferencer = new Inferencer();
        inferencer->init(model_path);

        postprocessor = new Postprocessor(collection_name);
    }

    ~CrossroadsApp() {

        for(int i = 0; i < LANES_LEN; i++) {
            if(entryLanes[i]) delete entryLanes[i];
            if(exitLanes[i]) delete exitLanes[i];
        }

        pthread_mutex_destroy(&mu_f2);
        pthread_mutex_destroy(&mu_f3);
        sem_destroy(&sem_f2);
    }

    void run() {
        FpsProfiler showImgFps(10);

        pthread_t t1; // read video.
        pthread_t t2; // detect infer.

        sem_init(&sem_f2,   // semaphore,
            0,              // shared between threads,
            0);             // initialised to 0.

        pthread_create(&t1, NULL, readVideoThread, this);
        pthread_create(&t2, NULL, detectInferThread, this);

        set_conio_terminal_mode();

        while(true) {
            if(kbhit()) {
                int key = getch();
                if(key == 113 || key == 81) { // q or Q.
                    std::cout << "EXITED BY 'Q' KEY!!!" << std::endl;
                    break;
                }
            }
            if(showImg) {
                if (pthread_mutex_trylock(&mu_f3) == 0) {
                    showImgFps.measure();
                    cv::waitKey(1); // used so that imshow renders to screen.
                    cv::imshow("Original feed", frame3);

                    showImg = 0;
                    pthread_mutex_unlock(&mu_f3);
                }
            }
        }
        reset_terminal_mode();

        quit = true;
        pthread_join(t1, NULL);
        pthread_join(t2, NULL);

        std::cout << "Run (ShowImg) thread processing frames per second: " << showImgFps.get_fps() << std::endl;
    }

private:

    static void *readVideoThread(void *context) {
        CrossroadsApp *self = reinterpret_cast<CrossroadsApp*>(context);
        self->readVideo();
    }

    static void *detectInferThread(void *context) {
        CrossroadsApp *self = reinterpret_cast<CrossroadsApp*>(context);
        self->detectInfer();
    }

    /**
     * Thread function reading video stream and copying frame1 to frame2.
     */
    void readVideo() {
        int emptyFramesCounter = 0;
        FpsProfiler readVideoFps(10);

        cv::VideoCapture capture(video_fname);
        if (!capture.isOpened()) {
            //error in opening the video input
            std::cerr << "Unable to open: " << video_fname << std::endl;
            return;
        }

        std::cout << "Original video frames per second : " << capture.get(cv::CAP_PROP_FPS) << std::endl;

        while(!quit) {
            readVideoFps.measure();

            if(!capture.grab()) {
                // nothing I can do will stop video capture from freezing after 146 frames of livestream video.
                // only thing left to do is reload the video every 146 frames, but this takes a lot of time.
                capture.release();
                capture.open(video_fname);
            }

            capture.retrieve(frame1);
            // usleep(1000000); // doesn't help stop the freezing.

            if(frame1.empty()) {
                if(++emptyFramesCounter >= 100) {
                    // 100 consecutive empty frames signalling end of video.
                    quit = true;
                    break;
                }
                continue; // skip empty frame without copying.
            }
            emptyFramesCounter = 0; // reset counter when a non-empty frame is received.

            if(live) {
                if (pthread_mutex_trylock(&mu_f2) == 0) {
                    frame1.copyTo(frame2);
                    sem_post(&sem_f2);
                    pthread_mutex_unlock(&mu_f2);
                }
            } else {
                pthread_mutex_lock(&mu_f2);
                frame1.copyTo(frame2);
                sem_post(&sem_f2);
                pthread_mutex_unlock(&mu_f2);
            }
        }
        std::cout << "ReadVideo thread processing frames per second: " << readVideoFps.get_fps() << std::endl;
    }

    /**
     *  Thread function detecting vehicles in ROIs and infering cv::Mat frame2 member variable. 
     *  Also copies frame2 to frame3.
     */
    void detectInfer() {
        TimeProfiler bgSubTime(10); // time profiler for background subtraction.
        TimeProfiler detectTime(10); // time profiler for entry lanes vehicle detection (entryLanes[i]->isVehicleReady(fgDilated)).
        TimeProfiler inferTime(10); // time profiler for preprocessing and inference.
        FpsProfiler detectInferFps(10);

        // different frame transformations needed in main while loop.
        cv::Mat fgMask, fgMaskBlur, fgDilated; 

        // dilation element.
        cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(6, 6)); 
        cv::Ptr<cv::BackgroundSubtractor> pBackSub = cv::createBackgroundSubtractorMOG2();

        // main loop for detecting and infering.
        while (!quit) {
            sem_wait(&sem_f2);
            pthread_mutex_lock(&mu_f2);

            detectInferFps.measure();

            // draw ROIs on frame.
            drawRois(frame2);

            bgSubTime.start();
            //update the background model
            pBackSub->apply(frame2, fgMask, 0.008); // learning rate = 0.008 [0,1]

            cv::blur(fgMask, fgMaskBlur, cv::Size(9, 9), cv::Point(-1, -1));
            cv::dilate(fgMaskBlur, fgDilated, element);
            cv::threshold(fgDilated, fgDilated, 128, 255, cv::THRESH_BINARY); // remove the shadow parts and the noise.
            bgSubTime.stop();

            for(int i = 0; i < LANES_LEN; i++) {

                detectTime.start();
                bool processVehicle = entryLanes[i]->isVehicleReady(fgDilated);
                detectTime.stop();

                if(processVehicle) {
                    // if vehicle is detected for the first time.
                    cv::Mat roiToClassify(frame2, recs[i]);
                    cv::Mat roiPreprocessed;

                    inferTime.start();
                    inferencer->preprocessInput(&roiToClassify, &roiPreprocessed);
                    inferencer->run(roiPreprocessed.data);
                    inferTime.stop();

                    const float* res = inferencer->getOut();

                    // // print probabilities returned from inferencer.
                    // std::cout << "Probabilities ";
                    // for(int i = 0; i < 4; i++) {
                    //     printf("%f ", *(res + i));
                    // }
                    // printf("\n");

                    if(postprocessor->run(res, i)) {
                        // if detection was classified as a vehicle with enough confidence (confidence threshold defined in Postprocessor class).
                        enteredVehicles[i]++;
                    }
                    // for each detection, get the class corresponding to the max probability and the probability.
                    laneLabels[i]->setLabel(postprocessor->getClassID(), postprocessor->getProb());
                }
                laneLabels[i]->drawLabel(frame2);

                exitLanes[i]->isVehicleReady(fgDilated);
                laneExitCounts[i]->drawExitCount(exitLanes[i]->getVehiclesExitedCnt(), frame2);
            }

            pthread_mutex_lock(&mu_f3);
            frame2.copyTo(frame3);
            showImg = 1;
            pthread_mutex_unlock(&mu_f3);

            pthread_mutex_unlock(&mu_f2);
        }
        printf("\n");
        std::cout << "DetectInfer thread processing frames per second: " << detectInferFps.get_fps() << std::endl;
        std::cout << "Average execution time of background subtraction in ms: " << bgSubTime.get_time() << std::endl;
        std::cout << "Average execution time of vehicle detection in ms: " << detectTime.get_time() << std::endl;
        std::cout << "Average execution time of preprocessing and inference in ms: " << inferTime.get_time() << std::endl;
    }

    void drawRois(cv::Mat &frame) {
        // colour Scalar is BGR.
        // red = exiting ROis.
        // green = entering ROIs.
        cv::rectangle(frame, recs[0], cv::Scalar(0, 255, 0));
        cv::rectangle(frame, recs[1], cv::Scalar(0, 255, 0));
        cv::rectangle(frame, recs[2], cv::Scalar(0, 255, 0));
        cv::rectangle(frame, recs[3], cv::Scalar(0, 255, 0));
        cv::rectangle(frame, recs[4], cv::Scalar(0, 0, 255));
        cv::rectangle(frame, recs[5], cv::Scalar(0, 0, 255));
        cv::rectangle(frame, recs[6], cv::Scalar(0, 0, 255));
        cv::rectangle(frame, recs[7], cv::Scalar(0, 0, 255));
    }

private:

    constexpr static int LANES_LEN = CLASS_COUNT;

    LaneBase *entryLanes[LANES_LEN]; // toTop, toBottom, toLeft, toRight.
    LaneBase *exitLanes[LANES_LEN]; // fromTop, fromBottom, fromLeft, fromRight.
    LaneLabel *laneLabels[LANES_LEN]; // objs for drawing class labels in "to" ROIs.
    LaneExitCount *laneExitCounts[LANES_LEN]; // objs for drawing exit counts in "from" ROIs.
    cv::Rect recs[LANES_LEN*2]; // ROIs.
    int enteredVehicles[LANES_LEN]; // counters for each ROI monitoring entering vehicles,
                                    // keeping track of the amount of classified vehicles for each ROI.
    Inferencer *inferencer;
    Postprocessor *postprocessor;

    std::string video_fname;

    // for livestream support.
    bool live; // true if video_fname has "://" in it, signifying a stream URL.

    // threadsafe variables.
    pthread_mutex_t mu_f2, mu_f3; // mutexes for frame2 and frame3.
    sem_t sem_f2; // semaphore for frame2. Ensures frame2 isn't being used before frame1 is copied to frame2.
    cv::Mat frame1, frame2, frame3;
    bool quit; // breaks out of all loops and exits program.
    volatile int showImg; // Ensures frame3 isn't being used before frame2 is copied to frame3,
                          // has same purpose as the semaphore, just a different approach.  
                          
};

int main(int argc, char* argv[]) {
    std::string model = std::string(argv[1]);      // argv[1] = /path/to/model.onnx
    std::string collection = std::string(argv[2]); // argv[2] = MongoDB Collection name.
    std::string video = std::string(argv[3]);      // argv[3] = /path/to/video

    CrossroadsApp crossroadsApp(video.c_str(), model.c_str(), collection.c_str());

    crossroadsApp.run();

    return 0;
}