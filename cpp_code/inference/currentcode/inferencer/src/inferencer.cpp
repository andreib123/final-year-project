#include "inferencer.h"

#include <NvOnnxParser.h>
#include <iostream>
#include <string.h>
#include <fstream>

bool Inferencer::init(const char* model_path) {
    buffers[2] = {0};
    outputBuffer = new float[OUTPUT_SIZE];
    trtModelStream = nullptr;
    engine = nullptr;
    context = nullptr;

    if (!loadOnnx(model_path))
        return false;

    if(!setCudaEngine())
        return false;

    allocateCudaMemStream();

    return true;
}

void Inferencer::run(const void* inputBuffer) {
    // std::cout << inputBIndex << std::endl;
    // printf("in_buf: %p, cuda_buf: %p\n", inputBuffer, buffers[inputBIndex]);

    // DMA the input to the GPU, execute the batch asynchronously, and DMA it back:
    cudaMemcpyAsync(buffers[inputBIndex], inputBuffer, BATCH_SIZE * INBUFBYTES, cudaMemcpyHostToDevice, stream);
    context->enqueue(BATCH_SIZE, buffers, stream, nullptr);
    cudaMemcpyAsync(outputBuffer, buffers[outputBIndex], BATCH_SIZE * OUTPUT_SIZE * sizeof(float), cudaMemcpyDeviceToHost, stream);
    cudaStreamSynchronize(stream);

    // std::cout << "Probabilities ";
    // for(int i = 0; i < 4; i++) {
    //     printf("%f ", *(outputBuffer + i));
    // }

    // std::cout << std::endl;
}

void Inferencer::destroy() {
    // release the stream, engine and the buffers.

    std::free(outputBuffer);
    cudaStreamDestroy(stream);
    cudaFree(buffers[inputBIndex]);
    cudaFree(buffers[outputBIndex]);
    engine->destroy();
    context->destroy();
}

bool Inferencer::loadOnnx(const char* model) {
    // load onnx model from file into trtModelStream.

    nvinfer1::IBuilder* builder = nvinfer1::createInferBuilder(logger.getTRTLogger());
    assert(builder != nullptr);

    nvinfer1::INetworkDefinition* trtNwk = builder->createNetwork();

    nvonnxparser::IParser* parser = nvonnxparser::createParser(*trtNwk, logger.getTRTLogger());

    bool parsed = parser->parseFromFile(
        model,
        static_cast<int>(logger.getReportableSeverity()));
    if(!parsed) {
        std::cout << "Failure while parsing ONNX file" << std::endl;
        return false;
    }
    
    nvinfer1::Dims inputDims = trtNwk->getInput(0)->getDimensions();
    nvinfer1::Dims outputDims = trtNwk->getOutput(0)->getDimensions();
    std::cout << "input:  (" << inputDims.d[0] << "," << inputDims.d[1] << "," << inputDims.d[2] << ")" << std::endl;
    std::cout << "output: (" << outputDims.d[0] << "," << outputDims.d[1] << "," << outputDims.d[2] << ")" << std::endl;

    //Optional - uncomment below lines to view network layer information
    //config->setPrintLayerInfo(true);
    //parser->reportParsingInfo();

    builder->setMaxBatchSize(BATCH_SIZE);
    builder->setMaxWorkspaceSize(1 << 20); // 2^20 B size.
    builder->setFp16Mode(0);
    builder->setInt8Mode(0); 

    nvinfer1::ICudaEngine *engine = builder->buildCudaEngine(*trtNwk);
    assert(engine);
    trtModelStream = engine->serialize();

    engine->destroy();
    builder->destroy();
    trtNwk->destroy();
    parser->destroy();

    return (trtModelStream != nullptr);
}

bool Inferencer::setCudaEngine() {
    // create inference runtime.
    nvinfer1::IRuntime* runtime = nvinfer1::createInferRuntime(logger);
    assert(runtime != nullptr);

    // deserialize the engine from the trtModelStream.
    engine = runtime->deserializeCudaEngine(
        trtModelStream->data(),
        trtModelStream->size(),
        nullptr); 

    // set inputBIndex and outputBIndex.
    inputBIndex = engine->getBindingIndex("input_2:0"); // input and output names of model can be found in a model viewer like Netron.
    outputBIndex = engine->getBindingIndex("Identity:0");
    // set execution context.
    context = engine->createExecutionContext();

    runtime->destroy();
    trtModelStream->destroy();

    return ((engine != nullptr) && (context!= nullptr));
}


void Inferencer::allocateCudaMemStream() {
    // allocate CUDA GPU buffers memory for an input and output of CNN and initialise the CUDA stream.

    cudaMalloc(&buffers[inputBIndex], BATCH_SIZE * NET_IN_SIDE * NET_IN_SIDE * 3 * sizeof(float));
    cudaMalloc(&buffers[outputBIndex], BATCH_SIZE * OUTPUT_SIZE * sizeof(float));
    cudaStreamCreate(&stream);
}

void Inferencer::preprocessInput(const cv::Mat *input, cv::Mat *out) const {
    // The main steps involved are:
    //  - resizing an interleaved input crop so that its longer side is 128 and preserving aspect ratio of original crop.
    //  - copy the resized crop into the centre of a grey  (128,128,3) interleaved matrix.
    //  - split the resultant matrix into R, G and B planes.
    //  - concatenate the planes to create a planar matrix.
    //  - rescale the matrix values to be in the range [-1,1].

    cv::Mat input_l;
    int h = input->size[0];
    int w = input->size[1];
    int ww = NET_IN_SIDE, hh = NET_IN_SIDE;
    int off_x = 0, off_y = 0;
    if(w>h) {
        hh = NET_IN_SIDE*h/w;
        off_y = (NET_IN_SIDE - hh)/2;
    }
    else {
        ww = NET_IN_SIDE*w/h;
        off_x = (NET_IN_SIDE - ww)/2;
    }
    cv::resize(*input, input_l, cv::Size(ww,hh));            // resized frame to be copied into centre of grey square.
    cv::Mat greyPad(128, 128, CV_8UC3, cv::Scalar::all(80)); // interleaved grey (128,128,3) matrix. grey: all elements = 80.
    //std::cout << greyPad << std::endl;
    input_l.copyTo(greyPad(cv::Rect(off_x, off_y, input_l.cols, input_l.rows)));
    cv::Mat planes[3];
    cv::split(greyPad, planes);                              // split greyPad matrix with frame copied into R, G and B planes.
    cv::Mat planar; // NOTE: shape of this mat seems odd, but should be able to load into CUDA memory continuously:  (128, 384, 2) elemSize: 1.
    cv::vconcat(planes, 3, planar);                          // concatenate planes into RGB planar image with G plane after R etc.
    //dbg_mat_info(&planar, "planar");
    cv::Mat planar_f;                                        // shape: (128, 384, 2) elemSize: 4.
    constexpr float coeff = 1.f/127.5f;                      // coefficient for rescaling all elements of matrix to [-1,1] range.
    planar.convertTo(planar_f, CV_32FC3);                    // must cast to float32 first as float*int produces int for some reason.
    if(out)
        *out = (coeff*planar_f) - 1;                         // planar float is ready to be inputted into CNN.
}
