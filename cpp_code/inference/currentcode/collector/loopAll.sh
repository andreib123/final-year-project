t="15"
c="1"
hrs="2"
mins15cnt=$(( hrs*4 ))
url="https://www.youtube.com/watch?v=WsYtosQta5Y" # LaGrange, KY.

# -le 8 -> means 1 through 8 15min periods i.e. 2 hours of video recording.
while [ $c -le $mins15cnt ]; do
        echo "creating video" final-year-project-results/videos/livecam/lagrange"$c"_"$t"m_best.mp4
        timeout "$t"m streamlink --hls-live-restart -o ~/Desktop/final-year-project-results/videos/livecam/lagrange"$c"_"$t"m_best.mp4 $(youtube-dl -f 96 -g "$url")
        c=$[$c+1]
done

d="1"
while [ $d -le $mins15cnt ]; do
        ../bin/AppDsCollect vid"$d"_NEWTEST1_ ~/Desktop/final-year-project-results/videos/livecam/lagrange"$d"_"$t"m_best.mp4
        d=$[$d+1]
done

