// Note: the intersection is taken as the focal point of the ROIs, so the word "to" implies "to the intersection", i.e. vehicle entering intersection.
//           Similarly, "from" implies from the intersection, i.e. vehicle is leaving the intersection.

#include <string.h>
#include "lanes.h"

class DsCollector {
public:

    DsCollector(const char* rootpath, const char* suffix) {
        recs[0] = cv::Rect(1451, 314, 134, 161); // toTop.
        recs[1] = cv::Rect(1043, 677, 365, 383); // toBottom.
        recs[2] = cv::Rect(63, 459, 312, 149); // toLeft.
        recs[3] = cv::Rect(1419, 459, 408, 193); // toRight.

        // FRONTAL TO TOP SETUP.
        lanes[0] = new FrontalLane(true, true, recs[0], 25,   27,   7,     25,    10,    7);
        //                                        topH, botH, thTop, thCen, thBot, waitFrames

        // FRONTAL TO BOTTOM SETUP.
        lanes[1] = new FrontalLane(false, true, recs[1], 50, 32, 35, 30, 8, 0);

        // LATERAL TO LEFT SETUP
        lanes[2] = new LateralLane(true, true, recs[2], 50,    47,     15,     20,    10,      15);
        //                                        leftW, rightW, thLeft, thCen, thRight, waitFrames

        // LATERAL TO RIGHT SETUP
        lanes[3] = new LateralLane(false, true, recs[3], 40, 68, 10, 30, 30, 0);

        snprintf(save_path_prefixes[0], PATH_MAX, "%sfrontal/vehicle_ft_%s_",
            rootpath, suffix);
        snprintf(save_path_prefixes[1], PATH_MAX, "%sfrontal/vehicle_fb_%s_",
            rootpath, suffix);
        snprintf(save_path_prefixes[2], PATH_MAX, "%slateral/vehicle_fl_%s_",
            rootpath, suffix);
        snprintf(save_path_prefixes[3], PATH_MAX, "%slateral/vehicle_fr_%s_",
            rootpath, suffix);

        memset(imcounters, 0, sizeof(imcounters));
    }

    ~DsCollector() {
        for(int i = 0; i < LANES_LEN; i++) {
            if(lanes[i]) delete lanes[i];
        }
    }

    void run(const char* video) {
        // different frame transformations needed in main while loop.
        cv::Mat frame, fgMask, fgMaskBlur, fgDilated; 

        // dilation element.
        cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size( 6,6 )); 
        cv::Ptr<cv::BackgroundSubtractor> pBackSub = cv::createBackgroundSubtractorMOG2();

        time_t start, end; // start and end times for calculating FPS.
        int framecount = 0;

        //VideoCapture capture(parser.get<String>("input"));
        cv::VideoCapture capture(video);
        if (!capture.isOpened()){
            //error in opening the video input
            std::cerr << "Unable to open: " << video << std::endl;
            return;
        }

        std::cout << "Original video frames per second : " << capture.get(cv::CAP_PROP_FPS) << std::endl;

        // main while loop
        time(&start);
        while (true) {

            capture >> frame;

            if(frame.empty())
                break;
            
            // cv::imshow("orig", frame);
            // cv::waitKey(1);

            //update the background model
            pBackSub->apply(frame, fgMask, 0.008); // learning rate = 0.008 [0,1]


            cv::blur(fgMask, fgMaskBlur, cv::Size( 9, 9 ), cv::Point(-1,-1) );
            cv::dilate(fgMaskBlur, fgDilated, element );
            cv::threshold(fgDilated, fgDilated, 128, 255, cv::THRESH_BINARY); // Remove the shadow parts and the noise

            for(int i = 0; i < LANES_LEN; i++) {
                bool processVehicle = lanes[i]->isVehicleReady(fgDilated);
                if(processVehicle) {
                    cv::Mat roiSave(frame, recs[i]); 
                    cv::imwrite(
                        std::string(save_path_prefixes[i]) +
                        std::to_string(imcounters[i]) + ".jpeg",
                        roiSave
                    );
                    imcounters[i]++;
                }
            }

            if (framecount == 30) {
                time(&end);
            }
            framecount++;
        }

        double seconds = difftime(end, start);
        std::cout << "Processing frames per second: " << 30 / seconds << std::endl;
        std::cout << "num frames: " << framecount << std::endl;
    }

private:
    constexpr static int LANES_LEN = 4;
    LaneBase *lanes[LANES_LEN]; // toTop, toBottom, toLeft, toRight
    char save_path_prefixes[LANES_LEN][PATH_MAX];
    int imcounters[LANES_LEN];
    cv::Rect recs[LANES_LEN];
};

int main(int argc, char* argv[]) {
    // argv[1] = current run's short suffix added to all image paths.
    // argv[2] = /path/to/video.mp4.

    DsCollector collector("/home/andrei/Desktop/final-year-project-results/images/livecam/", argv[1]);

    collector.run(argv[2]);

    return 0;
}
