#pragma once

#include <time.h>

class TimeProfiler {
public:
    explicit TimeProfiler(int count_max) {
        count_max_ = count_max;
        reset();

    }
    void start() {
        clock_gettime(CLOCK_MONOTONIC, &t1_);
    }
    void stop() {
        struct timespec t2;
        clock_gettime(CLOCK_MONOTONIC, &t2);

        uint64_t time_ns = 1e9*((uint64_t)t2.tv_sec - t1_.tv_sec) + t2.tv_nsec - t1_.tv_nsec;
        acc_ns_ += time_ns;
        count_++;
        if(count_ >= count_max_) {
            avg_ms_ = static_cast<int32_t>(acc_ns_/count_max_*1e-6);
            reset();
        }

    }
    void reset() {
        count_ = 0;
        acc_ns_ = 0;
    }

    /**
     * Gets average time in milliseconds counted between start() and stop() calls.
     */
    int32_t get_time() {
        return avg_ms_;
    }

private:
    struct timespec t1_;

    uint64_t acc_ns_;
    int32_t count_max_;
    int32_t count_;
    int32_t avg_ms_;
};

class FpsProfiler {
 public:
    explicit FpsProfiler(int count_max) {
        started_ = false;
        count_max_ = count_max;
        fps_ = 0;
        reset();
    }
    void reset() {
        count_ = 0;
        t_acc_ns_ = 0;
    }
    void measure() {
        struct timespec time;
        clock_gettime(CLOCK_MONOTONIC, &time);
        if (!started_) {
            t_st_ = time;
            started_ = true;
        } else {
            uint64_t time_ns = 1e9*((uint64_t)time.tv_sec - t_st_.tv_sec) + time.tv_nsec - t_st_.tv_nsec;
            t_st_ = time;
            t_acc_ns_ += time_ns;
        }
        count_++;
        if (count_ >= count_max_) {
            fps_ = (static_cast<float>(count_))*1e9/t_acc_ns_;
            reset();
        }
    }
    float get_fps() {
        return fps_;
    }
 private:
    bool started_;
    struct timespec t_st_;
    int64_t t_acc_ns_;
    int count_max_;
    int count_;
    float fps_;
};