#include "lanes.h"

LaneBase::LaneBase(bool fs1, bool to, const cv::Rect &roi,
    int thseg1, int thcen, int thseg2, int waitfr)
    :
    fromSeg1(fs1), to(to), roi(roi),
    thSeg1(thseg1), thCen(thcen), thSeg2(thseg2),
    waitFramesAmt(waitfr)
{
    processVehicle = false;
    detectedCounter = 0;
    vehicleDetected= false;
    saveSeg1ROI = false;
    vehiclesExitedCnt = 0;
    waitNFramesCounter = 0;
}

/**
 * Highest level protected member function that is called on each frame from video.
 * 
 * Gets white pixels values from the Background Subtracted frame.
 * Calls isVehicleReady() if ROI monitors vehicles entering the intersection.
 * Calls tryIncreaseExitedCount() if ROI monitors vehicles leaving the intersection.
 * Ensures processVehicle: is true for only one frame when the vehicle is first detected in this ROI.
 * 
 * @param cv::Mat[in] a background subtracted matrix representing the current frame.
 * @return bool processVehicle that is true for only one frame if vehicle is first detected.
 */
bool LaneBase::isVehicleReady(cv::Mat &frameDilated) {
    // called on each frame.
    if(saveSeg1ROI) 
        return waitNframesSeg1(); // calls waitNframesSeg1 repeatedly until saveSeg1ROI flag is false.
    // local variables
    cv::Mat roiDilated(frameDilated, roi);
    cv::Mat seg1(roiDilated, seg1Rect);
    cv::Mat seg2(roiDilated, seg2Rect);
    cv::Mat segCenter(roiDilated, cenRect);

    int whitePixelsSeg1 = countNonZero(seg1);
    int whitePixelsCenter = countNonZero(segCenter);
    int whitePixelsSeg2 = countNonZero(seg2);

    if(to) {
        // if this ROI is used to monitor vehicles entering the intersection.
        vehicleDetected = detectVehicle(whitePixelsSeg1, whitePixelsCenter, whitePixelsSeg2);
    }
    else {
        // if this ROI is used to monitor vehicles leaving the intersection.
        // no need to return vehicleDetected, as in this case ROI is used to just increase counter of number of vehicles that left
        // the intersection through this ROI.
        tryIncreaseExitedCount(whitePixelsSeg1, whitePixelsCenter, whitePixelsSeg2);
    }

    // ensures that processVehicle is true for only one frame when the vehicle is first detected.
    processVehicle = (!detectedCounter && vehicleDetected);
    if(vehicleDetected) detectedCounter++;

    return processVehicle;
}

/**
 * Decides whether a vehicle is detected in the ROI or not.
 * 
 * It is only called for lanes monitoring vehicles entering the intersection.
 * 
 * @param int[in] number of white pixels in segment 1 of the ROI.
 * @param int[in] number of white pixels in the centre segment of the ROI.
 * @param int[in] number of white pixels in segment 2 of the ROI.
 * @return vehicleDetected bool: is true for each frame a vehicle is deemed present in the ROI.
 */
bool LaneBase::detectVehicle(int whitePixelsSeg1, int whitePixelsCenter, int whitePixelsSeg2) {
    // seg1 (left/top) and seg2 (right/bottom) both empty (below their thresholds of vehicles present).
    if((whitePixelsSeg1 < whPixThSeg1) && (whitePixelsSeg2 < whPixThSeg2)) {
        // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleDetected= false).
        if((whitePixelsCenter > whPixThCen) && (vehicleDetected == false)) {
            // vehicle is in the center
            // for a seg1 vehicle, return true N frames later.
            if(fromSeg1) { 
                saveSeg1ROI = true; // will call waitNframesSeg1 on next frame.
            }
            // for a seg2 vehicle, return true immediately.
            else {
                vehicleDetected = true;
            }
        }
    }
    // vehicle is leaving the ROI and approaching intersection.
    // seg1 (left for lateral lanes, top for frontal lanes).
    else if(fromSeg1 && detectedCounter) {
        if(whitePixelsSeg2 > whPixThSeg2) {
            // reset flags.
            vehicleDetected = false;
            detectedCounter = 0;
        }
    }
    // seg2
    else if(detectedCounter) {
        if(whitePixelsSeg1 > whPixThSeg1) {
            vehicleDetected = false;
            detectedCounter = 0;
        }
    }

    return vehicleDetected;
}

/**
 * Decides whether a vehicle has left the intersection through this ROI.
 * 
 * It is only called for lanes monitoring vehicles leaving the intersection.
 * Increments the vehiclesExitedCnt counter by 1 for each new vehicle leaving.
 * 
 * @param int[in] number of white pixels in segment 1 of the ROI.
 * @param int[in] number of white pixels in the centre segment of the ROI.
 * @param int[in] number of white pixels in segment 2 of the ROI.
 */
void LaneBase::tryIncreaseExitedCount(int whitePixelsSeg1, int whitePixelsCenter, int whitePixelsSeg2) {
    // seg1 (left/top) and seg2 (right/bottom) both empty (below their thresholds of vehicles present).
    if((whitePixelsSeg1 < whPixThSeg1) && (whitePixelsSeg2 < whPixThSeg2)) {
        // segCenter has vehicle present (above its threshold of vehicle present) and the vehicle is new (vehicleDetected= false).
        if((whitePixelsCenter > whPixThCen) && (vehicleDetected == false)) {
            // vehicle is in the center.
            vehicleDetected = true;
        }
    }
    // vehicle is leaving the ROI.
    // increase total count of number of vehicles that left from this ROI.
    else if(fromSeg1 && detectedCounter) {
        if(whitePixelsSeg1 > whPixThSeg1) {
            vehiclesExitedCnt++;
            vehicleDetected = false;
            detectedCounter = 0;
        }
    }
    else if(detectedCounter) {
        if(whitePixelsSeg2 > whPixThSeg2) {
            vehiclesExitedCnt++;
            vehicleDetected = false;
            detectedCounter = 0;
        }
    }
}

/**
 * Skips/waits N frames before signalling processVehicle = true
 * 
 * This is an additional option for top/left lanes, but can also be applied to bottom/right lanes.
 * There was a problem with detecting vehicles from top/left too early, so must wait a few frames
 * to get the vehicle centred in those ROIs.
 * 
 * @param return processVehicle bool.
 */
bool LaneBase::waitNframesSeg1() {
    if(waitNFramesCounter < waitFramesAmt) {
        waitNFramesCounter++;
        return processVehicle; // false;
    }
    // waited waitFramesAmt frames.
    // reset values.
    saveSeg1ROI = false; 
    waitNFramesCounter = 0;

    vehicleDetected = true;
    detectedCounter++;
    
    processVehicle = true;

    return processVehicle;
}

FrontalLane::FrontalLane(bool fs1, bool to, const cv::Rect &roi,
    int seg1h, int seg2h, int thseg1, int thcen, int thseg2, int waitfr)
    :
    LaneBase(fs1, to, roi, thseg1, thcen, thseg2, waitfr),
    seg1H(seg1h), seg2H(seg2h)
{
    // the formulae for calculating the different segments in the ROI differ between Frontal and Lateral lanes.
    seg1Area = roi.width*seg1H;
    cenArea = roi.width*(roi.height-seg1H-seg2H);
    seg2Area = roi.width*seg2H;
    whPixThSeg1 = seg1Area * thSeg1 / 100;
    whPixThCen = cenArea * thCen / 100;
    whPixThSeg2 = seg2Area * thSeg2 / 100;

    seg1Rect = cv::Rect(0, 0, roi.width, seg1H);
    cenRect = cv::Rect(0, seg1H, roi.width, roi.height - seg1H - seg2H);
    seg2Rect = cv::Rect(0, roi.height - seg2H, roi.width, seg2H);
}

LateralLane::LateralLane(bool fs1, bool to, const cv::Rect &roi,
    int seg1w, int seg2w, int thseg1, int thcen, int thseg2, int waitfr)
    :
    LaneBase(fs1, to, roi, thseg1, thcen, thseg2, waitfr),
    seg1W(seg1w), seg2W(seg2w)
{
    // the formulae for calculating the different segments in the ROI differ between Frontal and Lateral lanes.
    seg1Area = roi.height*seg1W;
    cenArea = roi.height*(roi.width-seg1W-seg2W);
    seg2Area = roi.height*seg2W;
    whPixThSeg1 = seg1Area * thSeg1 / 100;
    whPixThCen = cenArea * thCen / 100;
    whPixThSeg2 = seg2Area * thSeg2 / 100;

    seg1Rect = cv::Rect(0, 0, seg1W, roi.height);
    cenRect = cv::Rect(seg1W, 0, roi.width - seg1W - seg2W, roi.height);
    seg2Rect = cv::Rect(roi.width - seg2W, 0, seg2W, roi.height);
}