// note: "seg1" = "left" for lateral lanes, and = "top" for frontal lanes.
// note: "lane" == "ROI".

#pragma once

#include <stddef.h>
#include <opencv2/opencv.hpp>

/**
 * Cannot be initialised on its own. Parent class for initialisable classes.
 * 
 *  Represents the high level view of a Region Of Interest (ROI) positioned at an entry/exit point of the intersection.
 *  It should encompas a region wide enough to fit a vehicle of any class in question.
 */
class LaneBase {

protected:

    LaneBase(bool fs1,
        bool to,
        const cv::Rect &roi,
        int thseg1,
        int thcen,
        int thseg2,
        int waitfr);

    LaneBase();

public:
    /**
     * Called on each frame and tries to detect a vehicle using OpenCV Blob Detection method (Background Subtraction).
     */
    bool isVehicleReady(cv::Mat &frameDilated);

    /**
     * Gets the rectangle used for this object, i.e. for this ROI.
     * 
     * @param cv::Rect[in] whose values are overwritten with the ROI this object represents.
     */
    inline void getRect(cv::Rect *rc) {
        rc->x = roi.x;
        rc->y = roi.y;
        rc->width = roi.width;
        rc->height = roi.height;
    }

    /**
     * If this object is responsible for monitoring vehicles leaving the intersection.
     * 
     * @param int[out] the number of vehicles exited through this object's ROI.
     */
    inline int getVehiclesExitedCnt() { return vehiclesExitedCnt; }

protected:

    bool detectVehicle(int whitePixelsSeg1, int whitePixelsCenter, int whitePixelsSeg2);

    void tryIncreaseExitedCount(int whitePixelsSeg1, int whitePixelsCenter, int whitePixelsSeg2);

    bool waitNframesSeg1();

protected:
    // params to constr.
    int thSeg1, thCen, thSeg2; // threshold of white pixels for segments as percentages.
    int waitFramesAmt; // amount of frames to wait before processing frame if left/top ROI.
    bool fromSeg1; // true = left/top, false = right/bottom.
    bool to;  // true = to intersection, false = from intersection.
    cv::Rect roi; // the ROI this object represents.

    // derived from params.
    int seg1Area, cenArea, seg2Area;
    int whPixThSeg1, whPixThCen, whPixThSeg2; // threshold of white pixels for segments as absolute values.
    cv::Rect seg1Rect, cenRect, seg2Rect;

    // initialised to 0/false.
    bool vehicleDetected; // will be true for each frame the vehicle is detected.
    bool processVehicle; // will only be true for one frame if the vehicle is detected for the first time.
    int detectedCounter; // number of frames the vehicle is detected for.
    bool saveSeg1ROI; // used for processing left/top ROI frame N frames later.
    int waitNFramesCounter; // number of frames currently waited.
    int vehiclesExitedCnt; // if this ROI is used to monitor vehicles leaving the intersection, this is a counter for them.
};
/**
 * Represents a lane that focuses on vertically moving vehicles, with respect to the camera position.
 */
class FrontalLane: public LaneBase {
public:

    FrontalLane(bool fs1, bool to, const cv::Rect &roi,
        int seg1h, int seg2h, int thseg1, int thcen, int thseg2, int waitfr);

private:

    int seg1H, seg2H; // were originally needed for drawing purposes, now deprecated.
};

/**
 * Represents a lane that focuses on horizontally moving vehicles, with respect to the camera position.
 */
class LateralLane: public LaneBase {
public:

    LateralLane(bool fs1, bool to, const cv::Rect &roi,
        int seg1w, int seg2w, int thseg1, int thcen, int thseg2, int waitfr);

private:

    int seg1W, seg2W; // were originally needed for drawing purposes, now deprecated.
};