/*
    This is a test program for preprocessing an ROI from a video feed into a format suitable to input into a CNN.

    The main steps involved are:
        - resizing an interleaved ROI crop so that its longer side is 128 and preserving aspect ratio of original crop.
        - copy the resized crop into the centre of a grey  (128,128,3) interleaved matrix.
        - split the resultant matrix into R, G and B planes.
        - concatenate the planes to create a planar matrix.
        - rescale the matrix values to be in the range [-1,1].
*/

#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <opencv2/core/hal/interface.h>



const int NET_IN_SIZE = 128;

void save_img_interleaved(const cv::Mat* img, const char* name) {
    char fname[PATH_MAX];
    snprintf(fname, PATH_MAX, "%s.data", name);
    FILE* f = fopen(fname, "wb");
    if(f){
        fwrite(img->data, img->cols*img->rows, img->elemSize(), f);
        fclose(f);
    }
}

void save_planes(const cv::Mat *imgs, const char* name) {
    char fname[PATH_MAX];
    snprintf(fname, PATH_MAX, "%s.data", name);
    FILE* f = fopen(fname, "wb");
    if(f){
        fwrite(imgs[0].data, imgs[0].cols*imgs[0].rows, imgs[0].elemSize(), f);
        fwrite(imgs[1].data, imgs[1].cols*imgs[1].rows, imgs[1].elemSize(), f);
        fwrite(imgs[2].data, imgs[2].cols*imgs[2].rows, imgs[2].elemSize(), f);
        fclose(f);
    }
}

void dbg_mat_info(const cv::Mat *mat, const char *name) {
    printf("%s (%d, %d, %d) elemSize: %lu\n", name, mat->cols, mat->rows, mat->dims, mat->elemSize());
}

cv::Mat preprocess_frame(const cv::Mat *roi) {
    cv::Mat roi_l;
    int h = roi->size[0];
    int w = roi->size[1];
    int ww = NET_IN_SIZE, hh = NET_IN_SIZE;
    int off_x = 0, off_y = 0;
    if(w>h) {
        hh = NET_IN_SIZE*h/w;
        off_y = (NET_IN_SIZE - hh)/2;
    }
    else {
        ww = NET_IN_SIZE*w/h;
        off_x = (NET_IN_SIZE - ww)/2;
    }
    cv::resize(*roi, roi_l, cv::Size(ww,hh));                // resized frame to be copied into centre of grey square.
    cv::Mat greyPad(128, 128, CV_8UC3, cv::Scalar::all(80)); // interleaved grey (128,128,3) matrix. grey: all elements = 80.
    //std::cout << greyPad << std::endl;
    roi_l.copyTo(greyPad(cv::Rect(off_x, off_y, roi_l.cols, roi_l.rows)));
    cv::Mat planes[3];
    cv::split(greyPad, planes);                              // split greyPad matrix with frame copied into R, G and B planes.
    cv::Mat planar; // NOTE: shape of this mat seems odd, but should be able to load into CUDA memory continuously:  (128, 384, 2) elemSize: 1.
    cv::vconcat(planes, 3, planar);                          // concatenate planes into RGB planar image with G plane after R etc.
    //dbg_mat_info(&planar, "planar");
    cv::Mat planar_f;                                        // shape: (128, 384, 2) elemSize: 4.
    constexpr float coeff = 1.f/127.5f;                      // coefficient for rescaling all elements of matrix to [-1,1] range.
    planar.convertTo(planar_f, CV_32FC3);                    // must cast to float32 first as float*int produces int for some reason.
    return (coeff*planar_f) - 1;                             // planar float is ready to be inputted into CNN.
}

int main(int argc, char* argv[]) {
    std::string video_path = "/home/andrei/Desktop/final-year-project-results/videos/livecam/lagrange_15m_best.mp4";
    cv::Mat frame;
    const cv::Rect regions[] = {
        cv::Rect(10, 10, 200, 100),
        cv::Rect(10, 10, 100, 200)
    };

    cv::VideoCapture capture(video_path);
    if (!capture.isOpened()){
        //error in opening the video input
        std::cerr << "Unable to open: " << video_path << std::endl;
        return 1;
    }

    int playVid = true;
    while (true) {
        // std::cout << "OpenCV version : " << CV_VERSION <<            std::endl;
	    // std::cout << "Major version : " << CV_MAJOR_VERSION <<       std::endl;
	    // std::cout << "Minor version : " << CV_MINOR_VERSION <<       std::endl;
	    // std::cout << "Subminor version : " << CV_SUBMINOR_VERSION << std::endl;
        capture >> frame;
        cv::Mat reg0 = frame(regions[0]);
        cv::Mat preparedROI = preprocess_frame(&reg0);
        std::cout << preparedROI << std::endl;
        dbg_mat_info(&preparedROI, "preparedROI");

        // int sz[] = {3, NET_IN_SIZE, NET_IN_SIZE};
        
        // cv::Mat greyPl3D(120, 128, CV_8UC3, cv::Scalar(0)); // (3,128,128) greyed out Mat.
        // dbg_mat_info(&greyPl3D);
        // int sz2[] = {120, 128, 3};
        // cv::Mat img_grey(3, sz2, CV_8UC1, cv::Scalar(0));
        // std::cout << "1" << std::endl;
        // dbg_mat_info(&img_grey);
        // std::cout << "2" << std::endl;
        // int size[3] = { 5, 4, 3 };
        // cv::Mat M(3, size, CV_32F, cv::Scalar(0));
        // dbg_mat_info(&M);
        // //std::cout << img_grey << std::endl;

        // std::cout << "A" << std::endl;
        // cv::Mat roi3D = greyPl3D(cv::Rect(off_x, off_y, ww, hh)); // (3,ww,hh) greyed out Mat.
        // std::cout << "B" << std::endl;
        // cv::mixChannels(planes[0], roi3D, {0, 0});
        // std::cout << "C" << std::endl;
        // cv::mixChannels(planes[1], roi3D, {0, 1});
        // cv::mixChannels(planes[2], roi3D, {0, 2});
        // dbg_mat_info(&roi3D);


        //save_planes(planes, "reg0_planar");
        //save_img_interleaved(&reg0, "reg00");

        return 0;
    }
    
}
