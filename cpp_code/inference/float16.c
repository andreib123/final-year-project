#include "float16.h"

/// Convert IEEE single-precision to half-precision.
/// Credit for this goes to [Jeroen van der Zijp](ftp://ftp.fox-toolkit.org/pub/fasthalffloatconversion.pdf).
/// \tparam R rounding mode to use, `std::round_indeterminate` for fastest rounding
/// \param value single-precision value
/// \return binary representation of half-precision value

float16_t float16_from_float(float value) {
    uint32_t xp = *((uint32_t*)&value);

    uint32_t xs, xe, xm;
    uint16_t hp; // Type pun output as an unsigned 16-bit int
    uint16_t hs, he, hm;
    int hes;

    if( (xp & 0x7FFFFFFFu) == 0 ) { // Signed zero
        hp = (xp >> 16); // Return the signed zero
    } else { // Not zero
        xs = (xp & 0x80000000u); // Pick off sign bit
        xe = (xp & 0x7F800000u); // Pick off ebitsonent bits
        xm = (xp & 0x007FFFFFu); // Pick off mantissa bits
        if( xe == 0 ) {  // Denormal will underflow, return a signed zero
            hp = (uint16_t)(xs >> 16);
        } else if(xe == 0x7F800000u) { // Inf or NaN (all the exponent bits are set)
            if(xm == 0) { // If mantissa is zero ...
                hp = (uint16_t)((xs >> 16) | 0x7C00u); // Signed Inf
            } else {
                hp = (uint16_t)0xFE00u; // NaN, only 1st mantissa bit set
            }
        } else {
            hs = (uint16_t)(xs >> 16); // Sign bit
            hes = (int)(xe >> 23) - 127 + 15; // Exponent unbias the single, then bias the halfp
            if( hes >= 0x1F ) {  // Overflow
                hp = (uint16_t)((xs >> 16) | 0x7C00u); //Signed Inf
            } else if( hes <= 0 ) {  // Underflow
                if( (14 - hes) > 24 ) {  // Mantissa shifted all the way off & no rounding possibility
                    hm = (uint16_t) 0u;  // Set mantissa to zero
                } else {
                    xm |= 0x00800000u;  // Add the hidden leading bit
                    hm = (uint16_t)(xm >> (14 - hes)); // Mantissa
                    if( (xm >> (13 - hes)) & 0x00000001u ) // Check for rounding
                        hm += (uint16_t)1u; // Round, might overflow into exp bit, but this is OK
                }
                hp = (hs | hm); // Combine sign bit and mantissa xp, biased ebitsonent is zero
            } else {
                he = (uint16_t)(hes << 10); // Exponent
                hm = (uint16_t)(xm >> 13); // Mantissa
                if( xm & 0x00001000u ) // Check for rounding
                    hp = (hs | he | hm) + (uint16_t)1u; // Round, might overflow to inf, this is OK
                else
                    hp = (hs | he | hm);  // No rounding
            }
        }
    }

    return hp;
}

/// Convert half-precision to IEEE single-precision.
/// Credit for this goes to [Jeroen van der Zijp](ftp://ftp.fox-toolkit.org/pub/fasthalffloatconversion.pdf).
/// \param value binary representation of half-precision value
/// \return single-precision value
float float16_to_float(float16_t value) {
    uint16_t hp = value; // Type pun input as an unsigned 16-bit int
    uint32_t xp;             // Type pun output as an unsigned 32-bit int
    uint16_t hs, he, hm;
    uint32_t xs, xe, xm;
    int xes;
    int e;

    if( (hp & 0x7FFFu) == 0 ) {  // Signed zero
        xp = ((uint32_t) hp) << 16;  // Return the signed zero
    } else { // Not zero
        hs = hp & 0x8000u;  // Pick off sign bit
        he = hp & 0x7C00u;  // Pick off exponent bits
        hm = hp & 0x03FFu;  // Pick off mantissa bits
        if( he == 0 ) {  // Denormal will convert to normalized
            e = -1; // The following loop figures out how much extra to adjust the exponent
            do {
                e++;
                hm <<= 1;
            } while( (hm & 0x0400u) == 0 ); // Shift until leading bit overflows into exponent bit
            xs  = ((uint32_t) hs) << 16; // Sign bit
            xes = ((int) (he >> 10)) - 15 + 127 - e; // Exponent unbias the halfp, then bias the single
            xe  = (uint32_t) (xes << 23); // Exponent
            xm  = ((uint32_t) (hm & 0x03FFu)) << 13; // Mantissa
            xp  = (uint32_t)(xs | xe | xm); // Combine sign bit, exponent bits, and mantissa bits
        } else if( he == 0x7C00u ) {  // Inf or NaN (all the exponent bits are set)
            if( hm == 0 ) { // If mantissa is zero ...
                xp = (((uint32_t) hs) << 16) | ((uint32_t) 0x7F800000u); // Signed Inf
            } else {
                xp = (uint32_t) 0xFFC00000u; // NaN, only 1st mantissa bit set
            }
        } else { // Normalized number
            xs  = ((uint32_t) hs) << 16; // Sign bit
            xes = ((uint32_t) (he >> 10)) - 15 + 127; // Exponent unbias the halfp, then bias the single
            xe  = (uint32_t) (xes << 23); // Exponent
            xm  = ((uint32_t) hm) << 13; // Mantissa
            xp  = (uint32_t)(xs | xe | xm); // Combine sign bit, exponent bits, and mantissa bits
        }
    }

    return *((float *)&xp);
}