#pragma once

#include <cstdint>

//#include "half.hpp"

typedef uint16_t float16_t;

#ifdef __cplusplus
extern "C" {
#endif

float16_t float16_from_float(float f32);
float float16_to_float(float16_t f16);

#ifdef __cplusplus
}
#endif
