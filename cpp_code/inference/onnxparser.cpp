/**
 * this is a test program for running inference on one image and seeing the probabilites returned.
 */

#include <NvInfer.h>
#include <NvOnnxParser.h>
#include "logger.h"
#include <iostream>
#include <string.h>
#include <fstream>
#include <cuda_runtime_api.h>
// #include "float16.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>



class InferenceApp {
public:
    InferenceApp() {}

    void preprocessInput(const cv::Mat *input, cv::Mat *out) const;

    bool loadOnnx(nvinfer1::IHostMemory*& trtModelStream);

    void run(nvinfer1::IExecutionContext* context);

    Logger& getLogger() { return logger; }

private:
    static const int INPUT_H = 128;
    static const int INPUT_W = 128;
    //static const int OUTPUT_SIZE = 126 * 126 * 32;
    static const int OUTPUT_SIZE = 4;
    static const int BATCH_SIZE = 1;

    Logger logger;
};

void InferenceApp::preprocessInput(const cv::Mat *input, cv::Mat *out) const {
    // The main steps involved are:
    //  - resizing an interleaved input crop so that its longer side is 128 and preserving aspect ratio of original crop.
    //  - copy the resized crop into the centre of a grey  (128,128,3) interleaved matrix.
    //  - split the resultant matrix into R, G and B planes.
    //  - concatenate the planes to create a planar matrix.
    //  - rescale the matrix values to be in the range [-1,1].

    int NET_IN_SIDE = 128;
    cv::Mat input_l;
    int h = input->size[0];
    int w = input->size[1];
    int ww = NET_IN_SIDE, hh = NET_IN_SIDE;
    int off_x = 0, off_y = 0;
    if(w>h) {
        hh = NET_IN_SIDE*h/w;
        off_y = (NET_IN_SIDE - hh)/2;
    }
    else {
        ww = NET_IN_SIDE*w/h;
        off_x = (NET_IN_SIDE - ww)/2;
    }
    cv::resize(*input, input_l, cv::Size(ww,hh));            // resized frame to be copied into centre of grey square.
    cv::Mat greyPad(128, 128, CV_8UC3, cv::Scalar::all(80)); // interleaved grey (128,128,3) matrix. grey: all elements = 80.
    //std::cout << greyPad << std::endl;
    input_l.copyTo(greyPad(cv::Rect(off_x, off_y, input_l.cols, input_l.rows)));
    cv::Mat planes[3];
    cv::split(greyPad, planes);                              // split greyPad matrix with frame copied into R, G and B planes.
    cv::Mat planar; // NOTE: shape of this mat seems odd, but should be able to load into CUDA memory continuously:  (128, 384, 2) elemSize: 1.
    cv::vconcat(planes, 3, planar);                          // concatenate planes into RGB planar image with G plane after R etc.
    //dbg_mat_info(&planar, "planar");
    cv::Mat planar_f;                                        // shape: (128, 384, 2) elemSize: 4.
    constexpr float coeff = 1.f/127.5f;                      // coefficient for rescaling all elements of matrix to [-1,1] range.
    planar.convertTo(planar_f, CV_32FC3);                    // must cast to float32 first as float*int produces int for some reason.
    if(out)
        *out = (coeff*planar_f) - 1;                         // planar float is ready to be inputted into CNN.
}

bool InferenceApp::loadOnnx(nvinfer1::IHostMemory*& trtModelStream) {
 
    nvinfer1::IBuilder* builder = nvinfer1::createInferBuilder(logger.getTRTLogger());
    assert(builder != nullptr);

    nvinfer1::INetworkDefinition* trtNwk = builder->createNetwork();

    nvonnxparser::IParser* parser = nvonnxparser::createParser(*trtNwk, logger.getTRTLogger());

    bool parsed = parser->parseFromFile(
        "/home/andrei/Desktop/mycnn_pf32resc.onnx",
        static_cast<int>(logger.getReportableSeverity()));
    if(!parsed) {
        std::cout << "Failure while parsing ONNX file" << std::endl;
        return false;
    }
    
    nvinfer1::Dims inputDims = trtNwk->getInput(0)->getDimensions();
    nvinfer1::Dims outputDims = trtNwk->getOutput(0)->getDimensions();
    std::cout << "input:  (" << inputDims.d[0] << "," << inputDims.d[1] << "," << inputDims.d[2] << ")" << std::endl;
    std::cout << "output: (" << outputDims.d[0] << "," << outputDims.d[1] << "," << outputDims.d[2] << ")" << std::endl;

    //Optional - uncomment below lines to view network layer information
    //config->setPrintLayerInfo(true);
    //parser->reportParsingInfo();

    builder->setMaxBatchSize(BATCH_SIZE);
    builder->setMaxWorkspaceSize(1 << 20); // 2^20 B size.
    builder->setFp16Mode(0);
    builder->setInt8Mode(0); 

    nvinfer1::ICudaEngine *engine = builder->buildCudaEngine(*trtNwk);
    assert(engine);
    trtModelStream = engine->serialize();

    engine->destroy();
    builder->destroy();
    trtNwk->destroy();
    parser->destroy();

    return (trtModelStream != nullptr);
}

void InferenceApp::run(nvinfer1::IExecutionContext* context) {
    const nvinfer1::ICudaEngine& engine = context->getEngine();

    int inputBIndex = engine.getBindingIndex("conv2d_input:0"); // input and output names of model can be found in a model viewer like Netron.
    int outputBIndex = engine.getBindingIndex("Identity:0");
    void* buffers[2] = {0}; // stores inputBuffer pointer and outputBuffer pointer.
    uint8_t* inputBuffer = nullptr; // binary image.
    float* outputBuffer = new float[OUTPUT_SIZE]; // predictions array having 4 float32 probability scores.
    //nvinfer1::DataType inputDatatype = engine->getBindingDataType(inputBIndex);
    //std::cout << int(inputDatatype) << std::endl;

    // std::ifstream file;
    // file.open("/home/andrei/Desktop/imgs_test/sedan_128pf32rgb_resc.data",
    //           std::ios::in|std::ios::binary|std::ios::ate);

    // if(file.is_open()) {
    //     std::streampos size = file.tellg();
    //     file.seekg (0, std::ios::beg);
    //     inputBuffer = new char[size];
    //     file.read(inputBuffer, size);
    //     file.close();
    // }

    cv::Mat orig = cv::imread("/home/andrei/Desktop/final-year-project-results/images/private/left/orig/good/pickup/pickup_orig_good1.jpeg");
    cv::Mat resc;
    preprocessInput(&orig, &resc);
    inputBuffer = resc.data;

    const size_t inBufBytes = INPUT_H * INPUT_W * 3 * sizeof(float);

    // create GPU buffers and a stream.
    cudaMalloc(&buffers[inputBIndex], BATCH_SIZE * inBufBytes);
    cudaMalloc(&buffers[outputBIndex], BATCH_SIZE * OUTPUT_SIZE * sizeof(float));
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    
    // float* inputOnes = (float*)new uint8_t[inBufBytes];
    // float* inputZeros = (float*)new uint8_t[inBufBytes];
    // memset(inputZeros, 0x00, inBufBytes);
    // for (int i = 0, cnt = INPUT_H * INPUT_W * 3; cnt; cnt--, i++) { // faster to check against 0 than using i <= INPUT_H * INPUT_W * 3.
    //     inputOnes[i] = 1.f;
    // }
    // DMA the input to the GPU, execute the batch asynchronously, and DMA it back:
    cudaMemcpyAsync(buffers[inputBIndex], inputBuffer, BATCH_SIZE * inBufBytes, cudaMemcpyHostToDevice, stream);
    context->enqueue(BATCH_SIZE, buffers, stream, nullptr);
    //context->execute(1, buffers);
    cudaMemcpyAsync(outputBuffer, buffers[outputBIndex], BATCH_SIZE * OUTPUT_SIZE * sizeof(float), cudaMemcpyDeviceToHost, stream);
    cudaStreamSynchronize(stream);

    // release the stream and the buffers
    cudaStreamDestroy(stream);
    cudaFree(buffers[inputBIndex]);
    cudaFree(buffers[outputBIndex]);

    // std::ofstream fileOut;
    // fileOut.open("/home/andrei/Desktop/final-year-project-results/images/test/onesout.data",
    //           std::ios::out|std::ios::binary);

    // if(fileOut.is_open()) {
    //     fileOut.write((char*)outputBuffer, OUTPUT_SIZE * sizeof(float));
    //     fileOut.close();
    //}

    // if using float16.
    // std::cout << "Probabilities ";
    // for(int i = 0; i < 4; i++) {
    //     float a = *(outputBuffer + i);
    //     printf("%f ", floato_float(a));
    // }
    // std::cout << std::endl;

    // if using float.
    std::cout << "Probabilities ";
    for(int i = 0; i < 4; i++) {
        printf("%f ", *(outputBuffer + i));
    }
    std::cout << std::endl;

    std::free(inputBuffer);
    std::free(outputBuffer);
    cudaStreamDestroy(stream);
}

int main(int argc, char* argv[]) {
    InferenceApp app;

    nvinfer1::IHostMemory* trtModelStream = nullptr;
    
    if (!app.loadOnnx(trtModelStream))
        return -1;

    // deserialize the engine
    nvinfer1::IRuntime* runtime = nvinfer1::createInferRuntime(app.getLogger());
    assert(runtime != nullptr);

    nvinfer1::ICudaEngine* engine = runtime->deserializeCudaEngine(
        trtModelStream->data(),
        trtModelStream->size(),
        nullptr);
    assert(engine != nullptr);
    trtModelStream->destroy();
    nvinfer1::IExecutionContext* context = engine->createExecutionContext();
    assert(context != nullptr);

    // do inference.
    app.run(context);

    context->destroy();
    engine->destroy();
    runtime->destroy();
    return 0;
}
