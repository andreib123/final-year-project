# to find out how many train/valid/test images are being used, run the createtesttrainsplit.py script.
# this will reshuffle images in the corresponding folders.

import sys
import json
import time
import math
import wandb
import os,os.path
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from wandb.keras import WandbCallback

CLASS_NAMES = ["pickup-truck", "sedan", "suv", "van"]

# NOTE: must give 1 command line argument: backend (TF) engine_location.
# location of dataset directory depending on where the script is run.
engine_location = sys.argv[1]

if(engine_location == "local"):
    dst_path_start_private = "/home/andrei/Desktop/final-year-project-results/images/private/dataset/"
    dst_path_start_public = "/home/andrei/Desktop/final-year-project-results/images/public/dataset/"
elif(engine_location == "google"):
    dst_path_start_private = "/content/drive/My Drive/final-year-project/images/private/dataset/"
    dst_path_start_public = "/content/drive/My Drive/final-year-project/images/public/dataset/"
elif(engine_location == "gpu-server"):
    dst_path_start_private = "/media/RAID5/abarcovschi/final-year-project-results/images/private/dataset/"
    dst_path_start_public = "/media/RAID5/abarcovschi/final-year-project-results/images/public/dataset/"
else:
    raise Exception("InvalidArgumentException: please specify 'local', 'google' or 'gpu-server' as first argv!")

# get hyperparameters from JSON file.
script_dir = os.path.dirname(__file__)
file_path = os.path.join(script_dir, 'hparams_tlearn.json')
with open(file_path, encoding='utf-8') as json_file:
    json_hparams = json.loads(json_file.read())

# HYPERPARAMETERS.
MODEL_NAME = json_hparams['model name']
TRAIN_PRIV = (json_hparams['train data'] == 'private')
IMG_SIZE = json_hparams['image size']
COLOUR_CHANNELS = 3
PLANAR = (json_hparams['planar'] == 'true') # unfortunately cannot use planar format for MobileNetV2.
IMG_SHAPE = (COLOUR_CHANNELS, IMG_SIZE, IMG_SIZE) if PLANAR else (IMG_SIZE, IMG_SIZE, COLOUR_CHANNELS)
BATCH_SIZE = json_hparams['batch size']
BASE_LEARNING_RATE = json_hparams['base learning rate']
INITIAL_EPOCHS = json_hparams['initial epochs']
VALIDATION_STEPS = json_hparams['validation steps']
FINE_TUNE_EPOCHS = json_hparams['fine tune epochs']
SHUFFLE_BUFFER_SIZE = json_hparams['buffer size']

# Weights and Biases setup.
wandb.init(project="final-year-project-transferlearn",
           notes=json_hparams["wandb notes"],
           tags=json_hparams["wandb tags"],
           config={"buffer size": SHUFFLE_BUFFER_SIZE,
                    "image shape": IMG_SHAPE,
                    "batch size": BATCH_SIZE,
                    "base learning rate": BASE_LEARNING_RATE,
                    "intial epochs": INITIAL_EPOCHS,
                    "validation steps": VALIDATION_STEPS,
                    "fine tune epochs": FINE_TUNE_EPOCHS})
#WANDB_MODE = dryrun # for testing, disables wandb syncing.

# create a per class accuracy score as a fraction of the total attempted predictions for each true class.
def per_class_accuracy(actual_labels, predicted_labels):
    if(len(actual_labels) != len(predicted_labels)): raise Exception("len(actual_labels) != len(predicted_labels!!!")
    correct_pickups=total_pickups=correct_sedans=total_sedans=correct_suvs=total_suvs=correct_vans=total_vans=0
    for i in range(len(predicted_labels)):
        actual = actual_labels[i,:]
        predicted = predicted_labels[i,:]
        actual_argmax_idx = np.argmax(actual)
        predicted_argmax_idx = np.argmax(predicted)
        if(actual_argmax_idx == 0):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_pickups+=1
                total_pickups+=1
            else:
                total_pickups+=1
        elif(actual_argmax_idx == 1):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_sedans+=1
                total_sedans+=1
            else:
                total_sedans+=1
        elif(actual_argmax_idx == 2):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_suvs+=1
                total_suvs+=1
            else:
                total_suvs+=1
        else:
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_vans+=1
                total_vans+=1
            else:
                total_vans+=1
    frac_correct_pickups = correct_pickups / total_pickups
    frac_correct_sedans = correct_sedans / total_sedans
    frac_correct_suvs = correct_suvs / total_suvs
    frac_correct_vans = correct_vans / total_vans
    return (frac_correct_pickups, frac_correct_sedans, frac_correct_suvs, frac_correct_vans)

# arg private is a bool that specifies to use private or public dataset for training.
def create_unlabeled_train_data(private):
    # create unlabeled lists of images as tf.Dataset objects for training (from folders train and valid).
    if (private):
        # private images.
        list_ds_train = tf.data.Dataset.list_files(dst_path_start_private + 'train/*/*')
        list_ds_valid = tf.data.Dataset.list_files(dst_path_start_private + 'valid/*/*')
    else:
        # public images.
        list_ds_train = tf.data.Dataset.list_files(dst_path_start_public + 'train/*/*')
        list_ds_valid = tf.data.Dataset.list_files(dst_path_start_public + 'valid/*/*')
    return (list_ds_train, list_ds_valid)

# labels_dict : {ind_label: count_label}
# mu : parameter to tune: when mu -> 0, weights -> 1.0 (for larger datasets mu should get smaller to smooth out weigths for imbalanced classes more).
# for very large datasets consider taking log of score.
def create_class_weights(labels_dict,mu=1):
    total = sum(labels_dict.values())
    class_weight = dict()

    for key in labels_dict.keys():
        score = mu*total/float(labels_dict[key])
        class_weight[key] = score if score > 1.0 else 1.0

    return class_weight

def get_label(file_path):
    # convert the path to a list of path components.
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory.
    a = parts[-2] == CLASS_NAMES
    # label for an image will be a one-hot encoded version of ["pickup-truck", "sedan", "suv", "van"], e.g. [1,0,0,0].
    return tf.cast(a, tf.int32)

def decode_img_planar(img):
    # convert the JPEG image string to a 3D uint8 interleaved tensor (RGB24).
    img = tf.image.decode_jpeg(img, channels=COLOUR_CHANNELS)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    r = img[:,:,0] # R plane.
    g = img[:,:,1] # G plane.
    b = img[:,:,2] # B plane.
    # convert image to planar tensor of shape COLOUR_CHANNELSxIMG_SIZExIMG_SIZE.
    img = tf.concat([r,g,b],0)
    img = tf.reshape(img, [3,128,128])
    # cast image to float32 format.
    img = tf.cast(img, tf.float32)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

def decode_img_interleaved(img):
    # convert the JPEG image string to a 3D uint8 interleaved tensor (RGB24).
    img = tf.image.decode_jpeg(img, channels=COLOUR_CHANNELS)
    # cast image to float32 format.
    img = tf.cast(img, tf.float32)
    # Use `convert_image_dtype` to convert to floats in the [0,1] range.
    #img = tf.image.convert_image_dtype(img, tf.float32)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    # resize the image to the shape of IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    return tf.image.resize(img, [IMG_SIZE, IMG_SIZE])

def process_path(file_path):
    label = get_label(file_path)
    # load the raw data from the file as a string
    img = tf.io.read_file(file_path)
    if(PLANAR):
        img = decode_img_planar(img)
    else:
        img = decode_img_interleaved(img)
    return img, label

list_ds_train, list_ds_valid = create_unlabeled_train_data(TRAIN_PRIV)
    
if(json_hparams['test data'] == 'private'):
    # private images.
    list_ds_test = tf.data.Dataset.list_files(dst_path_start_private + 'test/*/*')
elif(json_hparams['test data'] == 'public'):
    # public images.
    list_ds_test = tf.data.Dataset.list_files(dst_path_start_public + 'test/*/*')
else:
    raise Exception("Test data hyperparam in JSON hparam file is incorrect. Please specify 'public' or 'private' for 'test data' hparam.")

# Use tf.Dataset.map to create tf.Dataset objects containing (image, label) pairs. 
# image and label objects are of type tf.Tensor.
labeled_ds_train = list_ds_train.map(process_path)
labeled_ds_valid = list_ds_valid.map(process_path)
labeled_ds_test = list_ds_test.map(process_path)
test_labels = np.array([x[1].numpy() for x in list(labeled_ds_test)])

# inspect train data.
# image and label are tf.Tensor objects.
# for image, label in labeled_ds_train.take(2):
#     #pass
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# inspect test data
# for image, label in labeled_ds_test.take(2):
#     #pass
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# create batches.
# shuffle the train batch data.
train_batches = labeled_ds_train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = labeled_ds_valid.batch(BATCH_SIZE)
test_batches = labeled_ds_test.batch(BATCH_SIZE)

# inspect a batch of data.
# for image_batch, label_batch in train_batches.take(1):
#        pass
# print(image_batch.shape)

# instantiate a NN model pre-loaded with weights trained on a dataset.
# include_top=False loads the model without the top calssification layer, and the last layer will be the bottleneck layer.
# base model is used as a feature extractor.
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                                include_top=False,
                                                weights='imagenet')

# forward pass of model on one batch.
# feature_batch = base_model(image_batch)
# print(feature_batch.shape)

# freeze base model and use it as a feature extractor only.
base_model.trainable = False

global_average_layer = tf.keras.layers.GlobalAveragePooling2D() # add layer on top of base model to average every 5x5 into 1 value to create a vector. (BATCH_SIZEx5x5x1280 -> BATCH_SIZEx1x1x1280 (which is outputted as BATCH_SIZEx1280)).
# feature_batch_average = global_average_layer(feature_batch) # output of the global_average_layer.
prediction_layer = tf.keras.layers.Dense(4, activation="softmax") # add densely connected (fully connected) layer that outputs one prediction. No activation function here as binary classification (pos = cass 1, neg = class 0).
# prediction_batch = prediction_layer(feature_batch_average) # outputted predictions (BATCH_SIZEx1).

# stack the base model and additional layers into a combined new model.
model = tf.keras.Sequential([
    base_model,
    global_average_layer,
    prediction_layer])

# must compile the model before training.
model.compile(optimizer=tf.optimizers.Adam(learning_rate=BASE_LEARNING_RATE),
              loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary()

# print(model.summary()) # parameters from the base model are frozen.
# print(len(model.trainable_variables)) # returns: 2 (these are tf.Variable objects: one for weights and one for biases).

# dictionary of number of images for each class present in the train directory.
train_base_path = dst_path_start_private + "train/" if TRAIN_PRIV else dst_path_start_public + "train/"

train_img_amounts = {
    0: len([name for name in os.listdir(train_base_path+"pickup-truck") if os.path.isfile(os.path.join(train_base_path+"pickup-truck", name))]),
    1: len([name for name in os.listdir(train_base_path+"sedan") if os.path.isfile(os.path.join(train_base_path+"sedan", name))]),
    2: len([name for name in os.listdir(train_base_path+"suv") if os.path.isfile(os.path.join(train_base_path+"suv", name))]),
    3: len([name for name in os.listdir(train_base_path+"van") if os.path.isfile(os.path.join(train_base_path+"van", name))])
}
class_weights = create_class_weights(train_img_amounts)

tic = time.perf_counter()

# evaluate initial accuracy of model before training. Different from predict, eval doesn't return predictions.
loss0, accuracy0 = model.evaluate(validation_batches, steps = VALIDATION_STEPS)
wandb.log({'initial_accuracy': accuracy0, 'initial_loss': loss0})

print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

# train the model.
history = model.fit(train_batches,
                    epochs=INITIAL_EPOCHS,
                    validation_data=validation_batches,
                    class_weight=class_weights,
                    callbacks=[WandbCallback()])

# acc = history.history['accuracy']
# val_acc = history.history['val_accuracy']

# loss = history.history['loss']
# val_loss = history.history['val_loss']

# plt.figure(figsize=(8, 8))
# plt.subplot(2, 1, 1)
# plt.plot(acc, label='Training Accuracy')
# plt.plot(val_acc, label='Validation Accuracy')
# plt.legend(loc='lower right')
# plt.ylabel('Accuracy')
# plt.title('Training and Validation Accuracy')

# plt.subplot(2, 1, 2)
# plt.plot(loss, label='Training Loss')
# plt.plot(val_loss, label='Validation Loss')
# plt.legend(loc='upper right')
# plt.ylabel('Cross Entropy')
# plt.title('Training and Validation Loss')
# plt.xlabel('epoch')
# plt.show()

loss1, accuracy1 = model.evaluate(test_batches, steps = VALIDATION_STEPS)
wandb.log({'test_accuracy0': accuracy1, 'test_loss0': loss1})
print("test loss: {:.2f}".format(loss1))
print("test accuracy: {:.2f}".format(accuracy1))

# can increase performance (accuracy) of model further by training the top layers of the base model.
# this will tune the weights from generic feature maps to features associated specifically with the dataset.
# do this only after training the top layer with the base model frozen.
# only fine tune a small number of top layers of base model, not the whole base model.

#print("Number of layers in the base model: ", len(base_model.layers))

# unfreeze base model.
base_model.trainable = True
# fine tune from this layer upwards.
fine_tune_at = 100
# freeze all layers before fine_tune_at layer.
for layer in base_model.layers[:fine_tune_at]:
    layer.trainable = False

# FINE TUNING ON PRIVATE DATA.
# compile model again with lower learning rate.
model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
              optimizer=tf.optimizers.Adam(learning_rate=BASE_LEARNING_RATE/2),
              metrics=['accuracy'])

# make train data from private dataset and shuffle test data again.
total_epochs =  INITIAL_EPOCHS + FINE_TUNE_EPOCHS
list_ds_train, list_ds_valid = create_unlabeled_train_data(True)
labeled_ds_train = list_ds_train.map(process_path)
labeled_ds_valid = list_ds_valid.map(process_path)
train_batches = labeled_ds_train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = labeled_ds_valid.batch(BATCH_SIZE)

# inspect train data
# for image, label in labeled_ds_train.take(2):
#     #pass
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# create class weights for training.
# dictionary of number of images for each class present in the train directory.
train_base_path = dst_path_start_private + "train/"
train_img_amounts = {
    0: len([name for name in os.listdir(train_base_path+"pickup-truck") if os.path.isfile(os.path.join(train_base_path+"pickup-truck", name))]),
    1: len([name for name in os.listdir(train_base_path+"sedan") if os.path.isfile(os.path.join(train_base_path+"sedan", name))]),
    2: len([name for name in os.listdir(train_base_path+"suv") if os.path.isfile(os.path.join(train_base_path+"suv", name))]),
    3: len([name for name in os.listdir(train_base_path+"van") if os.path.isfile(os.path.join(train_base_path+"van", name))])
}
class_weights = create_class_weights(train_img_amounts)

history_fine = model.fit(train_batches,
                         epochs=total_epochs,
                         initial_epoch =  history.epoch[-1],
                         validation_data=validation_batches,
                         class_weight=class_weights,
                         callbacks=[WandbCallback()])

model.save(MODEL_NAME)

toc = time.perf_counter()
print(f"Trained model in {(toc - tic)/60:0.4f} minutes")

# if validation loss much higher than training loss -> overfitting.
# this means the new training set is relatively small and similar to the original datasets used to train the base model (ImageNet).

# acc += history_fine.history['accuracy']
# val_acc += history_fine.history['val_accuracy']

# loss += history_fine.history['loss']
# val_loss += history_fine.history['val_loss']

# plt.figure(figsize=(8, 8))
# plt.subplot(2, 1, 1)
# plt.plot(acc, label='Training Accuracy')
# plt.plot(val_acc, label='Validation Accuracy')
# plt.plot([INITIAL_EPOCHS-1,INITIAL_EPOCHS-1],
#           plt.ylim(), label='Start Fine Tuning')
# plt.legend(loc='lower right')
# plt.title('Training and Validation Accuracy')

# plt.subplot(2, 1, 2)
# plt.plot(loss, label='Training Loss')
# plt.plot(val_loss, label='Validation Loss')
# plt.plot([INITIAL_EPOCHS-1,INITIAL_EPOCHS-1],
#          plt.ylim(), label='Start Fine Tuning')
# plt.legend(loc='upper right')
# plt.title('Training and Validation Loss')
# plt.xlabel('epoch')
# plt.show()

loss2, accuracy2 = model.evaluate(test_batches, steps = VALIDATION_STEPS)
wandb.log({'finetune_test_accuracy': accuracy2, 'finetune_test_loss': loss2})
print("fine tuned test loss: {:.2f}".format(loss2))
print("fine tuned test accuracy: {:.2f}".format(accuracy2))

test_predictions = model.predict(test_batches)
frac_correct_pickups, frac_correct_sedans, frac_correct_suvs, frac_correct_vans = per_class_accuracy(test_labels, test_predictions)
print("Fraction of pickup-trucks correctly classified on test data after fine tuning: " + str(frac_correct_pickups))
print("Fraction of sedans correctly classified on test data after fine tuning: " + str(frac_correct_sedans))
print("Fraction of SUVs correctly classified on test data after fine tuning: " + str(frac_correct_suvs))
print("Fraction of vans correctly classified on test data after fine tuning: " + str(frac_correct_vans))