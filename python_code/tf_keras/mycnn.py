# to find out how many train/valid/test images are being used, run the createtesttrainsplit.py script.
# this will reshuffle images in the corresponding folders.

import sys
import json
import time
import math
import wandb
import os,os.path
import numpy as np
from PIL import Image
import tensorflow as tf
from string import Template
import matplotlib.pyplot as plt
from skimage.transform import resize
from wandb.keras import WandbCallback
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array, array_to_img

CLASS_NAMES = ["pickup-truck", "sedan", "suv", "van"]

# read JSON hyperparameters file.
script_dir = os.path.dirname(__file__)
file_path = os.path.join(script_dir, 'hparams_mycnn.json')
with open(file_path, encoding='utf-8') as json_file:
    json_hparams = json.loads(json_file.read())

# if using dataset_rescaled images ("_rescaled"), no need for on the fly rescaling of images from the original dataset ("").
ds_xtra_str = json_hparams['ds_xtra_str for dataset_rescaled']

# NOTE: must give 1 command line argument: backend (TF) engine_location.
# location of dataset directory depending on where the script is run.
engine_location = sys.argv[1]

if(engine_location == "local"):
    dst_path_start_private = "/home/andrei/Desktop/final-year-project-results/images/private/dataset" + ds_xtra_str
    dst_path_start_public = "/home/andrei/Desktop/final-year-project-results/images/public/dataset" + ds_xtra_str
elif(engine_location == "msi"):
    dst_path_start_private = "D:\\datasets\\private\\dataset" + ds_xtra_str
    dst_path_start_public = "D:\\datasets\\public\\dataset" + ds_xtra_str
elif(engine_location == "gpu-server"):
    dst_path_start_private = "/media/RAID5/abarcovschi/final-year-project-results/images/private/dataset" + ds_xtra_str
    dst_path_start_public = "/media/RAID5/abarcovschi/final-year-project-results/images/public/dataset" + ds_xtra_str
else:
    raise Exception("InvalidArgumentException: please specify 'local', 'msi' or 'gpu-server' as first argv!")

# HYPERPARAMETERS.
DIR_SEP = '\\' if engine_location == 'msi' else '/'
MODEL_NAME = json_hparams['saved model name']
TRAIN_PRIV = (json_hparams['train data'] == 'private')
IMG_SIZE = json_hparams['image size']
COLOUR_CHANNELS = 3
PLANAR = (json_hparams['planar'] == 'true')
IMG_SHAPE = (COLOUR_CHANNELS, IMG_SIZE, IMG_SIZE) if PLANAR else (IMG_SIZE, IMG_SIZE, COLOUR_CHANNELS)
BATCH_SIZE = json_hparams['batch size']
BASE_LEARNING_RATE = json_hparams['base learning rate']
INITIAL_EPOCHS = json_hparams['initial epochs']
VALIDATION_STEPS = json_hparams['validation steps']
FINE_TUNE_EPOCHS = json_hparams['fine tune epochs']
SHUFFLE_BUFFER_SIZE = json_hparams['buffer size']

# Weights and Biases setup.
wandb.init(project="final-year-project-mycnn",
           notes=json_hparams["wandb notes"],
           tags=json_hparams["wandb tags"],
           config={"buffer size": SHUFFLE_BUFFER_SIZE,
                    "image shape": IMG_SHAPE,
                    "batch size": BATCH_SIZE,
                    "base learning rate": BASE_LEARNING_RATE,
                    "intial epochs": INITIAL_EPOCHS,
                    "validation steps": VALIDATION_STEPS,
                    "fine tune epochs": FINE_TUNE_EPOCHS})
#WANDB_MODE = dryrun # for testing, disables wandb syncing.

# create a per class accuracy score as a fraction of the total attempted predictions for each true class.
def per_class_accuracy(actual_labels, predicted_labels):
    if(len(actual_labels) != len(predicted_labels)): raise Exception("len(actual_labels) != len(predicted_labels!!!")
    correct_pickups=total_pickups=correct_sedans=total_sedans=correct_suvs=total_suvs=correct_vans=total_vans=0
    for i in range(len(predicted_labels)):
        actual = actual_labels[i,:]
        predicted = predicted_labels[i,:]
        actual_argmax_idx = np.argmax(actual)
        predicted_argmax_idx = np.argmax(predicted)
        if(actual_argmax_idx == 0):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_pickups+=1
                total_pickups+=1
            else:
                total_pickups+=1
        elif(actual_argmax_idx == 1):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_sedans+=1
                total_sedans+=1
            else:
                total_sedans+=1
        elif(actual_argmax_idx == 2):
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_suvs+=1
                total_suvs+=1
            else:
                total_suvs+=1
        else:
            if(actual_argmax_idx == predicted_argmax_idx):
                correct_vans+=1
                total_vans+=1
            else:
                total_vans+=1
    frac_correct_pickups = correct_pickups / total_pickups
    frac_correct_sedans = correct_sedans / total_sedans
    frac_correct_suvs = correct_suvs / total_suvs
    frac_correct_vans = correct_vans / total_vans
    return (frac_correct_pickups, frac_correct_sedans, frac_correct_suvs, frac_correct_vans)

# arg private is a bool that specifies to use private or public dataset for training.
def create_unlabeled_train_data(private):
    # create unlabeled lists of images as tf.Dataset objects for training (from folders train and valid).
    if (private):
        # private images.
        list_ds_train = tf.data.Dataset.list_files(dst_path_start_private + Template('${d}train${d}*${d}*').substitute(d=DIR_SEP))
        list_ds_valid = tf.data.Dataset.list_files(dst_path_start_private + Template('${d}valid${d}*${d}*').substitute(d=DIR_SEP))
    else:
        # public images.
        list_ds_train = tf.data.Dataset.list_files(dst_path_start_public + Template('${d}train${d}*${d}*').substitute(d=DIR_SEP))
        list_ds_valid = tf.data.Dataset.list_files(dst_path_start_public + Template('${d}valid${d}*${d}*').substitute(d=DIR_SEP))
    return (list_ds_train, list_ds_valid)

# labels_dict : {ind_label: count_label}
# mu : parameter to tune: when mu -> 0, weights -> 1.0 (for larger datasets mu should get smaller to smooth out weigths for imbalanced classes more).
# for very large datasets consider taking log of score.
def create_class_weights(labels_dict,mu=1):
    total = sum(labels_dict.values())
    class_weight = dict()

    for key in labels_dict.keys():
        score = mu*total/float(labels_dict[key])
        class_weight[key] = score if score > 1.0 else 1.0

    return class_weight

# creates a padded (larger) image with aspect ratio 1:1.
def pad_img(img):
    h, w, c = img.shape
    # 1 if width is the longer side, 0 if height.
    w_longer = 1 if (w > h) else 0
    # create new image of desired size and colour for padding.
    side = img.shape[w_longer]
    colour = (80,80,80) # dark grey.
    result = np.full((side, side, c), colour, dtype=np.uint8)
    # compute padding amount.
    xx = (side - w) // 2
    yy = (side - h) // 2
    # copy img into center of result.
    result[yy:yy+h, xx:xx+w] = img

    return result

# creates a padded image of shape (IMG_SIZE,IMG_SIZE,c).
def resize_pad_img(img):
    # img is a PIL image.
    # if greyscale image.
    if(img.mode == "L"):
        img = img.convert("RGB")
    w, h = img.size
    ww = IMG_SIZE if (w>h) else int(IMG_SIZE*w/h)
    hh = IMG_SIZE if (h>w) else int(IMG_SIZE*h/w)
    # downsize original img.
    img = img.resize((ww,hh))
    # create new image of desired size and colour for padding.
    colour = (80,80,80) # dark grey.
    result = np.full((IMG_SIZE, IMG_SIZE, 3), colour, dtype=np.uint8)
    # compute padding amount.
    padx = (IMG_SIZE - ww) // 2
    pady = (IMG_SIZE - hh) // 2
    # copy img into center of result.
    img = np.array(img)
    result[pady:pady+hh, padx:padx+ww] = img

    return result # numpy array

def get_label(file_path):
    # convert the path to a list of path components.
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory.
    a = parts[-2] == CLASS_NAMES
    # label for an image will be a one-hot encoded version of ["pickup-truck", "sedan", "suv", "van"], e.g. [1,0,0,0].
    return tf.cast(a, tf.int32)

def planar_tensor(img):
    # convert the img numpy array to a 3D uint8 planar tensor (RGB24).
    img = tf.convert_to_tensor(img)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    r = img[:,:,0] # R plane.
    g = img[:,:,1] # G plane.
    b = img[:,:,2] # B plane.
    # convert image to planar tensor of shape COLOUR_CHANNELSxIMG_SIZExIMG_SIZE (correct for convolution).
    img = tf.concat([r,g,b],0)
    img = tf.reshape(img, [COLOUR_CHANNELS,IMG_SIZE,IMG_SIZE])
    # cast image to float32 format.
    img = tf.cast(img, tf.float32)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

def interleaved_tensor(img):
    # convert the img numpy array to a 3D uint8 interleaved tensor (RGB24).
    img = tf.convert_to_tensor(img)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    # cast image to float32 format.
    img = tf.cast(img, tf.float32)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

def process_path(file_path):
    label = get_label(file_path)

    # dataset already rescaled.
    if(ds_xtra_str == "_rescaled"):
        img = load_img(bytes.decode(file_path.numpy())) # PIL Image.
        img = img_to_array(img) # numpy array.
    # rescale images on the fly (slow).
    else:
        img = load_img(bytes.decode(file_path.numpy())) # PIL Image.
        img = resize_pad_img(img)

    if(PLANAR):
        img = planar_tensor(img)
    else:
        img = interleaved_tensor(img)
    return img, label

list_ds_train, list_ds_valid = create_unlabeled_train_data(TRAIN_PRIV)
    
if(json_hparams['test data'] == 'private'):
    # private images.
    list_ds_test = tf.data.Dataset.list_files(dst_path_start_private + Template('${d}test${d}*${d}*').substitute(d=DIR_SEP))
elif(json_hparams['test data'] == 'public'):
    # public images.
    list_ds_test = tf.data.Dataset.list_files(dst_path_start_public + Template('${d}test${d}*${d}*').substitute(d=DIR_SEP))
else:
    raise Exception("Test data hyperparam in JSON hparam file is incorrect. Please specify 'public' or 'private' for 'test data' hparam.")

def tf_reshape_planar(i,l):
    i = tf.reshape(i, [COLOUR_CHANNELS, IMG_SIZE, IMG_SIZE])
    l = tf.reshape(l, [4])
    return i, l

def tf_reshape_interleaved(i,l):
    i = tf.reshape(i, [IMG_SIZE, IMG_SIZE, COLOUR_CHANNELS])
    l = tf.reshape(l, [4])
    return i, l

# Use tf.Dataset.map to create tf.Dataset objects containing (image, label) pairs. 
# image and label objects are of type tf.Tensor (img dtype=tf.float32, label dtype=tf.int32).
# lambda expression and tf.py_function needed to execute process_path eagerly so that can convert Tensor to numpy array.
labeled_ds_train = list_ds_train.map(lambda x: tf.py_function(process_path, [x], [tf.float32, tf.int32]))
labeled_ds_valid = list_ds_valid.map(lambda x: tf.py_function(process_path, [x], [tf.float32, tf.int32]))
labeled_ds_test = list_ds_test.map(lambda x: tf.py_function(process_path, [x], [tf.float32, tf.int32]))

if(PLANAR):
    labeled_ds_train = labeled_ds_train.map(tf_reshape_planar)
    labeled_ds_valid = labeled_ds_valid.map(tf_reshape_planar)
    labeled_ds_test = labeled_ds_test.map(tf_reshape_planar)
else:
    labeled_ds_train = labeled_ds_train.map(tf_reshape_interleaved)
    labeled_ds_valid = labeled_ds_valid.map(tf_reshape_interleaved)
    labeled_ds_test = labeled_ds_test.map(tf_reshape_interleaved)

test_labels = np.array([x[1].numpy() for x in list(labeled_ds_test)])

# ensure eager execution produces required results by using the datasets created expicitly.
# for _, l in labeled_ds_train.take(1):
#     pass
# for _, l in labeled_ds_valid.take(1):
#     pass
# for _, l in labeled_ds_test.take(1):
#     pass

# inspect train data.
# image and label objects are of type tf.Tensor (img dtype=tf.float32, label dtype=tf.int32).
# for image, label in labeled_ds_train.take(10):
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# inspect test data
# for image, label in labeled_ds_test.take(2):
#     #pass
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# create shuffled batches.
train_batches = labeled_ds_train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = labeled_ds_valid.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
test_batches = labeled_ds_test.batch(BATCH_SIZE) # shuffling test data will mess up the test_labels pairing with test data.

def uncompiled_model_complex_vgg():
    cfg_data_fmt = 'channels_first' if PLANAR else 'channels_last'

    he_initialiser = tf.keras.initializers.GlorotNormal()
    model = tf.keras.Sequential()
    
    # VGGNet version.
    #model.add(tf.keras.layers.InputLayer(input_shape=(3,128,128), dtype=tf.float32))
    model.add(tf.keras.layers.Conv2D(64, kernel_size=(3,3), input_shape=IMG_SHAPE, activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(64, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Conv2D(128, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(128, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Conv2D(512, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Flatten(data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dense(2048, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dense(2048, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dense(4, activation='softmax', kernel_initializer='he_uniform'))

    model.summary()
    return model

def uncompiled_model_complex_my():
    he_initialiser = tf.keras.initializers.GlorotNormal()
    model = tf.keras.Sequential()
    cfg_data_fmt = 'channels_first' if PLANAR else 'channels_last'

    model.add(tf.keras.layers.InputLayer(input_shape=(3,128,128), dtype=tf.float32))
    model.add(tf.keras.layers.Conv2D(32, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(32, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.Conv2D(64, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(64, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dropout(0.3))
    model.add(tf.keras.layers.Conv2D(128, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(128, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dropout(0.4))
    model.add(tf.keras.layers.Conv2D(516, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.Conv2D(516, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Flatten(data_format=cfg_data_fmt))
    model.add(tf.keras.layers.Dense(256, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dense(256, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(4, activation='softmax'))

    model.summary()
    return model

model = uncompiled_model_complex_my()

# must compile the model before training.
model.compile(optimizer=tf.optimizers.Adam(learning_rate=BASE_LEARNING_RATE),
              loss=tf.keras.losses.CategoricalCrossentropy(),
              metrics=[
                    'accuracy',
                    tf.keras.metrics.Precision(class_id=0),
                    tf.keras.metrics.Precision(class_id=1),
                    tf.keras.metrics.Precision(class_id=2),
                    tf.keras.metrics.Precision(class_id=3)])

# print(len(model.trainable_variables)) # returns: 2 (these are tf.Variable objects: one for weights and one for biases).

# dictionary of number of images for each class present in the train directory.
train_base_path = dst_path_start_private + Template('${d}train${d}').substitute(d=DIR_SEP) if TRAIN_PRIV else dst_path_start_public + Template('${d}train${d}').substitute(d=DIR_SEP)

train_img_amounts = {
    0: len([name for name in os.listdir(train_base_path+"pickup-truck") if os.path.isfile(os.path.join(train_base_path+"pickup-truck", name))]),
    1: len([name for name in os.listdir(train_base_path+"sedan") if os.path.isfile(os.path.join(train_base_path+"sedan", name))]),
    2: len([name for name in os.listdir(train_base_path+"suv") if os.path.isfile(os.path.join(train_base_path+"suv", name))]),
    3: len([name for name in os.listdir(train_base_path+"van") if os.path.isfile(os.path.join(train_base_path+"van", name))])
}
class_weights = create_class_weights(train_img_amounts)

tic = time.perf_counter()

# evaluate initial accuracy of model before training. Different from predict, eval doesn't return predictions.
loss0, accuracy0, precision_1, precision_2, precision_3, precision_4 = model.evaluate(validation_batches, steps = VALIDATION_STEPS)
wandb.log({'initial_accuracy': accuracy0, 'initial_loss': loss0})

print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

# train the model.
history = model.fit(train_batches,
                    epochs=INITIAL_EPOCHS,
                    validation_data=validation_batches,
                    class_weight=class_weights,
                    callbacks=[WandbCallback()])

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

# plt.figure(figsize=(8, 8))
# plt.subplot(2, 1, 1)
# plt.plot(acc, label='Training Accuracy')
# plt.plot(val_acc, label='Validation Accuracy')
# plt.legend(loc='lower right')
# plt.ylabel('Accuracy')
# plt.title('Training and Validation Accuracy')

# plt.subplot(2, 1, 2)
# plt.plot(loss, label='Training Loss')
# plt.plot(val_loss, label='Validation Loss')
# plt.legend(loc='upper right')
# plt.ylabel('Cross Entropy')
# plt.title('Training and Validation Loss')
# plt.xlabel('epoch')
# plt.show()

loss1, accuracy1, precision_1, precision_2, precision_3, precision_4 = model.evaluate(test_batches, steps = VALIDATION_STEPS)

wandb.log({'test_accuracy': accuracy1, 'test_loss': loss1})
print("test loss: {:.2f}".format(loss1))
print("test accuracy: {:.2f}".format(accuracy1))

# FINE TUNING ON PRIVATE DATA.

def prepare_finetuned_model(base_model):
    # remove dense layers and last 1 conv layers.
    # create new finetune model.
    for i in range(9):
        base_model.pop()
        i+=1
    
    ft_model = tf.keras.Sequential()
    ft_model.add(tf.keras.layers.InputLayer(input_shape=(3,128,128), dtype=tf.float32)) # must add a new input layer as input layer of base_model is not addable.

    for layer in base_model.layers:
        ft_model.add(layer)

    cfg_data_fmt = 'channels_first' if PLANAR else 'channels_last'
    ft_model.trainable = False

    # ft_model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    # ft_model.add(tf.keras.layers.Conv2D(256, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    # ft_model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    # ft_model.add(tf.keras.layers.Dropout(0.4))
    ft_model.add(tf.keras.layers.Conv2D(516, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    ft_model.add(tf.keras.layers.Conv2D(516, kernel_size=(3,3), activation='relu', padding='same', data_format=cfg_data_fmt, kernel_initializer='he_uniform')) # 32 kernels/channels, kernel size = 3x3.
    ft_model.add(tf.keras.layers.MaxPooling2D((2, 2), data_format=cfg_data_fmt))
    ft_model.add(tf.keras.layers.Dropout(0.5))
    ft_model.add(tf.keras.layers.Flatten(data_format=cfg_data_fmt))
    ft_model.add(tf.keras.layers.Dense(256, activation='relu', kernel_initializer='he_uniform'))
    ft_model.add(tf.keras.layers.Dense(256, activation='relu', kernel_initializer='he_uniform'))
    ft_model.add(tf.keras.layers.Dropout(0.5))
    ft_model.add(tf.keras.layers.Dense(4, activation='softmax'))
    ft_model.summary()
    return ft_model

new_finetune_model = prepare_finetuned_model(model)

# compile model again with lower learning rate.
new_finetune_model.compile(loss=tf.keras.losses.CategoricalCrossentropy(),
              optimizer=tf.optimizers.Adam(learning_rate=BASE_LEARNING_RATE/2),
              metrics=[
                    'accuracy',
                    tf.keras.metrics.Precision(class_id=0),
                    tf.keras.metrics.Precision(class_id=1),
                    tf.keras.metrics.Precision(class_id=2),
                    tf.keras.metrics.Precision(class_id=3)])

# make train data from private dataset and shuffle training data again.
total_epochs =  INITIAL_EPOCHS + FINE_TUNE_EPOCHS
list_ds_train, list_ds_valid = create_unlabeled_train_data(True)
labeled_ds_train = list_ds_train.map(lambda x: tf.py_function(process_path, [x], [tf.float32, tf.int32]))
labeled_ds_valid = list_ds_valid.map(lambda x: tf.py_function(process_path, [x], [tf.float32, tf.int32]))
if(PLANAR):
    labeled_ds_train = labeled_ds_train.map(tf_reshape_planar)
    labeled_ds_valid = labeled_ds_valid.map(tf_reshape_planar)
else:
    labeled_ds_train = labeled_ds_train.map(tf_reshape_interleaved)
    labeled_ds_valid = labeled_ds_valid.map(tf_reshape_interleaved)
train_batches = labeled_ds_train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = labeled_ds_valid.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)

# inspect train data
# for image, label in labeled_ds_train.take(2):
#     #pass
#     print("Image shape: ", image.numpy().shape)
#     print("Label: ", label.numpy())
#     plt.figure()
#     plt.imshow(np.squeeze(image))
#     plt.title(label.numpy())
#     plt.show()

# create class weights for training.
# dictionary of number of images for each class present in the train directory.
train_base_path = dst_path_start_private + Template('${d}train${d}').substitute(d=DIR_SEP)
train_img_amounts = {
    0: len([name for name in os.listdir(train_base_path+"pickup-truck") if os.path.isfile(os.path.join(train_base_path+"pickup-truck", name))]),
    1: len([name for name in os.listdir(train_base_path+"sedan") if os.path.isfile(os.path.join(train_base_path+"sedan", name))]),
    2: len([name for name in os.listdir(train_base_path+"suv") if os.path.isfile(os.path.join(train_base_path+"suv", name))]),
    3: len([name for name in os.listdir(train_base_path+"van") if os.path.isfile(os.path.join(train_base_path+"van", name))])
}
class_weights = create_class_weights(train_img_amounts)

# fine tune model.
history_fine = new_finetune_model.fit(train_batches,
                         epochs=total_epochs,
                         initial_epoch =  history.epoch[-1],
                         validation_data=validation_batches,
                         class_weight=class_weights,
                         callbacks=[WandbCallback()])

new_finetune_model.summary()
new_finetune_model.save(MODEL_NAME)

toc = time.perf_counter()
print(f"Trained model in {(toc - tic)/60:0.4f} minutes")

# if validation loss much higher than training loss -> overfitting.
# this means the new training set is relatively small and similar to the original datasets used to train the base model (ImageNet).

# acc += history_fine.history['accuracy']
# val_acc += history_fine.history['val_accuracy']

# loss += history_fine.history['loss']
# val_loss += history_fine.history['val_loss']

# plt.figure(figsize=(8, 8))
# plt.subplot(2, 1, 1)
# plt.plot(acc, label='Training Accuracy')
# plt.plot(val_acc, label='Validation Accuracy')
# plt.plot([INITIAL_EPOCHS-1,INITIAL_EPOCHS-1],
#           plt.ylim(), label='Start Fine Tuning')
# plt.legend(loc='lower right')
# plt.title('Training and Validation Accuracy')

# plt.subplot(2, 1, 2)
# plt.plot(loss, label='Training Loss')
# plt.plot(val_loss, label='Validation Loss')
# plt.plot([INITIAL_EPOCHS-1,INITIAL_EPOCHS-1],
#          plt.ylim(), label='Start Fine Tuning')
# plt.legend(loc='upper right')
# plt.title('Training and Validation Loss')
# plt.xlabel('epoch')
# plt.show()

loss2, accuracy2, precision_pickup, precision_sedan, precision_suv, precision_van = new_finetune_model.evaluate(test_batches, steps = VALIDATION_STEPS)
wandb.log({
    'finetune_test_accuracy': accuracy2,
    'finetune_test_loss': loss2,
    'fine_tune_pickup_precision': precision_pickup,
    'fine_tune_sedan_precision': precision_sedan,
    'fine_tune_suv_precision': precision_suv,
    'fine_tune_van_precision': precision_van})
print("fine tuned test loss: {:.2f}".format(loss2))
print("fine tuned test accuracy: {:.2f}".format(accuracy2))

test_predictions = new_finetune_model.predict(test_batches)
frac_correct_pickups, frac_correct_sedans, frac_correct_suvs, frac_correct_vans = per_class_accuracy(test_labels, test_predictions)
print("Fraction of pickup-trucks correctly classified on test data after fine tuning: " + str(frac_correct_pickups))
print("Fraction of sedans correctly classified on test data after fine tuning: " + str(frac_correct_sedans))
print("Fraction of SUVs correctly classified on test data after fine tuning: " + str(frac_correct_suvs))
print("Fraction of vans correctly classified on test data after fine tuning: " + str(frac_correct_vans))
