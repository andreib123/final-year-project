## code to run inference using the model.predict method on a loaded saved model using the test set from the dataset.

import sys
import os,os.path
import numpy as np
from PIL import Image
import tensorflow as tf
from string import Template
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array, array_to_img

lbl = [1,0,0,0] # test label for one image.

# NOTE: must give 1 command line argument: backend (TF) engine_location.
# location of dataset directory depending on where the script is run.
engine_location = sys.argv[1]

if(engine_location == "local"):
    dst_path_start_private = "/home/andrei/Desktop/final-year-project-results/images/private/dataset_rescaled"
    dst_path_start_public = "/home/andrei/Desktop/final-year-project-results/images/public/dataset_rescaled"
elif(engine_location == "msi"):
    dst_path_start_private = "D:\\datasets\\private\\dataset_rescaled"
    dst_path_start_public = "D:\\datasets\\public\\dataset_rescaled"
elif(engine_location == "gpu-server"):
    dst_path_start_private = "/media/RAID5/abarcovschi/final-year-project-results/images/private/dataset_rescaled"
    dst_path_start_public = "/media/RAID5/abarcovschi/final-year-project-results/images/public/dataset_rescaled"
else:
    raise Exception("InvalidArgumentException: please specify 'local', 'msi' or 'gpu-server' as first argv!")

# HYPERPARAMETERS.
DIR_SEP = '\\' if engine_location == 'msi' else '/'
IMG_SIZE = 128
CLASS_NAMES = ["pickup-truck", "sedan", "suv", "van"]

def get_label(file_path):
    # convert the path to a list of path components.
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory.
    a = parts[-2] == CLASS_NAMES
    # label for an image will be a one-hot encoded version of ["pickup-truck", "sedan", "suv", "van"], e.g. [1,0,0,0].
    return tf.cast(a, tf.int32)

def resize_pad_img(img):
    # img is a PIL image.
    # if greyscale image.
    if(img.mode == "L"):
        img = img.convert("RGB")
    w, h = img.size
    ww = IMG_SIZE if (w>h) else int(IMG_SIZE*w/h)
    hh = IMG_SIZE if (h>w) else int(IMG_SIZE*h/w)
    # downsize original img.
    img = img.resize((ww,hh))
    # create new image of desired size and colour for padding.
    colour = (80,80,80) # dark grey.
    result = np.full((IMG_SIZE, IMG_SIZE, 3), colour, dtype=np.uint8)
    # compute padding amount.
    padx = (IMG_SIZE - ww) // 2
    pady = (IMG_SIZE - hh) // 2
    # copy img into center of result.
    img = np.array(img)
    result[pady:pady+hh, padx:padx+ww] = img

    return result

def planar_tensor(img):
    # convert the expanded img array to a 3D uint8 planar tensor (RGB24).
    img = tf.convert_to_tensor(img)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    r = img[:,:,0] # R plane.
    g = img[:,:,1] # G plane.
    b = img[:,:,2] # B plane.
    # convert image to planar tensor of shape COLOUR_CHANNELSxIMG_SIZExIMG_SIZE (correct for convolution).
    img = tf.concat([r,g,b],0)
    img = tf.reshape(img, [3,IMG_SIZE,IMG_SIZE])
    # cast image to tf_dtype format.
    img = tf.cast(img, tf.float32)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

# uses hard-coded label array.
def process_path_one_img(file_path):
    label = LABEL_ARR_1
    img = load_img(bytes.decode(file_path.numpy())) # PIL Image
    img = resize_pad_img(img)
    img = planar_tensor(img)
    return img, label

# uses rescaled dataset.
def process_path_multiple_imgs(file_path):
    label = get_label(file_path)
    img = load_img(bytes.decode(file_path.numpy())) # PIL Image
    img = img_to_array(img)
    img = planar_tensor(img)
    return img, label

def tf_reshape(i,l):
    i = tf.reshape(i, [3, IMG_SIZE, IMG_SIZE])
    l = tf.reshape(l, [4])
    return i, l

def predict_one_img(model, imgfile, label_arr):
    # create BatchDataset of images (one image only).
    oneimglist = tf.data.Dataset.list_files(imgfile)
    oneimglabeled = oneimglist.map(lambda x: tf.py_function(process_path_one_img, [x], [tf.float32, tf.int32]))
    oneimglabeled = oneimglabeled.map(tf_reshape)
    oneimgbatch = oneimglabeled.batch(1)

    # output prediction.
    print(oneimgbatch)
    prediction = model.predict(oneimgbatch)
    return prediction

def predict_multiple_imgs(model, private):
    if(private):
        # private images.
        list_ds_test = tf.data.Dataset.list_files(dst_path_start_private + Template('${d}test${d}*${d}*').substitute(d=DIR_SEP))
    else:
        # public images.
        list_ds_test = tf.data.Dataset.list_files(dst_path_start_public + Template('${d}test${d}*${d}*').substitute(d=DIR_SEP))
    labeled_ds_test = list_ds_test.map(lambda x: tf.py_function(process_path_multiple_imgs, [x], [tf.float32, tf.int32]))
    labeled_ds_test = labeled_ds_test.map(tf_reshape)
    test_images = np.array([x[0].numpy() for x in list(labeled_ds_test)])
    test_labels = np.array([x[1].numpy() for x in list(labeled_ds_test)])
    #b = labeled_ds_test_batches.take(1)
    #test_np = list(b)
    #test_np = tf.convert_to_tensor(test_np)
    test_predictions = model.predict(test_images, batch_size=32)
    #test_predictions = model(test_np)
    print(tf.math.confusion_matrix(tf.argmax(test_labels, 1), tf.argmax(test_predictions, 1)))

# load model and images for prediction.
model = tf.keras.models.load_model('bigger_private_ds_v5')
model.summary()

# one img prediction
# imgfile = '/home/andrei/Desktop/final-year-project-results/images/test/pickup.jpg'
# LABEL_ARR_1 = [1,0,0,0]
# prediction_one = predict_one_img(model, imgfile, LABEL_ARR_1)

# multiple images prediction.
predict_multiple_imgs(model, True)