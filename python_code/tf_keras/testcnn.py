import tensorflow as tf
import numpy as np

ones_initializer = tf.keras.initializers.ones()
model = tf.keras.Sequential()
model.add(tf.keras.layers.InputLayer(input_shape=(3,128,128), batch_size=1, dtype=tf.float32))
model.add(tf.keras.layers.Conv2D(32,
    input_shape=(3,128,128),
    kernel_size=(3,3),
    padding='valid',
    data_format='channels_first',
    kernel_initializer=ones_initializer)) # 32 kernels/channels, kernel size = 3x3.

model.save('conv1_unk')
#tensor1 = tf.ones((3,128,128))

#out = model.predict(tensor1)
pass

# data_dir = "/home/andrei/Desktop/final-year-project-results/images/test/"
# fname = "outfrommpy.data"
# with open(data_dir+fname, "wb") as myfile:
#     np.save(myfile, out)
