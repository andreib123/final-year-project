'''Opens an interleaved JPEG file and saves as a float32 planar/interleaved rescaled to [-1,1] resized to 128x128 RAW binary file.'''

import os,os.path
import numpy as np
from PIL import Image
import tensorflow as tf
from numpy import savetxt
import matplotlib.pyplot as plt

from tensorflow.keras.preprocessing.image import load_img

IMG_SIZE = 128
py_dtype = np.float32
tf_dtype = tf.float32

def resize_pad_img(img):
    # img is a PIL image.
    # if greyscale image.
    if(img.mode == "L"):
        img = img.convert("RGB")
    w, h = img.size
    ww = IMG_SIZE if (w>h) else int(IMG_SIZE*w/h)
    hh = IMG_SIZE if (h>w) else int(IMG_SIZE*h/w)
    # downsize original img.
    img = img.resize((ww,hh))
    # create new image of desired size and colour for padding.
    colour = (80,80,80) # dark grey.
    result = np.full((IMG_SIZE, IMG_SIZE, 3), colour, dtype=np.uint8)
    # compute padding amount.
    padx = (IMG_SIZE - ww) // 2
    pady = (IMG_SIZE - hh) // 2
    # copy img into center of result.
    img = np.array(img)
    result[pady:pady+hh, padx:padx+ww] = img

    return result

def planar_tensor(img):
    # convert the expanded img array to a 3D uint8 planar tensor (RGB24).
    img = tf.convert_to_tensor(img)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    r = img[:,:,0] # R plane.
    g = img[:,:,1] # G plane.
    b = img[:,:,2] # B plane.
    # convert image to planar tensor of shape COLOUR_CHANNELSxIMG_SIZExIMG_SIZE (correct for convolution).
    img = tf.concat([r,g,b],0)
    img = tf.reshape(img, [3,IMG_SIZE,IMG_SIZE])
    # cast image to tf_dtype format.
    img = tf.cast(img, tf_dtype)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

def interleaved_tensor(img):
    # convert the expanded img array to a 3D uint8 interleaved tensor (RGB24).
    img = tf.convert_to_tensor(img)
    # resize the image to shape IMG_SIZExIMG_SIZExCOLOUR_CHANNELS.
    img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    # cast image to tf_dtype format.
    img = tf.cast(img, tf_dtype)
    # rescale the image values to range [-1,1].
    # pretrained models expect dataset input channels to be in range of [-1,1].
    img = (img/127.5) - 1
    
    return img

def process_path(file_path, planar):
    img = Image.open(file_path) # PIL Image
    img = resize_pad_img(img)
    if(planar):
        img = planar_tensor(img)
    else:
        img = interleaved_tensor(img)
    return img.numpy()

planar = True # True for planar result, false for interleaved result.
infile= '/home/andrei/Desktop/final-year-project-results/images/test/van_private.jpeg'
img = process_path(infile, planar) # numpy array.
outfile = "/home/andrei/Desktop/final-year-project-results/images/test/van_128pf32rgb_resc_priv.data"

# save raw image as RGB24 format.
fileobj = open(outfile, mode='wb')
img.tofile(fileobj)
fileobj.close()

# load raw image as binary file.
# check if saved binary file has same values as the numpy array that was saved.
binfile = np.fromfile(outfile, dtype=py_dtype)
img_copy = np.reshape(binfile, (3,128,128))
pass
# plt.figure()
# plt.imshow(np.squeeze(img_np_copy))
# plt.show()