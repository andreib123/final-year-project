# $1 is path/to/savedmodel
# $2 is path/to/created/modelname.onnx
python -m tf2onnx.convert --saved-model $1 --output $2
