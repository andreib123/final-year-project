import glob
import shutil
import random
import os, os.path
import numpy as np
from PIL import Image

IMG_SIZE = 128

src_path_start_private = "/home/andrei/Desktop/final-year-project-results/images/private/from_livecam/"
src_path_start_public = "/home/andrei/Desktop/final-year-project-results/images/public/cars196/"
dst_path_start_private = "/home/andrei/Desktop/final-year-project-results/images/private/dataset/"
dst_path_start_public = "/home/andrei/Desktop/final-year-project-results/images/public/dataset/"


classes = ["pickup-truck", "sedan", "suv", "van"]

splits_percs = {
    "train": 64,
    "test": 20,
    "valid": 16
}

datasets = ["private", "public"]

# creates a padded numpy image of shape (IMG_SIZE,IMG_SIZE,c).
def resize_pad_img(img): # img is a PIL image.
    # if greyscale image.
    if(img.mode == "L"):
        img = img.convert("RGB")
    w, h = img.size
    ww = IMG_SIZE if (w>h) else int(IMG_SIZE*w/h)
    hh = IMG_SIZE if (h>w) else int(IMG_SIZE*h/w)
    # downsize original img.
    img = img.resize((ww,hh))
    # create new image of desired size and colour for padding.
    colour = (80,80,80) # dark grey.
    result = np.full((IMG_SIZE, IMG_SIZE, 3), colour, dtype=np.uint8)
    # compute padding amount.
    padx = (IMG_SIZE - ww) // 2
    pady = (IMG_SIZE - hh) // 2
    # copy img into center of result.
    img = np.array(img)
    result[pady:pady+hh, padx:padx+ww] = img

    return result # numpy array

# creates datasets of padded images of shape (IMG_SIZE,IMG_SIZE,3) by processing images in the src datasets.
def create_resized_padded_ds(base_path):
    for dataset in datasets:
        for folder in splits_percs:
            for c in classes:
                src_folder_path = base_path + dataset + "/dataset/" + folder + "/" + c
                dst_folder_path = base_path + dataset + "/dataset_rescaled/" + folder + "/" + c

                files = glob.glob(src_folder_path + "/*")
                i = 0
                for f in files:
                    img = Image.open(f)
                    print(f)
                    img = resize_pad_img(img)
                    img_name = c + str(i) + ".jpeg"
                    img = Image.fromarray(img)
                    img.save(dst_folder_path+"/"+img_name)
                    i+=1


def rename_files_in_folder(base_path , super_folder_name, folder_name):
    folder_path = base_path + super_folder_name + folder_name
    files = os.listdir(folder_path)
    for i, file in enumerate(files):
        os.rename(os.path.join(folder_path, file), os.path.join(folder_path, ''.join([folder_name ,str(i), '.jpeg'])))


def copy_files(files, src_path_start, dst_path_start, src_folder, dst_folder):
    for file_name in files:
        full_file_name = os.path.join(src_path_start+src_folder, file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, dst_path_start+dst_folder)

def clear_folder(folder_path):
    files = glob.glob(folder_path + "/*")
    for f in files:
        os.remove(f)

# create a new division of images added to test, train, valid folders for a particular splits_percs split.
def create_test_train_split(src_path_start, dst_path_start):
    total_train = 0
    total_valid = 0
    total_test = 0
    for c in classes:
        path = src_path_start + c
        file_names = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]
        random.shuffle(file_names)
        train_cnt = int(len(file_names) * splits_percs["train"] / 100)
        valid_cnt = int(len(file_names) * splits_percs["valid"] / 100)
        test_cnt = int(len(file_names) * splits_percs["test"] / 100)
        idx1 = 0
        idx2 = train_cnt - 1
        train_list = file_names[idx1:idx2]
        idx1 = idx2 + 1
        idx2 = idx1 + valid_cnt - 1
        valid_list = file_names[idx1:idx2]
        idx1 = idx2 + 1
        idx2 = idx1 + test_cnt - 1
        test_list = file_names[idx1:idx2]
        copy_files(train_list, src_path_start, dst_path_start, c, "train/"+c)
        copy_files(valid_list, src_path_start, dst_path_start, c, "valid/"+c)
        copy_files(test_list, src_path_start, dst_path_start, c, "test/"+c)
        total_train = total_train + train_cnt
        total_valid = total_valid + valid_cnt
        total_test = total_test + test_cnt
        print("%d train images, %d validation images and %d test images of class %s added to %s" % (train_cnt, valid_cnt, test_cnt, c, dst_path_start+c))
        #print("%s train_list_len:%d valid_list_len:%d test_list_len:%d" % (c, len(train_list), len(valid_list), len(test_list)))
    print("total train images: %d\ntotal validation images: %d\ntotal test images: %d" % (total_train, total_valid, total_test))
    print("=======================================================================================================")

# # clear dataset directories before adding new images into them.
# for folder in splits_percs:
#     for c in classes:
#         clear_folder(dst_path_start_public+folder+"/"+c)
#         clear_folder(dst_path_start_private+folder+"/"+c)

# create_test_train_split(src_path_start_private, dst_path_start_private)
# create_test_train_split(src_path_start_public, dst_path_start_public)

base_path = "/home/andrei/Desktop/final-year-project-results/images/"

# # rename all files in dataset directories to <classN>.jpeg
# for dataset in datasets:
#     for folder in splits_percs:
#         for c in classes:
#             rename_files_in_folder(base_path + dataset + "/dataset/" , folder + "/", c)

# # clear the dataset_rescaled directories.
# for dataset in datasets:
#     for folder in splits_percs:
#         for c in classes:
#             dataset_rescaled = base_path + dataset + "/dataset_rescaled/" + folder + "/" + c
#             clear_folder(dataset_rescaled)

# # read dataset directories and populate dataset_rescaled directories.
create_resized_padded_ds(base_path)
