import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_datasets as tfds

# tfds.load() downloads and caches the dataset data and returns a Dataset object.
# raw_train, raw_validation, raw_test are all Dataset objects that contain a list of (image, label) pairs.
# (image,label) pairs are of different sizes.
tfds.disable_progress_bar()
(raw_train, raw_validation, raw_test), metadata = tfds.load(
    'cats_vs_dogs',
    split=['train[:80%]', 'train[80%:90%]', 'train[90%:]'],
    with_info=True,
    as_supervised=True,
)

# get number of elements in any object.
# for num, _ in enumerate(raw_train):
#     pass
# print(f'NUmber fo elements: {num}')

# Show the first two images and labels from a Dataset object.
get_label_name = metadata.features['label'].int2str
for image, label in raw_train.take(2):
    plt.figure()
    plt.imshow(image)
    plt.title(label.numpy())
    plt.show()

# format the images to a fixed size input for the NN.
# input channels rescaled to a range of [-1,1] (rescale)
IMG_SIZE = 160 # All images will be resized to 160x160

def format_example(image, label):
    image = tf.cast(image, tf.float32)
    image = (image/127.5) -1
    image = tf.image.resize(image, (IMG_SIZE, IMG_SIZE))
    return image, label
train = raw_train.map(format_example)
validation = raw_validation.map(format_example)
test = raw_test.map(format_example)

# get number of elements in any object.
# for num, _ in enumerate(train):
#     pass
# print(f'Number of elements: {num}')

# create batches.
# shuffle the train batch data.
BATCH_SIZE = 15
SHUFFLE_BUFFER_SIZE = 1000
train_batches = train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = validation.batch(BATCH_SIZE)
test_batches = test.batch(BATCH_SIZE)

# get number of elements in any object.
# for num, _ in enumerate(train_batches):
#     pass
# print(f'Number of elements: {num}')

# inspect a batch of data.
for image_batch, label_batch in train_batches.take(1):
    pass

# instantiate a NN model pre-loaded with weights trained on a dataset.
# include_top=False loads the model without the top calssification layer, and the last layer will be the bottleneck layer.
# base model is used as a feature extractor.
IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 3)
base_model = tf.keras.applications.MobileNetV2(input_shape=(160,160,3), include_top=False, weights='imagenet')

# forward pass of model on one batch.
feature_batch = base_model(image_batch)
#print(feature_batch.shape)

# freeze base model and use it as a feature extractor only.
base_model.trainable = False

# base model architecture
base_model.summary()

global_average_layer = tf.keras.layers.GlobalAveragePooling2D() # add layer on top of base model to average every 5x5 into 1 value to create a vector. (BATCH_SIZEx5x5x1280 -> BATCH_SIZEx1x1x1280 (which is outputted as BATCH_SIZEx1280)).
feature_batch_average = global_average_layer(feature_batch) # output of the global_average_layer.
print(feature_batch_average.shape)

prediction_layer = tf.keras.layers.Dense(1) # add densely connected (fully connected) layer that outputs one prediction. No activation function here as binary classification (pos = cass 1, neg = class 0).
prediction_batch = prediction_layer(feature_batch_average) # outputted predictions (BATCH_SIZEx1).
#print(prediction_batch.shape)

# stack the base model and additional layers into a combined new model.
model = tf.keras.Sequential([
    base_model,
    global_average_layer,
    prediction_layer
])

# must compile the model before training.
# binary cross entropy loss used for binary classification.
base_learning_rate = 0.0001
model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=base_learning_rate),
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary() # parameters from the base model are frozen.
len(model.trainable_variables) # 2 tf.Variable objects: one for weights and one for biases.


initial_epochs = 10
validation_steps=20

# evaluate initial accuracy of model before training. Different from predict, eval doesn't return predictions.
loss0, accuracy0 = model.evaluate(validation_batches, steps = validation_steps)

print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

# train the model.
history = model.fit(train_batches,
                    epochs=initial_epochs,
                    validation_data=validation_batches)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()),1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0,1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()

# can increase performance (accuracy) of model further by training the top layers of the base model.
# this will tune the weights from generic feature maps to features associated specifically with the dataset.


# must compile the model before training.
# binary cross entropy loss used for binary classification.
base_learning_rate = 0.0001
model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=base_learning_rate),
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary() # parameters from the base model are frozen.
len(model.trainable_variables) # 2 tf.Variable objects: one for weights and one for biases.


initial_epochs = 10
validation_steps=20

# evaluate initial accuracy of model before training. Different from predict, eval doesn't return predictions.
loss0, accuracy0 = model.evaluate(validation_batches, steps = validation_steps)

print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

# train the model.
history = model.fit(train_batches,
                    epochs=initial_epochs,
                    validation_data=validation_batches)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()),1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0,1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()

# can increase performance (accuracy) of model further by training the top layers of the base model.
# this will tune the weights from generic feature maps to features associated specifically with the dataset.
# do this only after training the top layer with the base model frozen.
# only fine tune a small number of top layers of base model, not the whole base model.

#print("Number of layers in the base model: ", len(base_model.layers))

# unfreeze base model.
base_model.trainable = True
# fine tune from this layer upwards.
fine_tune_at = 100
# freeze all layers before fine_tune_at layer.
for layer in base_model.layers[:fine_tune_at]:
    layer.trainable = False

# compile model for changes to take effect.
# use a much lower learning rate.
model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              optimizer = tf.keras.optimizers.RMSprop(lr=base_learning_rate/10),
              metrics=['accuracy'])

# train model again.
fine_tune_epochs = 10
total_epochs =  initial_epochs + fine_tune_epochs

history_fine = model.fit(train_batches,
                         epochs=total_epochs,
                         initial_epoch =  history.epoch[-1],
                         validation_data=validation_batches)

# if validation loss much higher than training loss -> overfitting.
# this means the new training set is relatively small and similar to the original datasets used to train the base model (ImageNet).

acc += history_fine.history['accuracy']
val_acc += history_fine.history['val_accuracy']

loss += history_fine.history['loss']
val_loss += history_fine.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.ylim([0.8, 1])
plt.plot([initial_epochs-1,initial_epochs-1],
          plt.ylim(), label='Start Fine Tuning')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.ylim([0, 1.0])
plt.plot([initial_epochs-1,initial_epochs-1],
         plt.ylim(), label='Start Fine Tuning')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()

#
# Copyright (c) 2017 François Chollet                                                                                                                    # IGNORE_COPYRIGHT: cleared by OSS licensing
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
